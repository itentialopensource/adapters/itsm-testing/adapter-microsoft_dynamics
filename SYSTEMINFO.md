# Microsoft Dynamics

Vendor: Microsoft
Homepage: https://www.microsoft.com/

Product: Dynamics365
Product Page: https://www.microsoft.com/en-us/dynamics-365

## Introduction
We classify Microsoft Dynamics into the ITSM domain as Microsoft Dynamics is a suite of intelligent business applications designed to streamline operations, improve customer relationships, and drive growth through data-driven insights and automation. 

## Why Integrate
The Microsoft Dynamics adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft's Dynamics 365.

With this adapter you have the ability to perform operations with Microsoft Dynamics on items such as:

- Account
- Project
- Purchase Invoice
- Sales Quote

## Additional Product Documentation
The [API documents for Microsoft Dynamics](https://learn.microsoft.com/en-us/dynamics365/)