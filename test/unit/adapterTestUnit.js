/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-microsoft_dynamics',
      type: 'MicrosoftDynamics',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const MicrosoftDynamics = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Microsoft_dynamics Adapter Test', () => {
  describe('MicrosoftDynamics Class Tests', () => {
    const a = new MicrosoftDynamics(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('microsoft_dynamics'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('microsoft_dynamics'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('MicrosoftDynamics', pronghornDotJson.export);
          assert.equal('Microsoft_dynamics', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-microsoft_dynamics', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('microsoft_dynamics'));
          assert.equal('MicrosoftDynamics', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-microsoft_dynamics', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-microsoft_dynamics', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#listCompanies - errors', () => {
      it('should have a listCompanies function', (done) => {
        try {
          assert.equal(true, typeof a.listCompanies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCompany - errors', () => {
      it('should have a getCompany function', (done) => {
        try {
          assert.equal(true, typeof a.getCompany === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCompany(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCompany', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listItems - errors', () => {
      it('should have a listItems function', (done) => {
        try {
          assert.equal(true, typeof a.listItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listItems(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postItem - errors', () => {
      it('should have a postItem function', (done) => {
        try {
          assert.equal(true, typeof a.postItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postItem(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postItem('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItem - errors', () => {
      it('should have a getItem function', (done) => {
        try {
          assert.equal(true, typeof a.getItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getItem(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.getItem('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteItem - errors', () => {
      it('should have a deleteItem function', (done) => {
        try {
          assert.equal(true, typeof a.deleteItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteItem(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.deleteItem('fakeparam', null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchItem - errors', () => {
      it('should have a patchItem function', (done) => {
        try {
          assert.equal(true, typeof a.patchItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchItem(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.patchItem('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchItem('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPictureForItem - errors', () => {
      it('should have a listPictureForItem function', (done) => {
        try {
          assert.equal(true, typeof a.listPictureForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPictureForItem(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.listPictureForItem('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForItem - errors', () => {
      it('should have a getPictureForItem function', (done) => {
        try {
          assert.equal(true, typeof a.getPictureForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPictureForItem(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.getPictureForItem('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.getPictureForItem('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForItem - errors', () => {
      it('should have a deletePictureForItem function', (done) => {
        try {
          assert.equal(true, typeof a.deletePictureForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePictureForItem(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.deletePictureForItem('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.deletePictureForItem('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPictureForItem - errors', () => {
      it('should have a patchPictureForItem function', (done) => {
        try {
          assert.equal(true, typeof a.patchPictureForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPictureForItem(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.patchPictureForItem('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.patchPictureForItem('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPictureForItem('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPicture - errors', () => {
      it('should have a listPicture function', (done) => {
        try {
          assert.equal(true, typeof a.listPicture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPicture(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPicture - errors', () => {
      it('should have a getPicture function', (done) => {
        try {
          assert.equal(true, typeof a.getPicture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPicture(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.getPicture('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePicture - errors', () => {
      it('should have a deletePicture function', (done) => {
        try {
          assert.equal(true, typeof a.deletePicture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePicture(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.deletePicture('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPicture - errors', () => {
      it('should have a patchPicture function', (done) => {
        try {
          assert.equal(true, typeof a.patchPicture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPicture(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.patchPicture('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPicture('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPicture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPictureForCustomer - errors', () => {
      it('should have a listPictureForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.listPictureForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPictureForCustomer(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.listPictureForCustomer('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForCustomer - errors', () => {
      it('should have a getPictureForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.getPictureForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPictureForCustomer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.getPictureForCustomer('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.getPictureForCustomer('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForCustomer - errors', () => {
      it('should have a deletePictureForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.deletePictureForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePictureForCustomer(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.deletePictureForCustomer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.deletePictureForCustomer('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPictureForCustomer - errors', () => {
      it('should have a patchPictureForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.patchPictureForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPictureForCustomer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.patchPictureForCustomer('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.patchPictureForCustomer('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPictureForCustomer('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPictureForVendor - errors', () => {
      it('should have a listPictureForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.listPictureForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPictureForVendor(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.listPictureForVendor('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForVendor - errors', () => {
      it('should have a getPictureForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.getPictureForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPictureForVendor(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.getPictureForVendor('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.getPictureForVendor('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForVendor - errors', () => {
      it('should have a deletePictureForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.deletePictureForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePictureForVendor(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.deletePictureForVendor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.deletePictureForVendor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPictureForVendor - errors', () => {
      it('should have a patchPictureForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.patchPictureForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPictureForVendor(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.patchPictureForVendor('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.patchPictureForVendor('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPictureForVendor('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPictureForEmployee - errors', () => {
      it('should have a listPictureForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.listPictureForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPictureForEmployee(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.listPictureForEmployee('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForEmployee - errors', () => {
      it('should have a getPictureForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.getPictureForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPictureForEmployee(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.getPictureForEmployee('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.getPictureForEmployee('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForEmployee - errors', () => {
      it('should have a deletePictureForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.deletePictureForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePictureForEmployee(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.deletePictureForEmployee('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.deletePictureForEmployee('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPictureForEmployee - errors', () => {
      it('should have a patchPictureForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.patchPictureForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPictureForEmployee(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.patchPictureForEmployee('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pictureId', (done) => {
        try {
          a.patchPictureForEmployee('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pictureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPictureForEmployee('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPictureForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForItem - errors', () => {
      it('should have a listDefaultDimensionsForItem function', (done) => {
        try {
          assert.equal(true, typeof a.listDefaultDimensionsForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDefaultDimensionsForItem(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.listDefaultDimensionsForItem('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDefaultDimensionsForItem - errors', () => {
      it('should have a postDefaultDimensionsForItem function', (done) => {
        try {
          assert.equal(true, typeof a.postDefaultDimensionsForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postDefaultDimensionsForItem(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.postDefaultDimensionsForItem('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postDefaultDimensionsForItem('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForItem - errors', () => {
      it('should have a getDefaultDimensionsForItem function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultDimensionsForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDefaultDimensionsForItem(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.getDefaultDimensionsForItem('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.getDefaultDimensionsForItem('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.getDefaultDimensionsForItem('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForItem - errors', () => {
      it('should have a deleteDefaultDimensionsForItem function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDefaultDimensionsForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteDefaultDimensionsForItem(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.deleteDefaultDimensionsForItem('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.deleteDefaultDimensionsForItem('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.deleteDefaultDimensionsForItem('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDefaultDimensionsForItem - errors', () => {
      it('should have a patchDefaultDimensionsForItem function', (done) => {
        try {
          assert.equal(true, typeof a.patchDefaultDimensionsForItem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchDefaultDimensionsForItem(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemId', (done) => {
        try {
          a.patchDefaultDimensionsForItem('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'itemId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.patchDefaultDimensionsForItem('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.patchDefaultDimensionsForItem('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchDefaultDimensionsForItem('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForItem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensions - errors', () => {
      it('should have a listDefaultDimensions function', (done) => {
        try {
          assert.equal(true, typeof a.listDefaultDimensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDefaultDimensions(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDefaultDimensions - errors', () => {
      it('should have a postDefaultDimensions function', (done) => {
        try {
          assert.equal(true, typeof a.postDefaultDimensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postDefaultDimensions(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postDefaultDimensions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensions - errors', () => {
      it('should have a getDefaultDimensions function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultDimensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDefaultDimensions(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.getDefaultDimensions('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.getDefaultDimensions('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensions - errors', () => {
      it('should have a deleteDefaultDimensions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDefaultDimensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteDefaultDimensions(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.deleteDefaultDimensions('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.deleteDefaultDimensions('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDefaultDimensions - errors', () => {
      it('should have a patchDefaultDimensions function', (done) => {
        try {
          assert.equal(true, typeof a.patchDefaultDimensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchDefaultDimensions(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.patchDefaultDimensions('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.patchDefaultDimensions('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchDefaultDimensions('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForCustomer - errors', () => {
      it('should have a listDefaultDimensionsForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.listDefaultDimensionsForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDefaultDimensionsForCustomer(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.listDefaultDimensionsForCustomer('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDefaultDimensionsForCustomer - errors', () => {
      it('should have a postDefaultDimensionsForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.postDefaultDimensionsForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postDefaultDimensionsForCustomer(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.postDefaultDimensionsForCustomer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postDefaultDimensionsForCustomer('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForCustomer - errors', () => {
      it('should have a getDefaultDimensionsForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultDimensionsForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDefaultDimensionsForCustomer(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.getDefaultDimensionsForCustomer('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.getDefaultDimensionsForCustomer('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.getDefaultDimensionsForCustomer('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForCustomer - errors', () => {
      it('should have a deleteDefaultDimensionsForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDefaultDimensionsForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteDefaultDimensionsForCustomer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.deleteDefaultDimensionsForCustomer('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.deleteDefaultDimensionsForCustomer('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.deleteDefaultDimensionsForCustomer('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDefaultDimensionsForCustomer - errors', () => {
      it('should have a patchDefaultDimensionsForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.patchDefaultDimensionsForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchDefaultDimensionsForCustomer(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.patchDefaultDimensionsForCustomer('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.patchDefaultDimensionsForCustomer('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.patchDefaultDimensionsForCustomer('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchDefaultDimensionsForCustomer('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForVendor - errors', () => {
      it('should have a listDefaultDimensionsForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.listDefaultDimensionsForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDefaultDimensionsForVendor(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.listDefaultDimensionsForVendor('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDefaultDimensionsForVendor - errors', () => {
      it('should have a postDefaultDimensionsForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.postDefaultDimensionsForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postDefaultDimensionsForVendor(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.postDefaultDimensionsForVendor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postDefaultDimensionsForVendor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForVendor - errors', () => {
      it('should have a getDefaultDimensionsForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultDimensionsForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDefaultDimensionsForVendor(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.getDefaultDimensionsForVendor('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.getDefaultDimensionsForVendor('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.getDefaultDimensionsForVendor('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForVendor - errors', () => {
      it('should have a deleteDefaultDimensionsForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDefaultDimensionsForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteDefaultDimensionsForVendor(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.deleteDefaultDimensionsForVendor('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.deleteDefaultDimensionsForVendor('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.deleteDefaultDimensionsForVendor('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDefaultDimensionsForVendor - errors', () => {
      it('should have a patchDefaultDimensionsForVendor function', (done) => {
        try {
          assert.equal(true, typeof a.patchDefaultDimensionsForVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchDefaultDimensionsForVendor(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.patchDefaultDimensionsForVendor('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.patchDefaultDimensionsForVendor('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.patchDefaultDimensionsForVendor('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchDefaultDimensionsForVendor('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForEmployee - errors', () => {
      it('should have a listDefaultDimensionsForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.listDefaultDimensionsForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDefaultDimensionsForEmployee(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.listDefaultDimensionsForEmployee('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDefaultDimensionsForEmployee - errors', () => {
      it('should have a postDefaultDimensionsForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.postDefaultDimensionsForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postDefaultDimensionsForEmployee(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.postDefaultDimensionsForEmployee('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postDefaultDimensionsForEmployee('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForEmployee - errors', () => {
      it('should have a getDefaultDimensionsForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultDimensionsForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDefaultDimensionsForEmployee(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.getDefaultDimensionsForEmployee('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.getDefaultDimensionsForEmployee('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.getDefaultDimensionsForEmployee('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForEmployee - errors', () => {
      it('should have a deleteDefaultDimensionsForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDefaultDimensionsForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteDefaultDimensionsForEmployee(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.deleteDefaultDimensionsForEmployee('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.deleteDefaultDimensionsForEmployee('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.deleteDefaultDimensionsForEmployee('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDefaultDimensionsForEmployee - errors', () => {
      it('should have a patchDefaultDimensionsForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.patchDefaultDimensionsForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchDefaultDimensionsForEmployee(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.patchDefaultDimensionsForEmployee('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsParentId', (done) => {
        try {
          a.patchDefaultDimensionsForEmployee('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultDimensionsDimensionId', (done) => {
        try {
          a.patchDefaultDimensionsForEmployee('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'defaultDimensionsDimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchDefaultDimensionsForEmployee('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDefaultDimensionsForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomers - errors', () => {
      it('should have a listCustomers function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCustomers(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCustomer - errors', () => {
      it('should have a postCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.postCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postCustomer(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCustomer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomer - errors', () => {
      it('should have a getCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCustomer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.getCustomer('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomer - errors', () => {
      it('should have a deleteCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteCustomer(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.deleteCustomer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCustomer - errors', () => {
      it('should have a patchCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.patchCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchCustomer(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.patchCustomer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCustomer('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerFinancialDetailsForCustomer - errors', () => {
      it('should have a listCustomerFinancialDetailsForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomerFinancialDetailsForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCustomerFinancialDetailsForCustomer(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerFinancialDetailsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.listCustomerFinancialDetailsForCustomer('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerFinancialDetailsForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerFinancialDetailForCustomer - errors', () => {
      it('should have a getCustomerFinancialDetailForCustomer function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomerFinancialDetailForCustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCustomerFinancialDetailForCustomer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerFinancialDetailForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerId', (done) => {
        try {
          a.getCustomerFinancialDetailForCustomer('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerFinancialDetailForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerFinancialDetailId', (done) => {
        try {
          a.getCustomerFinancialDetailForCustomer('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerFinancialDetailId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerFinancialDetailForCustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerFinancialDetails - errors', () => {
      it('should have a listCustomerFinancialDetails function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomerFinancialDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCustomerFinancialDetails(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerFinancialDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerFinancialDetail - errors', () => {
      it('should have a getCustomerFinancialDetail function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomerFinancialDetail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCustomerFinancialDetail(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerFinancialDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerFinancialDetailId', (done) => {
        try {
          a.getCustomerFinancialDetail('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerFinancialDetailId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerFinancialDetail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVendors - errors', () => {
      it('should have a listVendors function', (done) => {
        try {
          assert.equal(true, typeof a.listVendors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listVendors(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listVendors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVendor - errors', () => {
      it('should have a postVendor function', (done) => {
        try {
          assert.equal(true, typeof a.postVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postVendor(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postVendor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendor - errors', () => {
      it('should have a getVendor function', (done) => {
        try {
          assert.equal(true, typeof a.getVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getVendor(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.getVendor('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVendor - errors', () => {
      it('should have a deleteVendor function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteVendor(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.deleteVendor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVendor - errors', () => {
      it('should have a patchVendor function', (done) => {
        try {
          assert.equal(true, typeof a.patchVendor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchVendor(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorId', (done) => {
        try {
          a.patchVendor('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchVendor('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchVendor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCompanyInformation - errors', () => {
      it('should have a listCompanyInformation function', (done) => {
        try {
          assert.equal(true, typeof a.listCompanyInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCompanyInformation(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCompanyInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCompanyInformation - errors', () => {
      it('should have a getCompanyInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getCompanyInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCompanyInformation(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCompanyInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyInformationId', (done) => {
        try {
          a.getCompanyInformation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'companyInformationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCompanyInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCompanyInformation - errors', () => {
      it('should have a patchCompanyInformation function', (done) => {
        try {
          assert.equal(true, typeof a.patchCompanyInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchCompanyInformation(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCompanyInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyInformationId', (done) => {
        try {
          a.patchCompanyInformation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'companyInformationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCompanyInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCompanyInformation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCompanyInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesInvoices - errors', () => {
      it('should have a listSalesInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesInvoices(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesInvoice - errors', () => {
      it('should have a postSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesInvoice(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesInvoice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesInvoice - errors', () => {
      it('should have a getSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesInvoice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.getSalesInvoice('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesInvoice - errors', () => {
      it('should have a deleteSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesInvoice(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.deleteSalesInvoice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesInvoice - errors', () => {
      it('should have a patchSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesInvoice(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.patchSalesInvoice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesInvoice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelAndSendActionSalesInvoices - errors', () => {
      it('should have a cancelAndSendActionSalesInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.cancelAndSendActionSalesInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.cancelAndSendActionSalesInvoices(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelAndSendActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.cancelAndSendActionSalesInvoices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelAndSendActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelActionSalesInvoices - errors', () => {
      it('should have a cancelActionSalesInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.cancelActionSalesInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.cancelActionSalesInvoices(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.cancelActionSalesInvoices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#makeCorrectiveCreditMemoActionSalesInvoices - errors', () => {
      it('should have a makeCorrectiveCreditMemoActionSalesInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.makeCorrectiveCreditMemoActionSalesInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.makeCorrectiveCreditMemoActionSalesInvoices(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-makeCorrectiveCreditMemoActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.makeCorrectiveCreditMemoActionSalesInvoices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-makeCorrectiveCreditMemoActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAndSendActionSalesInvoices - errors', () => {
      it('should have a postAndSendActionSalesInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.postAndSendActionSalesInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postAndSendActionSalesInvoices(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAndSendActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.postAndSendActionSalesInvoices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAndSendActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postActionSalesInvoices - errors', () => {
      it('should have a postActionSalesInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.postActionSalesInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postActionSalesInvoices(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.postActionSalesInvoices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendActionSalesInvoices - errors', () => {
      it('should have a sendActionSalesInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.sendActionSalesInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.sendActionSalesInvoices(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-sendActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.sendActionSalesInvoices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-sendActionSalesInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesInvoiceLinesForSalesInvoice - errors', () => {
      it('should have a listSalesInvoiceLinesForSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesInvoiceLinesForSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesInvoiceLinesForSalesInvoice(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesInvoiceLinesForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.listSalesInvoiceLinesForSalesInvoice('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesInvoiceLinesForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should have a postSalesInvoiceLineForSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesInvoiceLineForSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesInvoiceLineForSalesInvoice(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.postSalesInvoiceLineForSalesInvoice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesInvoiceLineForSalesInvoice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should have a getSalesInvoiceLineForSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesInvoiceLineForSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesInvoiceLineForSalesInvoice(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.getSalesInvoiceLineForSalesInvoice('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceLineId', (done) => {
        try {
          a.getSalesInvoiceLineForSalesInvoice('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should have a deleteSalesInvoiceLineForSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesInvoiceLineForSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesInvoiceLineForSalesInvoice(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.deleteSalesInvoiceLineForSalesInvoice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceLineId', (done) => {
        try {
          a.deleteSalesInvoiceLineForSalesInvoice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should have a patchSalesInvoiceLineForSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesInvoiceLineForSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesInvoiceLineForSalesInvoice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.patchSalesInvoiceLineForSalesInvoice('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceLineId', (done) => {
        try {
          a.patchSalesInvoiceLineForSalesInvoice('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesInvoiceLineForSalesInvoice('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoiceLineForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesInvoiceLines - errors', () => {
      it('should have a listSalesInvoiceLines function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesInvoiceLines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesInvoiceLines(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesInvoiceLines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesInvoiceLine - errors', () => {
      it('should have a postSalesInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesInvoiceLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesInvoiceLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesInvoiceLine - errors', () => {
      it('should have a getSalesInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesInvoiceLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceLineId', (done) => {
        try {
          a.getSalesInvoiceLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesInvoiceLine - errors', () => {
      it('should have a deleteSalesInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesInvoiceLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceLineId', (done) => {
        try {
          a.deleteSalesInvoiceLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesInvoiceLine - errors', () => {
      it('should have a patchSalesInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesInvoiceLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceLineId', (done) => {
        try {
          a.patchSalesInvoiceLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesInvoiceLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPdfDocumentForSalesInvoice - errors', () => {
      it('should have a listPdfDocumentForSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.listPdfDocumentForSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPdfDocumentForSalesInvoice(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.listPdfDocumentForSalesInvoice('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForSalesInvoice - errors', () => {
      it('should have a getPdfDocumentForSalesInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.getPdfDocumentForSalesInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPdfDocumentForSalesInvoice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesInvoiceId', (done) => {
        try {
          a.getPdfDocumentForSalesInvoice('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pdfDocumentId', (done) => {
        try {
          a.getPdfDocumentForSalesInvoice('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pdfDocumentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPdfDocument - errors', () => {
      it('should have a listPdfDocument function', (done) => {
        try {
          assert.equal(true, typeof a.listPdfDocument === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPdfDocument(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocument', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocument - errors', () => {
      it('should have a getPdfDocument function', (done) => {
        try {
          assert.equal(true, typeof a.getPdfDocument === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPdfDocument(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocument', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pdfDocumentId', (done) => {
        try {
          a.getPdfDocument('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pdfDocumentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocument', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPdfDocumentForSalesQuote - errors', () => {
      it('should have a listPdfDocumentForSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.listPdfDocumentForSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPdfDocumentForSalesQuote(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.listPdfDocumentForSalesQuote('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForSalesQuote - errors', () => {
      it('should have a getPdfDocumentForSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.getPdfDocumentForSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPdfDocumentForSalesQuote(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.getPdfDocumentForSalesQuote('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pdfDocumentId', (done) => {
        try {
          a.getPdfDocumentForSalesQuote('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pdfDocumentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPdfDocumentForSalesCreditMemo - errors', () => {
      it('should have a listPdfDocumentForSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.listPdfDocumentForSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPdfDocumentForSalesCreditMemo(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.listPdfDocumentForSalesCreditMemo('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForSalesCreditMemo - errors', () => {
      it('should have a getPdfDocumentForSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.getPdfDocumentForSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPdfDocumentForSalesCreditMemo(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.getPdfDocumentForSalesCreditMemo('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pdfDocumentId', (done) => {
        try {
          a.getPdfDocumentForSalesCreditMemo('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pdfDocumentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPdfDocumentForPurchaseInvoice - errors', () => {
      it('should have a listPdfDocumentForPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.listPdfDocumentForPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPdfDocumentForPurchaseInvoice(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.listPdfDocumentForPurchaseInvoice('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPdfDocumentForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForPurchaseInvoice - errors', () => {
      it('should have a getPdfDocumentForPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.getPdfDocumentForPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPdfDocumentForPurchaseInvoice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.getPdfDocumentForPurchaseInvoice('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pdfDocumentId', (done) => {
        try {
          a.getPdfDocumentForPurchaseInvoice('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pdfDocumentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPdfDocumentForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerPaymentJournals - errors', () => {
      it('should have a listCustomerPaymentJournals function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomerPaymentJournals === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCustomerPaymentJournals(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerPaymentJournals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCustomerPaymentJournal - errors', () => {
      it('should have a postCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.postCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postCustomerPaymentJournal(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCustomerPaymentJournal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerPaymentJournal - errors', () => {
      it('should have a getCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCustomerPaymentJournal(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.getCustomerPaymentJournal('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerPaymentJournal - errors', () => {
      it('should have a deleteCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteCustomerPaymentJournal(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.deleteCustomerPaymentJournal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCustomerPaymentJournal - errors', () => {
      it('should have a patchCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.patchCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchCustomerPaymentJournal(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.patchCustomerPaymentJournal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCustomerPaymentJournal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerPaymentsForCustomerPaymentJournal - errors', () => {
      it('should have a listCustomerPaymentsForCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomerPaymentsForCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCustomerPaymentsForCustomerPaymentJournal(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerPaymentsForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.listCustomerPaymentsForCustomerPaymentJournal('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerPaymentsForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should have a postCustomerPaymentForCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.postCustomerPaymentForCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postCustomerPaymentForCustomerPaymentJournal(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.postCustomerPaymentForCustomerPaymentJournal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCustomerPaymentForCustomerPaymentJournal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should have a getCustomerPaymentForCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomerPaymentForCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCustomerPaymentForCustomerPaymentJournal(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.getCustomerPaymentForCustomerPaymentJournal('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentId', (done) => {
        try {
          a.getCustomerPaymentForCustomerPaymentJournal('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should have a deleteCustomerPaymentForCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomerPaymentForCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteCustomerPaymentForCustomerPaymentJournal(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.deleteCustomerPaymentForCustomerPaymentJournal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentId', (done) => {
        try {
          a.deleteCustomerPaymentForCustomerPaymentJournal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'customerPaymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should have a patchCustomerPaymentForCustomerPaymentJournal function', (done) => {
        try {
          assert.equal(true, typeof a.patchCustomerPaymentForCustomerPaymentJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchCustomerPaymentForCustomerPaymentJournal(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentJournalId', (done) => {
        try {
          a.patchCustomerPaymentForCustomerPaymentJournal('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentJournalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentId', (done) => {
        try {
          a.patchCustomerPaymentForCustomerPaymentJournal('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCustomerPaymentForCustomerPaymentJournal('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPaymentForCustomerPaymentJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerPayments - errors', () => {
      it('should have a listCustomerPayments function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomerPayments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCustomerPayments(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerPayments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCustomerPayment - errors', () => {
      it('should have a postCustomerPayment function', (done) => {
        try {
          assert.equal(true, typeof a.postCustomerPayment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postCustomerPayment(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCustomerPayment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerPayment - errors', () => {
      it('should have a getCustomerPayment function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomerPayment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCustomerPayment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentId', (done) => {
        try {
          a.getCustomerPayment('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerPayment - errors', () => {
      it('should have a deleteCustomerPayment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomerPayment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteCustomerPayment(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentId', (done) => {
        try {
          a.deleteCustomerPayment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customerPaymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCustomerPayment - errors', () => {
      it('should have a patchCustomerPayment function', (done) => {
        try {
          assert.equal(true, typeof a.patchCustomerPayment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchCustomerPayment(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerPaymentId', (done) => {
        try {
          a.patchCustomerPayment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerPaymentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCustomerPayment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCustomerPayment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAccounts - errors', () => {
      it('should have a listAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.listAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listAccounts(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccount - errors', () => {
      it('should have a getAccount function', (done) => {
        try {
          assert.equal(true, typeof a.getAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accountId', (done) => {
        try {
          a.getAccount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'accountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTaxGroups - errors', () => {
      it('should have a listTaxGroups function', (done) => {
        try {
          assert.equal(true, typeof a.listTaxGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listTaxGroups(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listTaxGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTaxGroup - errors', () => {
      it('should have a postTaxGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postTaxGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postTaxGroup(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTaxGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaxGroup - errors', () => {
      it('should have a getTaxGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getTaxGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getTaxGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taxGroupId', (done) => {
        try {
          a.getTaxGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taxGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTaxGroup - errors', () => {
      it('should have a deleteTaxGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTaxGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteTaxGroup(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taxGroupId', (done) => {
        try {
          a.deleteTaxGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'taxGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTaxGroup - errors', () => {
      it('should have a patchTaxGroup function', (done) => {
        try {
          assert.equal(true, typeof a.patchTaxGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchTaxGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taxGroupId', (done) => {
        try {
          a.patchTaxGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taxGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchTaxGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTaxGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listJournals - errors', () => {
      it('should have a listJournals function', (done) => {
        try {
          assert.equal(true, typeof a.listJournals === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listJournals(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listJournals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postJournal - errors', () => {
      it('should have a postJournal function', (done) => {
        try {
          assert.equal(true, typeof a.postJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postJournal(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postJournal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJournal - errors', () => {
      it('should have a getJournal function', (done) => {
        try {
          assert.equal(true, typeof a.getJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getJournal(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.getJournal('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJournal - errors', () => {
      it('should have a deleteJournal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteJournal(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.deleteJournal('fakeparam', null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchJournal - errors', () => {
      it('should have a patchJournal function', (done) => {
        try {
          assert.equal(true, typeof a.patchJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchJournal(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.patchJournal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchJournal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postActionJournals - errors', () => {
      it('should have a postActionJournals function', (done) => {
        try {
          assert.equal(true, typeof a.postActionJournals === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postActionJournals(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionJournals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.postActionJournals('fakeparam', null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionJournals', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listJournalLinesForJournal - errors', () => {
      it('should have a listJournalLinesForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.listJournalLinesForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listJournalLinesForJournal(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listJournalLinesForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.listJournalLinesForJournal('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listJournalLinesForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postJournalLineForJournal - errors', () => {
      it('should have a postJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.postJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postJournalLineForJournal(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.postJournalLineForJournal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postJournalLineForJournal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJournalLineForJournal - errors', () => {
      it('should have a getJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.getJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getJournalLineForJournal(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.getJournalLineForJournal('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.getJournalLineForJournal('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJournalLineForJournal - errors', () => {
      it('should have a deleteJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteJournalLineForJournal(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.deleteJournalLineForJournal('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.deleteJournalLineForJournal('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchJournalLineForJournal - errors', () => {
      it('should have a patchJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.patchJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchJournalLineForJournal(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.patchJournalLineForJournal('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.patchJournalLineForJournal('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listJournalLines - errors', () => {
      it('should have a listJournalLines function', (done) => {
        try {
          assert.equal(true, typeof a.listJournalLines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listJournalLines(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listJournalLines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postJournalLine - errors', () => {
      it('should have a postJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.postJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postJournalLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postJournalLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJournalLine - errors', () => {
      it('should have a getJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.getJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getJournalLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.getJournalLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJournalLine - errors', () => {
      it('should have a deleteJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteJournalLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.deleteJournalLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchJournalLine - errors', () => {
      it('should have a patchJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchJournalLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.patchJournalLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchJournalLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachmentsForJournalLineForJournal - errors', () => {
      it('should have a listAttachmentsForJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.listAttachmentsForJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listAttachmentsForJournalLineForJournal(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.listAttachmentsForJournalLineForJournal('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.listAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAttachmentsForJournalLineForJournal - errors', () => {
      it('should have a postAttachmentsForJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.postAttachmentsForJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postAttachmentsForJournalLineForJournal(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.postAttachmentsForJournalLineForJournal('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.postAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentsForJournalLineForJournal - errors', () => {
      it('should have a getAttachmentsForJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachmentsForJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getAttachmentsForJournalLineForJournal(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.getAttachmentsForJournalLineForJournal('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.getAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.getAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.getAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachmentsForJournalLineForJournal - errors', () => {
      it('should have a deleteAttachmentsForJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAttachmentsForJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteAttachmentsForJournalLineForJournal(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.deleteAttachmentsForJournalLineForJournal('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.deleteAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.deleteAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.deleteAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAttachmentsForJournalLineForJournal - errors', () => {
      it('should have a patchAttachmentsForJournalLineForJournal function', (done) => {
        try {
          assert.equal(true, typeof a.patchAttachmentsForJournalLineForJournal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchAttachmentsForJournalLineForJournal(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalId', (done) => {
        try {
          a.patchAttachmentsForJournalLineForJournal('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.patchAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.patchAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.patchAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAttachmentsForJournalLineForJournal('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLineForJournal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachmentsForJournalLine - errors', () => {
      it('should have a listAttachmentsForJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.listAttachmentsForJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listAttachmentsForJournalLine(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.listAttachmentsForJournalLine('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAttachmentsForJournalLine - errors', () => {
      it('should have a postAttachmentsForJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.postAttachmentsForJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postAttachmentsForJournalLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.postAttachmentsForJournalLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAttachmentsForJournalLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentsForJournalLine - errors', () => {
      it('should have a getAttachmentsForJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachmentsForJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getAttachmentsForJournalLine(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.getAttachmentsForJournalLine('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.getAttachmentsForJournalLine('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.getAttachmentsForJournalLine('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachmentsForJournalLine - errors', () => {
      it('should have a deleteAttachmentsForJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAttachmentsForJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteAttachmentsForJournalLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.deleteAttachmentsForJournalLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.deleteAttachmentsForJournalLine('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.deleteAttachmentsForJournalLine('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAttachmentsForJournalLine - errors', () => {
      it('should have a patchAttachmentsForJournalLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchAttachmentsForJournalLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchAttachmentsForJournalLine(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing journalLineId', (done) => {
        try {
          a.patchAttachmentsForJournalLine('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'journalLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.patchAttachmentsForJournalLine('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.patchAttachmentsForJournalLine('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAttachmentsForJournalLine('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachmentsForJournalLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachments - errors', () => {
      it('should have a listAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.listAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listAttachments(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAttachments - errors', () => {
      it('should have a postAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.postAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postAttachments(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAttachments('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachments - errors', () => {
      it('should have a getAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getAttachments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.getAttachments('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.getAttachments('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachments - errors', () => {
      it('should have a deleteAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteAttachments(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.deleteAttachments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.deleteAttachments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchAttachments - errors', () => {
      it('should have a patchAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.patchAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchAttachments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsParentId', (done) => {
        try {
          a.patchAttachments('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentsParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentsId', (done) => {
        try {
          a.patchAttachments('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchAttachments('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEmployees - errors', () => {
      it('should have a listEmployees function', (done) => {
        try {
          assert.equal(true, typeof a.listEmployees === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listEmployees(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listEmployees', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEmployee - errors', () => {
      it('should have a postEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.postEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postEmployee(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postEmployee('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmployee - errors', () => {
      it('should have a getEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.getEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getEmployee(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.getEmployee('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEmployee - errors', () => {
      it('should have a deleteEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteEmployee(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.deleteEmployee('fakeparam', null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchEmployee - errors', () => {
      it('should have a patchEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.patchEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchEmployee(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.patchEmployee('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchEmployee('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTimeRegistrationEntriesForEmployee - errors', () => {
      it('should have a listTimeRegistrationEntriesForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.listTimeRegistrationEntriesForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listTimeRegistrationEntriesForEmployee(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listTimeRegistrationEntriesForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.listTimeRegistrationEntriesForEmployee('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listTimeRegistrationEntriesForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTimeRegistrationEntryForEmployee - errors', () => {
      it('should have a postTimeRegistrationEntryForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.postTimeRegistrationEntryForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postTimeRegistrationEntryForEmployee(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.postTimeRegistrationEntryForEmployee('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTimeRegistrationEntryForEmployee('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRegistrationEntryForEmployee - errors', () => {
      it('should have a getTimeRegistrationEntryForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeRegistrationEntryForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getTimeRegistrationEntryForEmployee(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.getTimeRegistrationEntryForEmployee('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeRegistrationEntryId', (done) => {
        try {
          a.getTimeRegistrationEntryForEmployee('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeRegistrationEntryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeRegistrationEntryForEmployee - errors', () => {
      it('should have a deleteTimeRegistrationEntryForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTimeRegistrationEntryForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteTimeRegistrationEntryForEmployee(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.deleteTimeRegistrationEntryForEmployee('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeRegistrationEntryId', (done) => {
        try {
          a.deleteTimeRegistrationEntryForEmployee('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'timeRegistrationEntryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTimeRegistrationEntryForEmployee - errors', () => {
      it('should have a patchTimeRegistrationEntryForEmployee function', (done) => {
        try {
          assert.equal(true, typeof a.patchTimeRegistrationEntryForEmployee === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchTimeRegistrationEntryForEmployee(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing employeeId', (done) => {
        try {
          a.patchTimeRegistrationEntryForEmployee('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'employeeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeRegistrationEntryId', (done) => {
        try {
          a.patchTimeRegistrationEntryForEmployee('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'timeRegistrationEntryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchTimeRegistrationEntryForEmployee('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTimeRegistrationEntryForEmployee', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTimeRegistrationEntries - errors', () => {
      it('should have a listTimeRegistrationEntries function', (done) => {
        try {
          assert.equal(true, typeof a.listTimeRegistrationEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listTimeRegistrationEntries(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listTimeRegistrationEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTimeRegistrationEntry - errors', () => {
      it('should have a postTimeRegistrationEntry function', (done) => {
        try {
          assert.equal(true, typeof a.postTimeRegistrationEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postTimeRegistrationEntry(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTimeRegistrationEntry('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRegistrationEntry - errors', () => {
      it('should have a getTimeRegistrationEntry function', (done) => {
        try {
          assert.equal(true, typeof a.getTimeRegistrationEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getTimeRegistrationEntry(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeRegistrationEntryId', (done) => {
        try {
          a.getTimeRegistrationEntry('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeRegistrationEntryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeRegistrationEntry - errors', () => {
      it('should have a deleteTimeRegistrationEntry function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTimeRegistrationEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteTimeRegistrationEntry(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeRegistrationEntryId', (done) => {
        try {
          a.deleteTimeRegistrationEntry('fakeparam', null, (data, error) => {
            try {
              const displayE = 'timeRegistrationEntryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTimeRegistrationEntry - errors', () => {
      it('should have a patchTimeRegistrationEntry function', (done) => {
        try {
          assert.equal(true, typeof a.patchTimeRegistrationEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchTimeRegistrationEntry(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeRegistrationEntryId', (done) => {
        try {
          a.patchTimeRegistrationEntry('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'timeRegistrationEntryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchTimeRegistrationEntry('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTimeRegistrationEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGeneralLedgerEntries - errors', () => {
      it('should have a listGeneralLedgerEntries function', (done) => {
        try {
          assert.equal(true, typeof a.listGeneralLedgerEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listGeneralLedgerEntries(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listGeneralLedgerEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeneralLedgerEntry - errors', () => {
      it('should have a getGeneralLedgerEntry function', (done) => {
        try {
          assert.equal(true, typeof a.getGeneralLedgerEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getGeneralLedgerEntry(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getGeneralLedgerEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generalLedgerEntryId', (done) => {
        try {
          a.getGeneralLedgerEntry('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'generalLedgerEntryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getGeneralLedgerEntry', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCurrencies - errors', () => {
      it('should have a listCurrencies function', (done) => {
        try {
          assert.equal(true, typeof a.listCurrencies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCurrencies(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCurrencies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCurrency - errors', () => {
      it('should have a postCurrency function', (done) => {
        try {
          assert.equal(true, typeof a.postCurrency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postCurrency(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCurrency('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrency - errors', () => {
      it('should have a getCurrency function', (done) => {
        try {
          assert.equal(true, typeof a.getCurrency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCurrency(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing currencyId', (done) => {
        try {
          a.getCurrency('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'currencyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCurrency - errors', () => {
      it('should have a deleteCurrency function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCurrency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteCurrency(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing currencyId', (done) => {
        try {
          a.deleteCurrency('fakeparam', null, (data, error) => {
            try {
              const displayE = 'currencyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCurrency - errors', () => {
      it('should have a patchCurrency function', (done) => {
        try {
          assert.equal(true, typeof a.patchCurrency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchCurrency(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing currencyId', (done) => {
        try {
          a.patchCurrency('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'currencyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCurrency('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCurrency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPaymentMethods - errors', () => {
      it('should have a listPaymentMethods function', (done) => {
        try {
          assert.equal(true, typeof a.listPaymentMethods === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPaymentMethods(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPaymentMethods', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPaymentMethod - errors', () => {
      it('should have a postPaymentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.postPaymentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postPaymentMethod(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postPaymentMethod('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPaymentMethod - errors', () => {
      it('should have a getPaymentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.getPaymentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPaymentMethod(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paymentMethodId', (done) => {
        try {
          a.getPaymentMethod('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'paymentMethodId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePaymentMethod - errors', () => {
      it('should have a deletePaymentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.deletePaymentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePaymentMethod(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paymentMethodId', (done) => {
        try {
          a.deletePaymentMethod('fakeparam', null, (data, error) => {
            try {
              const displayE = 'paymentMethodId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPaymentMethod - errors', () => {
      it('should have a patchPaymentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.patchPaymentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPaymentMethod(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paymentMethodId', (done) => {
        try {
          a.patchPaymentMethod('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'paymentMethodId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPaymentMethod('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPaymentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDimensions - errors', () => {
      it('should have a listDimensions function', (done) => {
        try {
          assert.equal(true, typeof a.listDimensions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDimensions(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDimensions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDimension - errors', () => {
      it('should have a getDimension function', (done) => {
        try {
          assert.equal(true, typeof a.getDimension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDimension(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionId', (done) => {
        try {
          a.getDimension('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDimensionValuesForDimension - errors', () => {
      it('should have a listDimensionValuesForDimension function', (done) => {
        try {
          assert.equal(true, typeof a.listDimensionValuesForDimension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDimensionValuesForDimension(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDimensionValuesForDimension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionId', (done) => {
        try {
          a.listDimensionValuesForDimension('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'dimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDimensionValuesForDimension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDimensionValueForDimension - errors', () => {
      it('should have a getDimensionValueForDimension function', (done) => {
        try {
          assert.equal(true, typeof a.getDimensionValueForDimension === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDimensionValueForDimension(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionValueForDimension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionId', (done) => {
        try {
          a.getDimensionValueForDimension('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dimensionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionValueForDimension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionValueId', (done) => {
        try {
          a.getDimensionValueForDimension('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dimensionValueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionValueForDimension', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDimensionValues - errors', () => {
      it('should have a listDimensionValues function', (done) => {
        try {
          assert.equal(true, typeof a.listDimensionValues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDimensionValues(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDimensionValues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDimensionValue - errors', () => {
      it('should have a getDimensionValue function', (done) => {
        try {
          assert.equal(true, typeof a.getDimensionValue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDimensionValue(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionValueId', (done) => {
        try {
          a.getDimensionValue('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dimensionValueId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionValue', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDimensionLines - errors', () => {
      it('should have a listDimensionLines function', (done) => {
        try {
          assert.equal(true, typeof a.listDimensionLines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listDimensionLines(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listDimensionLines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDimensionLine - errors', () => {
      it('should have a postDimensionLine function', (done) => {
        try {
          assert.equal(true, typeof a.postDimensionLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postDimensionLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postDimensionLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDimensionLine - errors', () => {
      it('should have a getDimensionLine function', (done) => {
        try {
          assert.equal(true, typeof a.getDimensionLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getDimensionLine(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionLineParentId', (done) => {
        try {
          a.getDimensionLine('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'dimensionLineParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionLineId', (done) => {
        try {
          a.getDimensionLine('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dimensionLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDimensionLine - errors', () => {
      it('should have a deleteDimensionLine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDimensionLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteDimensionLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionLineParentId', (done) => {
        try {
          a.deleteDimensionLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dimensionLineParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionLineId', (done) => {
        try {
          a.deleteDimensionLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'dimensionLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDimensionLine - errors', () => {
      it('should have a patchDimensionLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchDimensionLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchDimensionLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionLineParentId', (done) => {
        try {
          a.patchDimensionLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dimensionLineParentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dimensionLineId', (done) => {
        try {
          a.patchDimensionLine('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dimensionLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchDimensionLine('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchDimensionLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPaymentTerms - errors', () => {
      it('should have a listPaymentTerms function', (done) => {
        try {
          assert.equal(true, typeof a.listPaymentTerms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPaymentTerms(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPaymentTerms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPaymentTerm - errors', () => {
      it('should have a postPaymentTerm function', (done) => {
        try {
          assert.equal(true, typeof a.postPaymentTerm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postPaymentTerm(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postPaymentTerm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPaymentTerm - errors', () => {
      it('should have a getPaymentTerm function', (done) => {
        try {
          assert.equal(true, typeof a.getPaymentTerm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPaymentTerm(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paymentTermId', (done) => {
        try {
          a.getPaymentTerm('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'paymentTermId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePaymentTerm - errors', () => {
      it('should have a deletePaymentTerm function', (done) => {
        try {
          assert.equal(true, typeof a.deletePaymentTerm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePaymentTerm(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paymentTermId', (done) => {
        try {
          a.deletePaymentTerm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'paymentTermId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPaymentTerm - errors', () => {
      it('should have a patchPaymentTerm function', (done) => {
        try {
          assert.equal(true, typeof a.patchPaymentTerm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPaymentTerm(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing paymentTermId', (done) => {
        try {
          a.patchPaymentTerm('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'paymentTermId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPaymentTerm('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPaymentTerm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listShipmentMethods - errors', () => {
      it('should have a listShipmentMethods function', (done) => {
        try {
          assert.equal(true, typeof a.listShipmentMethods === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listShipmentMethods(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listShipmentMethods', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postShipmentMethod - errors', () => {
      it('should have a postShipmentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.postShipmentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postShipmentMethod(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postShipmentMethod('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShipmentMethod - errors', () => {
      it('should have a getShipmentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.getShipmentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getShipmentMethod(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shipmentMethodId', (done) => {
        try {
          a.getShipmentMethod('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'shipmentMethodId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteShipmentMethod - errors', () => {
      it('should have a deleteShipmentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.deleteShipmentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteShipmentMethod(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shipmentMethodId', (done) => {
        try {
          a.deleteShipmentMethod('fakeparam', null, (data, error) => {
            try {
              const displayE = 'shipmentMethodId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchShipmentMethod - errors', () => {
      it('should have a patchShipmentMethod function', (done) => {
        try {
          assert.equal(true, typeof a.patchShipmentMethod === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchShipmentMethod(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing shipmentMethodId', (done) => {
        try {
          a.patchShipmentMethod('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'shipmentMethodId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchShipmentMethod('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchShipmentMethod', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listItemCategories - errors', () => {
      it('should have a listItemCategories function', (done) => {
        try {
          assert.equal(true, typeof a.listItemCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listItemCategories(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listItemCategories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postItemCategory - errors', () => {
      it('should have a postItemCategory function', (done) => {
        try {
          assert.equal(true, typeof a.postItemCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postItemCategory(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postItemCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItemCategory - errors', () => {
      it('should have a getItemCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getItemCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getItemCategory(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemCategoryId', (done) => {
        try {
          a.getItemCategory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteItemCategory - errors', () => {
      it('should have a deleteItemCategory function', (done) => {
        try {
          assert.equal(true, typeof a.deleteItemCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteItemCategory(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemCategoryId', (done) => {
        try {
          a.deleteItemCategory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'itemCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchItemCategory - errors', () => {
      it('should have a patchItemCategory function', (done) => {
        try {
          assert.equal(true, typeof a.patchItemCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchItemCategory(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing itemCategoryId', (done) => {
        try {
          a.patchItemCategory('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'itemCategoryId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchItemCategory('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchItemCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCashFlowStatement - errors', () => {
      it('should have a listCashFlowStatement function', (done) => {
        try {
          assert.equal(true, typeof a.listCashFlowStatement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCashFlowStatement(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCashFlowStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCashFlowStatement - errors', () => {
      it('should have a getCashFlowStatement function', (done) => {
        try {
          assert.equal(true, typeof a.getCashFlowStatement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCashFlowStatement(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCashFlowStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cashFlowStatementLineNumber', (done) => {
        try {
          a.getCashFlowStatement('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'cashFlowStatementLineNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCashFlowStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCountriesRegions - errors', () => {
      it('should have a listCountriesRegions function', (done) => {
        try {
          assert.equal(true, typeof a.listCountriesRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCountriesRegions(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCountriesRegions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCountryRegion - errors', () => {
      it('should have a postCountryRegion function', (done) => {
        try {
          assert.equal(true, typeof a.postCountryRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postCountryRegion(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postCountryRegion('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountryRegion - errors', () => {
      it('should have a getCountryRegion function', (done) => {
        try {
          assert.equal(true, typeof a.getCountryRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCountryRegion(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing countryRegionId', (done) => {
        try {
          a.getCountryRegion('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'countryRegionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCountryRegion - errors', () => {
      it('should have a deleteCountryRegion function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCountryRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteCountryRegion(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing countryRegionId', (done) => {
        try {
          a.deleteCountryRegion('fakeparam', null, (data, error) => {
            try {
              const displayE = 'countryRegionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCountryRegion - errors', () => {
      it('should have a patchCountryRegion function', (done) => {
        try {
          assert.equal(true, typeof a.patchCountryRegion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchCountryRegion(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing countryRegionId', (done) => {
        try {
          a.patchCountryRegion('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'countryRegionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchCountryRegion('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchCountryRegion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesOrders - errors', () => {
      it('should have a listSalesOrders function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesOrders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesOrders(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesOrders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesOrder - errors', () => {
      it('should have a postSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesOrder(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesOrder('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesOrder - errors', () => {
      it('should have a getSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesOrder(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.getSalesOrder('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesOrder - errors', () => {
      it('should have a deleteSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesOrder(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.deleteSalesOrder('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesOrder - errors', () => {
      it('should have a patchSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesOrder(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.patchSalesOrder('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesOrder('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#shipAndInvoiceActionSalesOrders - errors', () => {
      it('should have a shipAndInvoiceActionSalesOrders function', (done) => {
        try {
          assert.equal(true, typeof a.shipAndInvoiceActionSalesOrders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.shipAndInvoiceActionSalesOrders(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-shipAndInvoiceActionSalesOrders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.shipAndInvoiceActionSalesOrders('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-shipAndInvoiceActionSalesOrders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesOrderLinesForSalesOrder - errors', () => {
      it('should have a listSalesOrderLinesForSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesOrderLinesForSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesOrderLinesForSalesOrder(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesOrderLinesForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.listSalesOrderLinesForSalesOrder('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesOrderLinesForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesOrderLineForSalesOrder - errors', () => {
      it('should have a postSalesOrderLineForSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesOrderLineForSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesOrderLineForSalesOrder(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.postSalesOrderLineForSalesOrder('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesOrderLineForSalesOrder('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesOrderLineForSalesOrder - errors', () => {
      it('should have a getSalesOrderLineForSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesOrderLineForSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesOrderLineForSalesOrder(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.getSalesOrderLineForSalesOrder('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderLineId', (done) => {
        try {
          a.getSalesOrderLineForSalesOrder('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesOrderLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesOrderLineForSalesOrder - errors', () => {
      it('should have a deleteSalesOrderLineForSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesOrderLineForSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesOrderLineForSalesOrder(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.deleteSalesOrderLineForSalesOrder('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderLineId', (done) => {
        try {
          a.deleteSalesOrderLineForSalesOrder('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesOrderLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesOrderLineForSalesOrder - errors', () => {
      it('should have a patchSalesOrderLineForSalesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesOrderLineForSalesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesOrderLineForSalesOrder(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderId', (done) => {
        try {
          a.patchSalesOrderLineForSalesOrder('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesOrderId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderLineId', (done) => {
        try {
          a.patchSalesOrderLineForSalesOrder('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesOrderLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesOrderLineForSalesOrder('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrderLineForSalesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesOrderLines - errors', () => {
      it('should have a listSalesOrderLines function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesOrderLines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesOrderLines(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesOrderLines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesOrderLine - errors', () => {
      it('should have a postSalesOrderLine function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesOrderLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesOrderLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesOrderLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesOrderLine - errors', () => {
      it('should have a getSalesOrderLine function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesOrderLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesOrderLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderLineId', (done) => {
        try {
          a.getSalesOrderLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesOrderLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesOrderLine - errors', () => {
      it('should have a deleteSalesOrderLine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesOrderLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesOrderLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderLineId', (done) => {
        try {
          a.deleteSalesOrderLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesOrderLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesOrderLine - errors', () => {
      it('should have a patchSalesOrderLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesOrderLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesOrderLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesOrderLineId', (done) => {
        try {
          a.patchSalesOrderLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesOrderLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesOrderLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesOrderLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listRetainedEarningsStatement - errors', () => {
      it('should have a listRetainedEarningsStatement function', (done) => {
        try {
          assert.equal(true, typeof a.listRetainedEarningsStatement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listRetainedEarningsStatement(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listRetainedEarningsStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRetainedEarningsStatement - errors', () => {
      it('should have a getRetainedEarningsStatement function', (done) => {
        try {
          assert.equal(true, typeof a.getRetainedEarningsStatement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getRetainedEarningsStatement(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getRetainedEarningsStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing retainedEarningsStatementLineNumber', (done) => {
        try {
          a.getRetainedEarningsStatement('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'retainedEarningsStatementLineNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getRetainedEarningsStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUnitsOfMeasure - errors', () => {
      it('should have a listUnitsOfMeasure function', (done) => {
        try {
          assert.equal(true, typeof a.listUnitsOfMeasure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listUnitsOfMeasure(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listUnitsOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUnitOfMeasure - errors', () => {
      it('should have a postUnitOfMeasure function', (done) => {
        try {
          assert.equal(true, typeof a.postUnitOfMeasure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postUnitOfMeasure(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postUnitOfMeasure('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnitOfMeasure - errors', () => {
      it('should have a getUnitOfMeasure function', (done) => {
        try {
          assert.equal(true, typeof a.getUnitOfMeasure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getUnitOfMeasure(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing unitOfMeasureId', (done) => {
        try {
          a.getUnitOfMeasure('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'unitOfMeasureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnitOfMeasure - errors', () => {
      it('should have a deleteUnitOfMeasure function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUnitOfMeasure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteUnitOfMeasure(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing unitOfMeasureId', (done) => {
        try {
          a.deleteUnitOfMeasure('fakeparam', null, (data, error) => {
            try {
              const displayE = 'unitOfMeasureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUnitOfMeasure - errors', () => {
      it('should have a patchUnitOfMeasure function', (done) => {
        try {
          assert.equal(true, typeof a.patchUnitOfMeasure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchUnitOfMeasure(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing unitOfMeasureId', (done) => {
        try {
          a.patchUnitOfMeasure('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'unitOfMeasureId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchUnitOfMeasure('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchUnitOfMeasure', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAgedAccountsReceivable - errors', () => {
      it('should have a listAgedAccountsReceivable function', (done) => {
        try {
          assert.equal(true, typeof a.listAgedAccountsReceivable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listAgedAccountsReceivable(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAgedAccountsReceivable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAgedAccountsReceivable - errors', () => {
      it('should have a getAgedAccountsReceivable function', (done) => {
        try {
          assert.equal(true, typeof a.getAgedAccountsReceivable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getAgedAccountsReceivable(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAgedAccountsReceivable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agedAccountsReceivableCustomerId', (done) => {
        try {
          a.getAgedAccountsReceivable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'agedAccountsReceivableCustomerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAgedAccountsReceivable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAgedAccountsPayable - errors', () => {
      it('should have a listAgedAccountsPayable function', (done) => {
        try {
          assert.equal(true, typeof a.listAgedAccountsPayable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listAgedAccountsPayable(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listAgedAccountsPayable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAgedAccountsPayable - errors', () => {
      it('should have a getAgedAccountsPayable function', (done) => {
        try {
          assert.equal(true, typeof a.getAgedAccountsPayable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getAgedAccountsPayable(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAgedAccountsPayable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing agedAccountsPayableVendorId', (done) => {
        try {
          a.getAgedAccountsPayable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'agedAccountsPayableVendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getAgedAccountsPayable', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBalanceSheet - errors', () => {
      it('should have a listBalanceSheet function', (done) => {
        try {
          assert.equal(true, typeof a.listBalanceSheet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listBalanceSheet(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listBalanceSheet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBalanceSheet - errors', () => {
      it('should have a getBalanceSheet function', (done) => {
        try {
          assert.equal(true, typeof a.getBalanceSheet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getBalanceSheet(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getBalanceSheet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing balanceSheetLineNumber', (done) => {
        try {
          a.getBalanceSheet('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'balanceSheetLineNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getBalanceSheet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTrialBalance - errors', () => {
      it('should have a listTrialBalance function', (done) => {
        try {
          assert.equal(true, typeof a.listTrialBalance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listTrialBalance(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listTrialBalance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTrialBalance - errors', () => {
      it('should have a getTrialBalance function', (done) => {
        try {
          assert.equal(true, typeof a.getTrialBalance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getTrialBalance(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTrialBalance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trialBalanceNumber', (done) => {
        try {
          a.getTrialBalance('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'trialBalanceNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTrialBalance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listIncomeStatement - errors', () => {
      it('should have a listIncomeStatement function', (done) => {
        try {
          assert.equal(true, typeof a.listIncomeStatement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listIncomeStatement(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listIncomeStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncomeStatement - errors', () => {
      it('should have a getIncomeStatement function', (done) => {
        try {
          assert.equal(true, typeof a.getIncomeStatement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getIncomeStatement(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getIncomeStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incomeStatementLineNumber', (done) => {
        try {
          a.getIncomeStatement('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'incomeStatementLineNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getIncomeStatement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTaxAreas - errors', () => {
      it('should have a listTaxAreas function', (done) => {
        try {
          assert.equal(true, typeof a.listTaxAreas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listTaxAreas(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listTaxAreas', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTaxArea - errors', () => {
      it('should have a postTaxArea function', (done) => {
        try {
          assert.equal(true, typeof a.postTaxArea === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postTaxArea(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTaxArea('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaxArea - errors', () => {
      it('should have a getTaxArea function', (done) => {
        try {
          assert.equal(true, typeof a.getTaxArea === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getTaxArea(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taxAreaId', (done) => {
        try {
          a.getTaxArea('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taxAreaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTaxArea - errors', () => {
      it('should have a deleteTaxArea function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTaxArea === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteTaxArea(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taxAreaId', (done) => {
        try {
          a.deleteTaxArea('fakeparam', null, (data, error) => {
            try {
              const displayE = 'taxAreaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTaxArea - errors', () => {
      it('should have a patchTaxArea function', (done) => {
        try {
          assert.equal(true, typeof a.patchTaxArea === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchTaxArea(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taxAreaId', (done) => {
        try {
          a.patchTaxArea('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'taxAreaId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchTaxArea('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchTaxArea', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesQuotes - errors', () => {
      it('should have a listSalesQuotes function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesQuotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesQuotes(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesQuotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesQuote - errors', () => {
      it('should have a postSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesQuote(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesQuote('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesQuote - errors', () => {
      it('should have a getSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesQuote(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.getSalesQuote('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesQuote - errors', () => {
      it('should have a deleteSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesQuote(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.deleteSalesQuote('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesQuote - errors', () => {
      it('should have a patchSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesQuote(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.patchSalesQuote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesQuote('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#makeInvoiceActionSalesQuotes - errors', () => {
      it('should have a makeInvoiceActionSalesQuotes function', (done) => {
        try {
          assert.equal(true, typeof a.makeInvoiceActionSalesQuotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.makeInvoiceActionSalesQuotes(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-makeInvoiceActionSalesQuotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.makeInvoiceActionSalesQuotes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-makeInvoiceActionSalesQuotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#makeOrderActionSalesQuotes - errors', () => {
      it('should have a makeOrderActionSalesQuotes function', (done) => {
        try {
          assert.equal(true, typeof a.makeOrderActionSalesQuotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.makeOrderActionSalesQuotes(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-makeOrderActionSalesQuotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.makeOrderActionSalesQuotes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-makeOrderActionSalesQuotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendActionSalesQuotes - errors', () => {
      it('should have a sendActionSalesQuotes function', (done) => {
        try {
          assert.equal(true, typeof a.sendActionSalesQuotes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.sendActionSalesQuotes(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-sendActionSalesQuotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.sendActionSalesQuotes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-sendActionSalesQuotes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesQuoteLinesForSalesQuote - errors', () => {
      it('should have a listSalesQuoteLinesForSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesQuoteLinesForSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesQuoteLinesForSalesQuote(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesQuoteLinesForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.listSalesQuoteLinesForSalesQuote('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesQuoteLinesForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesQuoteLineForSalesQuote - errors', () => {
      it('should have a postSalesQuoteLineForSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesQuoteLineForSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesQuoteLineForSalesQuote(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.postSalesQuoteLineForSalesQuote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesQuoteLineForSalesQuote('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesQuoteLineForSalesQuote - errors', () => {
      it('should have a getSalesQuoteLineForSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesQuoteLineForSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesQuoteLineForSalesQuote(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.getSalesQuoteLineForSalesQuote('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteLineId', (done) => {
        try {
          a.getSalesQuoteLineForSalesQuote('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesQuoteLineForSalesQuote - errors', () => {
      it('should have a deleteSalesQuoteLineForSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesQuoteLineForSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesQuoteLineForSalesQuote(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.deleteSalesQuoteLineForSalesQuote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteLineId', (done) => {
        try {
          a.deleteSalesQuoteLineForSalesQuote('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesQuoteLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesQuoteLineForSalesQuote - errors', () => {
      it('should have a patchSalesQuoteLineForSalesQuote function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesQuoteLineForSalesQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesQuoteLineForSalesQuote(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteId', (done) => {
        try {
          a.patchSalesQuoteLineForSalesQuote('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteLineId', (done) => {
        try {
          a.patchSalesQuoteLineForSalesQuote('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesQuoteLineForSalesQuote('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuoteLineForSalesQuote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesQuoteLines - errors', () => {
      it('should have a listSalesQuoteLines function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesQuoteLines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesQuoteLines(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesQuoteLines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesQuoteLine - errors', () => {
      it('should have a postSalesQuoteLine function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesQuoteLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesQuoteLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesQuoteLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesQuoteLine - errors', () => {
      it('should have a getSalesQuoteLine function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesQuoteLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesQuoteLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteLineId', (done) => {
        try {
          a.getSalesQuoteLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesQuoteLine - errors', () => {
      it('should have a deleteSalesQuoteLine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesQuoteLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesQuoteLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteLineId', (done) => {
        try {
          a.deleteSalesQuoteLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesQuoteLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesQuoteLine - errors', () => {
      it('should have a patchSalesQuoteLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesQuoteLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesQuoteLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesQuoteLineId', (done) => {
        try {
          a.patchSalesQuoteLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesQuoteLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesQuoteLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesQuoteLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesCreditMemos - errors', () => {
      it('should have a listSalesCreditMemos function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesCreditMemos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesCreditMemos(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesCreditMemo - errors', () => {
      it('should have a postSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesCreditMemo(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesCreditMemo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesCreditMemo - errors', () => {
      it('should have a getSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesCreditMemo(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.getSalesCreditMemo('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesCreditMemo - errors', () => {
      it('should have a deleteSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesCreditMemo(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.deleteSalesCreditMemo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesCreditMemo - errors', () => {
      it('should have a patchSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesCreditMemo(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.patchSalesCreditMemo('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesCreditMemo('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelAndSendActionSalesCreditMemos - errors', () => {
      it('should have a cancelAndSendActionSalesCreditMemos function', (done) => {
        try {
          assert.equal(true, typeof a.cancelAndSendActionSalesCreditMemos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.cancelAndSendActionSalesCreditMemos(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelAndSendActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.cancelAndSendActionSalesCreditMemos('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelAndSendActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelActionSalesCreditMemos - errors', () => {
      it('should have a cancelActionSalesCreditMemos function', (done) => {
        try {
          assert.equal(true, typeof a.cancelActionSalesCreditMemos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.cancelActionSalesCreditMemos(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.cancelActionSalesCreditMemos('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-cancelActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAndSendActionSalesCreditMemos - errors', () => {
      it('should have a postAndSendActionSalesCreditMemos function', (done) => {
        try {
          assert.equal(true, typeof a.postAndSendActionSalesCreditMemos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postAndSendActionSalesCreditMemos(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAndSendActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.postAndSendActionSalesCreditMemos('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postAndSendActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postActionSalesCreditMemos - errors', () => {
      it('should have a postActionSalesCreditMemos function', (done) => {
        try {
          assert.equal(true, typeof a.postActionSalesCreditMemos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postActionSalesCreditMemos(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.postActionSalesCreditMemos('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendActionSalesCreditMemos - errors', () => {
      it('should have a sendActionSalesCreditMemos function', (done) => {
        try {
          assert.equal(true, typeof a.sendActionSalesCreditMemos === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.sendActionSalesCreditMemos(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-sendActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.sendActionSalesCreditMemos('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-sendActionSalesCreditMemos', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesCreditMemoLinesForSalesCreditMemo - errors', () => {
      it('should have a listSalesCreditMemoLinesForSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesCreditMemoLinesForSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesCreditMemoLinesForSalesCreditMemo(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesCreditMemoLinesForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.listSalesCreditMemoLinesForSalesCreditMemo('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesCreditMemoLinesForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should have a postSalesCreditMemoLineForSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesCreditMemoLineForSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesCreditMemoLineForSalesCreditMemo(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.postSalesCreditMemoLineForSalesCreditMemo('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesCreditMemoLineForSalesCreditMemo('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should have a getSalesCreditMemoLineForSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesCreditMemoLineForSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesCreditMemoLineForSalesCreditMemo(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.getSalesCreditMemoLineForSalesCreditMemo('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoLineId', (done) => {
        try {
          a.getSalesCreditMemoLineForSalesCreditMemo('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should have a deleteSalesCreditMemoLineForSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesCreditMemoLineForSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesCreditMemoLineForSalesCreditMemo(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.deleteSalesCreditMemoLineForSalesCreditMemo('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoLineId', (done) => {
        try {
          a.deleteSalesCreditMemoLineForSalesCreditMemo('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should have a patchSalesCreditMemoLineForSalesCreditMemo function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesCreditMemoLineForSalesCreditMemo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesCreditMemoLineForSalesCreditMemo(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoId', (done) => {
        try {
          a.patchSalesCreditMemoLineForSalesCreditMemo('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoLineId', (done) => {
        try {
          a.patchSalesCreditMemoLineForSalesCreditMemo('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesCreditMemoLineForSalesCreditMemo('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemoLineForSalesCreditMemo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesCreditMemoLines - errors', () => {
      it('should have a listSalesCreditMemoLines function', (done) => {
        try {
          assert.equal(true, typeof a.listSalesCreditMemoLines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listSalesCreditMemoLines(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listSalesCreditMemoLines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSalesCreditMemoLine - errors', () => {
      it('should have a postSalesCreditMemoLine function', (done) => {
        try {
          assert.equal(true, typeof a.postSalesCreditMemoLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postSalesCreditMemoLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSalesCreditMemoLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesCreditMemoLine - errors', () => {
      it('should have a getSalesCreditMemoLine function', (done) => {
        try {
          assert.equal(true, typeof a.getSalesCreditMemoLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getSalesCreditMemoLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoLineId', (done) => {
        try {
          a.getSalesCreditMemoLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesCreditMemoLine - errors', () => {
      it('should have a deleteSalesCreditMemoLine function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSalesCreditMemoLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteSalesCreditMemoLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoLineId', (done) => {
        try {
          a.deleteSalesCreditMemoLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchSalesCreditMemoLine - errors', () => {
      it('should have a patchSalesCreditMemoLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchSalesCreditMemoLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchSalesCreditMemoLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing salesCreditMemoLineId', (done) => {
        try {
          a.patchSalesCreditMemoLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'salesCreditMemoLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchSalesCreditMemoLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchSalesCreditMemoLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGeneralLedgerEntryAttachments - errors', () => {
      it('should have a listGeneralLedgerEntryAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.listGeneralLedgerEntryAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listGeneralLedgerEntryAttachments(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGeneralLedgerEntryAttachments - errors', () => {
      it('should have a postGeneralLedgerEntryAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.postGeneralLedgerEntryAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postGeneralLedgerEntryAttachments(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postGeneralLedgerEntryAttachments('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeneralLedgerEntryAttachments - errors', () => {
      it('should have a getGeneralLedgerEntryAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.getGeneralLedgerEntryAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getGeneralLedgerEntryAttachments(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generalLedgerEntryAttachmentsGeneralLedgerEntryNumber', (done) => {
        try {
          a.getGeneralLedgerEntryAttachments('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'generalLedgerEntryAttachmentsGeneralLedgerEntryNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generalLedgerEntryAttachmentsId', (done) => {
        try {
          a.getGeneralLedgerEntryAttachments('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'generalLedgerEntryAttachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeneralLedgerEntryAttachments - errors', () => {
      it('should have a deleteGeneralLedgerEntryAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGeneralLedgerEntryAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteGeneralLedgerEntryAttachments(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generalLedgerEntryAttachmentsGeneralLedgerEntryNumber', (done) => {
        try {
          a.deleteGeneralLedgerEntryAttachments('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'generalLedgerEntryAttachmentsGeneralLedgerEntryNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generalLedgerEntryAttachmentsId', (done) => {
        try {
          a.deleteGeneralLedgerEntryAttachments('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'generalLedgerEntryAttachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchGeneralLedgerEntryAttachments - errors', () => {
      it('should have a patchGeneralLedgerEntryAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.patchGeneralLedgerEntryAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchGeneralLedgerEntryAttachments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generalLedgerEntryAttachmentsGeneralLedgerEntryNumber', (done) => {
        try {
          a.patchGeneralLedgerEntryAttachments('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'generalLedgerEntryAttachmentsGeneralLedgerEntryNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing generalLedgerEntryAttachmentsId', (done) => {
        try {
          a.patchGeneralLedgerEntryAttachments('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'generalLedgerEntryAttachmentsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchGeneralLedgerEntryAttachments('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchGeneralLedgerEntryAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPurchaseInvoices - errors', () => {
      it('should have a listPurchaseInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.listPurchaseInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPurchaseInvoices(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPurchaseInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPurchaseInvoice - errors', () => {
      it('should have a postPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.postPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postPurchaseInvoice(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postPurchaseInvoice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseInvoice - errors', () => {
      it('should have a getPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.getPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPurchaseInvoice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.getPurchaseInvoice('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseInvoice - errors', () => {
      it('should have a deletePurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.deletePurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePurchaseInvoice(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.deletePurchaseInvoice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPurchaseInvoice - errors', () => {
      it('should have a patchPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.patchPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPurchaseInvoice(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.patchPurchaseInvoice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPurchaseInvoice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postActionPurchaseInvoices - errors', () => {
      it('should have a postActionPurchaseInvoices function', (done) => {
        try {
          assert.equal(true, typeof a.postActionPurchaseInvoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postActionPurchaseInvoices(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionPurchaseInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.postActionPurchaseInvoices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postActionPurchaseInvoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPurchaseInvoiceLinesForPurchaseInvoice - errors', () => {
      it('should have a listPurchaseInvoiceLinesForPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.listPurchaseInvoiceLinesForPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPurchaseInvoiceLinesForPurchaseInvoice(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPurchaseInvoiceLinesForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.listPurchaseInvoiceLinesForPurchaseInvoice('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPurchaseInvoiceLinesForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should have a postPurchaseInvoiceLineForPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.postPurchaseInvoiceLineForPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postPurchaseInvoiceLineForPurchaseInvoice(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.postPurchaseInvoiceLineForPurchaseInvoice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postPurchaseInvoiceLineForPurchaseInvoice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should have a getPurchaseInvoiceLineForPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.getPurchaseInvoiceLineForPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPurchaseInvoiceLineForPurchaseInvoice(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.getPurchaseInvoiceLineForPurchaseInvoice('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceLineId', (done) => {
        try {
          a.getPurchaseInvoiceLineForPurchaseInvoice('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should have a deletePurchaseInvoiceLineForPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.deletePurchaseInvoiceLineForPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePurchaseInvoiceLineForPurchaseInvoice(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.deletePurchaseInvoiceLineForPurchaseInvoice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceLineId', (done) => {
        try {
          a.deletePurchaseInvoiceLineForPurchaseInvoice('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should have a patchPurchaseInvoiceLineForPurchaseInvoice function', (done) => {
        try {
          assert.equal(true, typeof a.patchPurchaseInvoiceLineForPurchaseInvoice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPurchaseInvoiceLineForPurchaseInvoice(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceId', (done) => {
        try {
          a.patchPurchaseInvoiceLineForPurchaseInvoice('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceLineId', (done) => {
        try {
          a.patchPurchaseInvoiceLineForPurchaseInvoice('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPurchaseInvoiceLineForPurchaseInvoice('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoiceLineForPurchaseInvoice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPurchaseInvoiceLines - errors', () => {
      it('should have a listPurchaseInvoiceLines function', (done) => {
        try {
          assert.equal(true, typeof a.listPurchaseInvoiceLines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listPurchaseInvoiceLines(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listPurchaseInvoiceLines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPurchaseInvoiceLine - errors', () => {
      it('should have a postPurchaseInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.postPurchaseInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postPurchaseInvoiceLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postPurchaseInvoiceLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postPurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseInvoiceLine - errors', () => {
      it('should have a getPurchaseInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.getPurchaseInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getPurchaseInvoiceLine(null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceLineId', (done) => {
        try {
          a.getPurchaseInvoiceLine('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getPurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseInvoiceLine - errors', () => {
      it('should have a deletePurchaseInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.deletePurchaseInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deletePurchaseInvoiceLine(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceLineId', (done) => {
        try {
          a.deletePurchaseInvoiceLine('fakeparam', null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deletePurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPurchaseInvoiceLine - errors', () => {
      it('should have a patchPurchaseInvoiceLine function', (done) => {
        try {
          assert.equal(true, typeof a.patchPurchaseInvoiceLine === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchPurchaseInvoiceLine(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseInvoiceLineId', (done) => {
        try {
          a.patchPurchaseInvoiceLine('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'purchaseInvoiceLineId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchPurchaseInvoiceLine('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchPurchaseInvoiceLine', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listProjects - errors', () => {
      it('should have a listProjects function', (done) => {
        try {
          assert.equal(true, typeof a.listProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listProjects(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listProjects', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postProject - errors', () => {
      it('should have a postProject function', (done) => {
        try {
          assert.equal(true, typeof a.postProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postProject(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postProject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProject - errors', () => {
      it('should have a getProject function', (done) => {
        try {
          assert.equal(true, typeof a.getProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getProject(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.getProject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProject - errors', () => {
      it('should have a deleteProject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteProject(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.deleteProject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchProject - errors', () => {
      it('should have a patchProject function', (done) => {
        try {
          assert.equal(true, typeof a.patchProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchProject(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectId', (done) => {
        try {
          a.patchProject('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchProject('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBankAccounts - errors', () => {
      it('should have a listBankAccounts function', (done) => {
        try {
          assert.equal(true, typeof a.listBankAccounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listBankAccounts(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listBankAccounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postBankAccount - errors', () => {
      it('should have a postBankAccount function', (done) => {
        try {
          assert.equal(true, typeof a.postBankAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.postBankAccount(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postBankAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-postBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBankAccount - errors', () => {
      it('should have a getBankAccount function', (done) => {
        try {
          assert.equal(true, typeof a.getBankAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getBankAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bankAccountId', (done) => {
        try {
          a.getBankAccount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bankAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBankAccount - errors', () => {
      it('should have a deleteBankAccount function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBankAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.deleteBankAccount(null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bankAccountId', (done) => {
        try {
          a.deleteBankAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bankAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-deleteBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchBankAccount - errors', () => {
      it('should have a patchBankAccount function', (done) => {
        try {
          assert.equal(true, typeof a.patchBankAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.patchBankAccount(null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bankAccountId', (done) => {
        try {
          a.patchBankAccount('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'bankAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.patchBankAccount('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-patchBankAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerSales - errors', () => {
      it('should have a listCustomerSales function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomerSales === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listCustomerSales(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listCustomerSales', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerSale - errors', () => {
      it('should have a getCustomerSale function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomerSale === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getCustomerSale(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerSale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerSaleCustomerId', (done) => {
        try {
          a.getCustomerSale('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerSaleCustomerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerSale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerSaleCustomerNumber', (done) => {
        try {
          a.getCustomerSale('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'customerSaleCustomerNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerSale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerSaleName', (done) => {
        try {
          a.getCustomerSale('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customerSaleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getCustomerSale', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVendorPurchases - errors', () => {
      it('should have a listVendorPurchases function', (done) => {
        try {
          assert.equal(true, typeof a.listVendorPurchases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.listVendorPurchases(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-listVendorPurchases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendorPurchase - errors', () => {
      it('should have a getVendorPurchase function', (done) => {
        try {
          assert.equal(true, typeof a.getVendorPurchase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing companyId', (done) => {
        try {
          a.getVendorPurchase(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'companyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getVendorPurchase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorPurchaseVendorId', (done) => {
        try {
          a.getVendorPurchase('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'vendorPurchaseVendorId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getVendorPurchase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorPurchaseVendorNumber', (done) => {
        try {
          a.getVendorPurchase('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vendorPurchaseVendorNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getVendorPurchase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vendorPurchaseName', (done) => {
        try {
          a.getVendorPurchase('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vendorPurchaseName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-microsoft_dynamics-adapter-getVendorPurchase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
