/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-microsoft_dynamics',
      type: 'MicrosoftDynamics',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const MicrosoftDynamics = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Microsoft_dynamics Adapter Test', () => {
  describe('MicrosoftDynamics Class Tests', () => {
    const a = new MicrosoftDynamics(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#listCompanies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCompanies(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Company', 'listCompanies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const companyCompanyId = 'fakedata';
    describe('#getCompany - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCompany(companyCompanyId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Company', 'getCompany', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const itemCompanyId = 'fakedata';
    const itemPostItemBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      type: 'string',
      itemCategoryId: 'string',
      itemCategoryCode: 'string',
      blocked: true,
      baseUnitOfMeasureId: 'string',
      baseUnitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 7,
          picture: [
            {
              id: 'string',
              width: 6,
              height: 2,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 8,
            height: 6,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      gtin: 'string',
      inventory: 9,
      unitPrice: 1,
      priceIncludesTax: false,
      unitCost: 8,
      taxGroupId: 'string',
      taxGroupCode: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postItem(itemCompanyId, itemPostItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Item', 'postItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listItems - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listItems(itemCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Item', 'listItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const itemItemId = 'fakedata';
    const itemPatchItemBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      type: 'string',
      itemCategoryId: 'string',
      itemCategoryCode: 'string',
      blocked: true,
      baseUnitOfMeasureId: 'string',
      baseUnitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 9,
          picture: [
            {
              id: 'string',
              width: 5,
              height: 7,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 6,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      gtin: 'string',
      inventory: 8,
      unitPrice: 4,
      priceIncludesTax: false,
      unitCost: 5,
      taxGroupId: 'string',
      taxGroupCode: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchItem(itemCompanyId, itemItemId, itemPatchItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Item', 'patchItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getItem(itemCompanyId, itemItemId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Item', 'getItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pictureCompanyId = 'fakedata';
    const pictureCustomerId = 'fakedata';
    describe('#listPictureForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPictureForCustomer(pictureCompanyId, pictureCustomerId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'listPictureForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const picturePictureId = 'fakedata';
    const picturePatchPictureForCustomerBodyParam = {
      id: 'string',
      width: 1,
      height: 6,
      contentType: 'string',
      content: 'string'
    };
    describe('#patchPictureForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPictureForCustomer(pictureCompanyId, pictureCustomerId, picturePictureId, picturePatchPictureForCustomerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'patchPictureForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPictureForCustomer(pictureCompanyId, pictureCustomerId, picturePictureId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'getPictureForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pictureEmployeeId = 'fakedata';
    describe('#listPictureForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPictureForEmployee(pictureCompanyId, pictureEmployeeId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'listPictureForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const picturePatchPictureForEmployeeBodyParam = {
      id: 'string',
      width: 8,
      height: 1,
      contentType: 'string',
      content: 'string'
    };
    describe('#patchPictureForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPictureForEmployee(pictureCompanyId, pictureEmployeeId, picturePictureId, picturePatchPictureForEmployeeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'patchPictureForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPictureForEmployee(pictureCompanyId, pictureEmployeeId, picturePictureId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'getPictureForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pictureItemId = 'fakedata';
    describe('#listPictureForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPictureForItem(pictureCompanyId, pictureItemId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'listPictureForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const picturePatchPictureForItemBodyParam = {
      id: 'string',
      width: 4,
      height: 2,
      contentType: 'string',
      content: 'string'
    };
    describe('#patchPictureForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPictureForItem(pictureCompanyId, pictureItemId, picturePictureId, picturePatchPictureForItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'patchPictureForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPictureForItem(pictureCompanyId, pictureItemId, picturePictureId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'getPictureForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPicture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPicture(pictureCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'listPicture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const picturePatchPictureBodyParam = {
      id: 'string',
      width: 2,
      height: 4,
      contentType: 'string',
      content: 'string'
    };
    describe('#patchPicture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPicture(pictureCompanyId, picturePictureId, picturePatchPictureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'patchPicture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPicture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPicture(pictureCompanyId, picturePictureId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'getPicture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pictureVendorId = 'fakedata';
    describe('#listPictureForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPictureForVendor(pictureCompanyId, pictureVendorId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'listPictureForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const picturePatchPictureForVendorBodyParam = {
      id: 'string',
      width: 6,
      height: 1,
      contentType: 'string',
      content: 'string'
    };
    describe('#patchPictureForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPictureForVendor(pictureCompanyId, pictureVendorId, picturePictureId, picturePatchPictureForVendorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'patchPictureForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPictureForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPictureForVendor(pictureCompanyId, pictureVendorId, picturePictureId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'getPictureForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsCompanyId = 'fakedata';
    const defaultDimensionsCustomerId = 'fakedata';
    const defaultDimensionsPostDefaultDimensionsForCustomerBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#postDefaultDimensionsForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDefaultDimensionsForCustomer(defaultDimensionsCompanyId, defaultDimensionsCustomerId, defaultDimensionsPostDefaultDimensionsForCustomerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'postDefaultDimensionsForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsPostDefaultDimensionsBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#postDefaultDimensions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDefaultDimensions(defaultDimensionsCompanyId, defaultDimensionsPostDefaultDimensionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'postDefaultDimensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsEmployeeId = 'fakedata';
    const defaultDimensionsPostDefaultDimensionsForEmployeeBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#postDefaultDimensionsForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDefaultDimensionsForEmployee(defaultDimensionsCompanyId, defaultDimensionsEmployeeId, defaultDimensionsPostDefaultDimensionsForEmployeeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'postDefaultDimensionsForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsItemId = 'fakedata';
    const defaultDimensionsPostDefaultDimensionsForItemBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#postDefaultDimensionsForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDefaultDimensionsForItem(defaultDimensionsCompanyId, defaultDimensionsItemId, defaultDimensionsPostDefaultDimensionsForItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'postDefaultDimensionsForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsVendorId = 'fakedata';
    const defaultDimensionsPostDefaultDimensionsForVendorBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#postDefaultDimensionsForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDefaultDimensionsForVendor(defaultDimensionsCompanyId, defaultDimensionsVendorId, defaultDimensionsPostDefaultDimensionsForVendorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'postDefaultDimensionsForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDefaultDimensionsForCustomer(defaultDimensionsCompanyId, defaultDimensionsCustomerId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'listDefaultDimensionsForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsDefaultDimensionsParentId = 'fakedata';
    const defaultDimensionsDefaultDimensionsDimensionId = 'fakedata';
    const defaultDimensionsPatchDefaultDimensionsForCustomerBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#patchDefaultDimensionsForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDefaultDimensionsForCustomer(defaultDimensionsCompanyId, defaultDimensionsCustomerId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, defaultDimensionsPatchDefaultDimensionsForCustomerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'patchDefaultDimensionsForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDefaultDimensionsForCustomer(defaultDimensionsCompanyId, defaultDimensionsCustomerId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'getDefaultDimensionsForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDefaultDimensions(defaultDimensionsCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'listDefaultDimensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsPatchDefaultDimensionsBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#patchDefaultDimensions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDefaultDimensions(defaultDimensionsCompanyId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, defaultDimensionsPatchDefaultDimensionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'patchDefaultDimensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDefaultDimensions(defaultDimensionsCompanyId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'getDefaultDimensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDefaultDimensionsForEmployee(defaultDimensionsCompanyId, defaultDimensionsEmployeeId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'listDefaultDimensionsForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsPatchDefaultDimensionsForEmployeeBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#patchDefaultDimensionsForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDefaultDimensionsForEmployee(defaultDimensionsCompanyId, defaultDimensionsEmployeeId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, defaultDimensionsPatchDefaultDimensionsForEmployeeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'patchDefaultDimensionsForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDefaultDimensionsForEmployee(defaultDimensionsCompanyId, defaultDimensionsEmployeeId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'getDefaultDimensionsForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDefaultDimensionsForItem(defaultDimensionsCompanyId, defaultDimensionsItemId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'listDefaultDimensionsForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsPatchDefaultDimensionsForItemBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#patchDefaultDimensionsForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDefaultDimensionsForItem(defaultDimensionsCompanyId, defaultDimensionsItemId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, defaultDimensionsPatchDefaultDimensionsForItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'patchDefaultDimensionsForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDefaultDimensionsForItem(defaultDimensionsCompanyId, defaultDimensionsItemId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'getDefaultDimensionsForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDefaultDimensionsForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDefaultDimensionsForVendor(defaultDimensionsCompanyId, defaultDimensionsVendorId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'listDefaultDimensionsForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const defaultDimensionsPatchDefaultDimensionsForVendorBodyParam = {
      parentId: 'string',
      dimensionId: 'string',
      dimensionCode: 'string',
      dimensionValueId: 'string',
      dimensionValueCode: 'string',
      postingValidation: 'string'
    };
    describe('#patchDefaultDimensionsForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDefaultDimensionsForVendor(defaultDimensionsCompanyId, defaultDimensionsVendorId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, defaultDimensionsPatchDefaultDimensionsForVendorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'patchDefaultDimensionsForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultDimensionsForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDefaultDimensionsForVendor(defaultDimensionsCompanyId, defaultDimensionsVendorId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'getDefaultDimensionsForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerCompanyId = 'fakedata';
    const customerPostCustomerBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      type: 'string',
      address: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 6,
            totalSalesExcludingTax: 4,
            overdueAmount: 10
          }
        ],
        picture: [
          {
            id: 'string',
            width: 9,
            height: 7,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 9,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 5,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      phoneNumber: 'string',
      email: 'string',
      website: 'string',
      taxLiable: false,
      taxAreaId: 'string',
      taxAreaDisplayName: 'string',
      taxRegistrationNumber: 'string',
      currencyId: 'string',
      currencyCode: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      paymentMethodId: 'string',
      blocked: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postCustomer(customerCompanyId, customerPostCustomerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'postCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCustomers(customerCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'listCustomers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerCustomerId = 'fakedata';
    const customerPatchCustomerBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      type: 'string',
      address: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 5,
            totalSalesExcludingTax: 7,
            overdueAmount: 1
          }
        ],
        picture: [
          {
            id: 'string',
            width: 5,
            height: 8,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 6,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 9,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      phoneNumber: 'string',
      email: 'string',
      website: 'string',
      taxLiable: true,
      taxAreaId: 'string',
      taxAreaDisplayName: 'string',
      taxRegistrationNumber: 'string',
      currencyId: 'string',
      currencyCode: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      paymentMethodId: 'string',
      blocked: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCustomer(customerCompanyId, customerCustomerId, customerPatchCustomerBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'patchCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomer(customerCompanyId, customerCustomerId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'getCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerFinancialDetailCompanyId = 'fakedata';
    describe('#listCustomerFinancialDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCustomerFinancialDetails(customerFinancialDetailCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerFinancialDetail', 'listCustomerFinancialDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerFinancialDetailCustomerFinancialDetailId = 'fakedata';
    describe('#getCustomerFinancialDetail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomerFinancialDetail(customerFinancialDetailCompanyId, customerFinancialDetailCustomerFinancialDetailId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerFinancialDetail', 'getCustomerFinancialDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerFinancialDetailCustomerId = 'fakedata';
    describe('#listCustomerFinancialDetailsForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCustomerFinancialDetailsForCustomer(customerFinancialDetailCompanyId, customerFinancialDetailCustomerId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerFinancialDetail', 'listCustomerFinancialDetailsForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerFinancialDetailForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomerFinancialDetailForCustomer(customerFinancialDetailCompanyId, customerFinancialDetailCustomerId, customerFinancialDetailCustomerFinancialDetailId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerFinancialDetail', 'getCustomerFinancialDetailForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorCompanyId = 'fakedata';
    const vendorPostVendorBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      address: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 5,
            totalSalesExcludingTax: 5,
            overdueAmount: 10
          }
        ],
        picture: [
          {
            id: 'string',
            width: 9,
            height: 5,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 8,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 10,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      phoneNumber: 'string',
      email: 'string',
      website: 'string',
      taxRegistrationNumber: 'string',
      currencyId: 'string',
      currencyCode: 'string',
      irs1099Code: 'string',
      paymentTermsId: 'string',
      paymentMethodId: 'string',
      taxLiable: false,
      blocked: 'string',
      balance: 6,
      lastModifiedDateTime: 'string'
    };
    describe('#postVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postVendor(vendorCompanyId, vendorPostVendorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'postVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listVendors - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listVendors(vendorCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'listVendors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorVendorId = 'fakedata';
    const vendorPatchVendorBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      address: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 10,
            totalSalesExcludingTax: 1,
            overdueAmount: 5
          }
        ],
        picture: [
          {
            id: 'string',
            width: 2,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 8,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 9,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      phoneNumber: 'string',
      email: 'string',
      website: 'string',
      taxRegistrationNumber: 'string',
      currencyId: 'string',
      currencyCode: 'string',
      irs1099Code: 'string',
      paymentTermsId: 'string',
      paymentMethodId: 'string',
      taxLiable: false,
      blocked: 'string',
      balance: 1,
      lastModifiedDateTime: 'string'
    };
    describe('#patchVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchVendor(vendorCompanyId, vendorVendorId, vendorPatchVendorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'patchVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVendor(vendorCompanyId, vendorVendorId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'getVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const companyInformationCompanyId = 'fakedata';
    describe('#listCompanyInformation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCompanyInformation(companyInformationCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CompanyInformation', 'listCompanyInformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const companyInformationCompanyInformationId = 'fakedata';
    const companyInformationPatchCompanyInformationBodyParam = {
      id: 'string',
      displayName: 'string',
      address: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 1,
            overdueAmount: 9
          }
        ],
        picture: [
          {
            id: 'string',
            width: 7,
            height: 9,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 9,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 9,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      phoneNumber: 'string',
      faxNumber: 'string',
      email: 'string',
      website: 'string',
      taxRegistrationNumber: 'string',
      currencyCode: 'string',
      currentFiscalYearStartDate: 'string',
      industry: 'string',
      picture: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchCompanyInformation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCompanyInformation(companyInformationCompanyId, companyInformationCompanyInformationId, companyInformationPatchCompanyInformationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CompanyInformation', 'patchCompanyInformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCompanyInformation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCompanyInformation(companyInformationCompanyId, companyInformationCompanyInformationId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CompanyInformation', 'getCompanyInformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesInvoiceCompanyId = 'fakedata';
    const salesInvoicePostSalesInvoiceBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      invoiceDate: 'string',
      dueDate: 'string',
      customerPurchaseOrderReference: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 5,
            totalSalesExcludingTax: 1,
            overdueAmount: 9
          }
        ],
        picture: [
          {
            id: 'string',
            width: 3,
            height: 9,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 6,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 2,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 10,
            totalSalesExcludingTax: 8,
            overdueAmount: 9
          }
        ],
        picture: [
          {
            id: 'string',
            width: 4,
            height: 4,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 9,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 5,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shippingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 8,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 3,
            height: 7,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 2,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 10,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      orderId: 'string',
      orderNumber: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      pricesIncludeTax: false,
      remainingAmount: 1,
      discountAmount: 3,
      discountAppliedBeforeTax: false,
      totalAmountExcludingTax: 9,
      totalTaxAmount: 6,
      totalAmountIncludingTax: 6,
      status: 'string',
      lastModifiedDateTime: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#postSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesInvoice(salesInvoiceCompanyId, salesInvoicePostSalesInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'postSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesInvoiceSalesInvoiceId = 'fakedata';
    describe('#cancelActionSalesInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelActionSalesInvoices(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'cancelActionSalesInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelAndSendActionSalesInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelAndSendActionSalesInvoices(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'cancelAndSendActionSalesInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#makeCorrectiveCreditMemoActionSalesInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.makeCorrectiveCreditMemoActionSalesInvoices(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'makeCorrectiveCreditMemoActionSalesInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postActionSalesInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postActionSalesInvoices(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'postActionSalesInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAndSendActionSalesInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAndSendActionSalesInvoices(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'postAndSendActionSalesInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendActionSalesInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sendActionSalesInvoices(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'sendActionSalesInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesInvoices(salesInvoiceCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'listSalesInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesInvoicePatchSalesInvoiceBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      invoiceDate: 'string',
      dueDate: 'string',
      customerPurchaseOrderReference: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 2,
            overdueAmount: 3
          }
        ],
        picture: [
          {
            id: 'string',
            width: 7,
            height: 5,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 4,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 3,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 5,
            overdueAmount: 1
          }
        ],
        picture: [
          {
            id: 'string',
            width: 8,
            height: 7,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 1,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 6,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shippingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 10,
            totalSalesExcludingTax: 6,
            overdueAmount: 5
          }
        ],
        picture: [
          {
            id: 'string',
            width: 3,
            height: 4,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 5,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 6,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      orderId: 'string',
      orderNumber: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      pricesIncludeTax: false,
      remainingAmount: 9,
      discountAmount: 4,
      discountAppliedBeforeTax: false,
      totalAmountExcludingTax: 9,
      totalTaxAmount: 4,
      totalAmountIncludingTax: 9,
      status: 'string',
      lastModifiedDateTime: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#patchSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesInvoice(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, salesInvoicePatchSalesInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'patchSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesInvoice(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'getSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesInvoiceLineCompanyId = 'fakedata';
    const salesInvoiceLinePostSalesInvoiceLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 6,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 7,
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 6,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 6,
                height: 2,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 3,
          unitPrice: 8,
          priceIncludesTax: false,
          unitCost: 4,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 6,
              height: 1,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 6,
          picture: [
            {
              id: 'string',
              width: 8,
              height: 2,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 2,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 8,
      quantity: 4,
      discountAmount: 6,
      discountPercent: 9,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 8,
      taxCode: 'string',
      taxPercent: 9,
      totalTaxAmount: 2,
      amountIncludingTax: 9,
      invoiceDiscountAllocation: 3,
      netAmount: 5,
      netTaxAmount: 10,
      netAmountIncludingTax: 1,
      shipmentDate: 'string'
    };
    describe('#postSalesInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesInvoiceLine(salesInvoiceLineCompanyId, salesInvoiceLinePostSalesInvoiceLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'postSalesInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesInvoiceLineSalesInvoiceId = 'fakedata';
    const salesInvoiceLinePostSalesInvoiceLineForSalesInvoiceBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 2,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: false,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 7,
              picture: [
                {
                  id: 'string',
                  width: 3,
                  height: 7,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 7,
                height: 8,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 7,
          unitPrice: 5,
          priceIncludesTax: true,
          unitCost: 2,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 3,
              height: 1,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 6,
          picture: [
            {
              id: 'string',
              width: 7,
              height: 10,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 10,
            height: 8,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 10,
      quantity: 3,
      discountAmount: 6,
      discountPercent: 6,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 6,
      taxCode: 'string',
      taxPercent: 8,
      totalTaxAmount: 3,
      amountIncludingTax: 6,
      invoiceDiscountAllocation: 10,
      netAmount: 4,
      netTaxAmount: 10,
      netAmountIncludingTax: 4,
      shipmentDate: 'string'
    };
    describe('#postSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesInvoiceLineForSalesInvoice(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceId, salesInvoiceLinePostSalesInvoiceLineForSalesInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'postSalesInvoiceLineForSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesInvoiceLines - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesInvoiceLines(salesInvoiceLineCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'listSalesInvoiceLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesInvoiceLineSalesInvoiceLineId = 'fakedata';
    const salesInvoiceLinePatchSalesInvoiceLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 3,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: false,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 10,
              picture: [
                {
                  id: 'string',
                  width: 10,
                  height: 7,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 9,
                height: 8,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 1,
          unitPrice: 6,
          priceIncludesTax: false,
          unitCost: 1,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 6,
              height: 7,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 5,
          picture: [
            {
              id: 'string',
              width: 8,
              height: 8,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 8,
            height: 5,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 3,
      quantity: 4,
      discountAmount: 3,
      discountPercent: 5,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 9,
      taxCode: 'string',
      taxPercent: 4,
      totalTaxAmount: 5,
      amountIncludingTax: 10,
      invoiceDiscountAllocation: 3,
      netAmount: 9,
      netTaxAmount: 1,
      netAmountIncludingTax: 9,
      shipmentDate: 'string'
    };
    describe('#patchSalesInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesInvoiceLine(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceLineId, salesInvoiceLinePatchSalesInvoiceLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'patchSalesInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesInvoiceLine(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'getSalesInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesInvoiceLinesForSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesInvoiceLinesForSalesInvoice(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'listSalesInvoiceLinesForSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesInvoiceLinePatchSalesInvoiceLineForSalesInvoiceBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 5,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 6,
              picture: [
                {
                  id: 'string',
                  width: 7,
                  height: 8,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 5,
                height: 7,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 4,
          unitPrice: 7,
          priceIncludesTax: false,
          unitCost: 7,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 7,
              height: 9,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 1,
          picture: [
            {
              id: 'string',
              width: 3,
              height: 10,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 4,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 7,
      quantity: 6,
      discountAmount: 9,
      discountPercent: 6,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 2,
      taxCode: 'string',
      taxPercent: 6,
      totalTaxAmount: 5,
      amountIncludingTax: 1,
      invoiceDiscountAllocation: 6,
      netAmount: 9,
      netTaxAmount: 5,
      netAmountIncludingTax: 7,
      shipmentDate: 'string'
    };
    describe('#patchSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesInvoiceLineForSalesInvoice(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceId, salesInvoiceLineSalesInvoiceLineId, salesInvoiceLinePatchSalesInvoiceLineForSalesInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'patchSalesInvoiceLineForSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesInvoiceLineForSalesInvoice(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceId, salesInvoiceLineSalesInvoiceLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'getSalesInvoiceLineForSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pdfDocumentCompanyId = 'fakedata';
    describe('#listPdfDocument - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPdfDocument(pdfDocumentCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'listPdfDocument', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pdfDocumentPdfDocumentId = 'fakedata';
    describe('#getPdfDocument - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPdfDocument(pdfDocumentCompanyId, pdfDocumentPdfDocumentId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'getPdfDocument', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pdfDocumentPurchaseInvoiceId = 'fakedata';
    describe('#listPdfDocumentForPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPdfDocumentForPurchaseInvoice(pdfDocumentCompanyId, pdfDocumentPurchaseInvoiceId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'listPdfDocumentForPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPdfDocumentForPurchaseInvoice(pdfDocumentCompanyId, pdfDocumentPurchaseInvoiceId, pdfDocumentPdfDocumentId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'getPdfDocumentForPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pdfDocumentSalesCreditMemoId = 'fakedata';
    describe('#listPdfDocumentForSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPdfDocumentForSalesCreditMemo(pdfDocumentCompanyId, pdfDocumentSalesCreditMemoId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'listPdfDocumentForSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPdfDocumentForSalesCreditMemo(pdfDocumentCompanyId, pdfDocumentSalesCreditMemoId, pdfDocumentPdfDocumentId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'getPdfDocumentForSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pdfDocumentSalesInvoiceId = 'fakedata';
    describe('#listPdfDocumentForSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPdfDocumentForSalesInvoice(pdfDocumentCompanyId, pdfDocumentSalesInvoiceId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'listPdfDocumentForSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPdfDocumentForSalesInvoice(pdfDocumentCompanyId, pdfDocumentSalesInvoiceId, pdfDocumentPdfDocumentId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'getPdfDocumentForSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pdfDocumentSalesQuoteId = 'fakedata';
    describe('#listPdfDocumentForSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPdfDocumentForSalesQuote(pdfDocumentCompanyId, pdfDocumentSalesQuoteId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'listPdfDocumentForSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPdfDocumentForSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPdfDocumentForSalesQuote(pdfDocumentCompanyId, pdfDocumentSalesQuoteId, pdfDocumentPdfDocumentId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PdfDocument', 'getPdfDocumentForSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerPaymentJournalCompanyId = 'fakedata';
    const customerPaymentJournalPostCustomerPaymentJournalBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string',
      balancingAccountId: 'string',
      balancingAccountNumber: 'string'
    };
    describe('#postCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postCustomerPaymentJournal(customerPaymentJournalCompanyId, customerPaymentJournalPostCustomerPaymentJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPaymentJournal', 'postCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerPaymentJournals - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCustomerPaymentJournals(customerPaymentJournalCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPaymentJournal', 'listCustomerPaymentJournals', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerPaymentJournalCustomerPaymentJournalId = 'fakedata';
    const customerPaymentJournalPatchCustomerPaymentJournalBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string',
      balancingAccountId: 'string',
      balancingAccountNumber: 'string'
    };
    describe('#patchCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCustomerPaymentJournal(customerPaymentJournalCompanyId, customerPaymentJournalCustomerPaymentJournalId, customerPaymentJournalPatchCustomerPaymentJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPaymentJournal', 'patchCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomerPaymentJournal(customerPaymentJournalCompanyId, customerPaymentJournalCustomerPaymentJournalId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPaymentJournal', 'getCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerPaymentCompanyId = 'fakedata';
    const customerPaymentCustomerPaymentJournalId = 'fakedata';
    const customerPaymentPostCustomerPaymentForCustomerPaymentJournalBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 2,
      customerId: 'string',
      customerNumber: 'string',
      contactId: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 7,
      appliesToInvoiceId: 'string',
      appliesToInvoiceNumber: 'string',
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 5,
                  totalSalesExcludingTax: 5,
                  overdueAmount: 6
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 5,
                  height: 1,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 8,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 6,
                calculateDiscountOnCreditMemos: false,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: false,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 1,
                totalSalesExcludingTax: 10,
                overdueAmount: 9
              }
            ],
            picture: [
              {
                id: 'string',
                width: 2,
                height: 7,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 3,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 7,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#postCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postCustomerPaymentForCustomerPaymentJournal(customerPaymentCompanyId, customerPaymentCustomerPaymentJournalId, customerPaymentPostCustomerPaymentForCustomerPaymentJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'postCustomerPaymentForCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerPaymentPostCustomerPaymentBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 1,
      customerId: 'string',
      customerNumber: 'string',
      contactId: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 8,
      appliesToInvoiceId: 'string',
      appliesToInvoiceNumber: 'string',
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 1,
                  totalSalesExcludingTax: 5,
                  overdueAmount: 4
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 2,
                  height: 6,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 9,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 8,
                calculateDiscountOnCreditMemos: false,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: false,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 8,
                totalSalesExcludingTax: 4,
                overdueAmount: 1
              }
            ],
            picture: [
              {
                id: 'string',
                width: 1,
                height: 8,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 1,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 5,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#postCustomerPayment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postCustomerPayment(customerPaymentCompanyId, customerPaymentPostCustomerPaymentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'postCustomerPayment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerPaymentsForCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCustomerPaymentsForCustomerPaymentJournal(customerPaymentCompanyId, customerPaymentCustomerPaymentJournalId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'listCustomerPaymentsForCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerPaymentCustomerPaymentId = 'fakedata';
    const customerPaymentPatchCustomerPaymentForCustomerPaymentJournalBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 10,
      customerId: 'string',
      customerNumber: 'string',
      contactId: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 5,
      appliesToInvoiceId: 'string',
      appliesToInvoiceNumber: 'string',
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 5,
                  totalSalesExcludingTax: 4,
                  overdueAmount: 10
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 7,
                  height: 3,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 2,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 8,
                calculateDiscountOnCreditMemos: false,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: false,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 3,
                totalSalesExcludingTax: 10,
                overdueAmount: 7
              }
            ],
            picture: [
              {
                id: 'string',
                width: 2,
                height: 10,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 5,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 9,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#patchCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCustomerPaymentForCustomerPaymentJournal(customerPaymentCompanyId, customerPaymentCustomerPaymentJournalId, customerPaymentCustomerPaymentId, customerPaymentPatchCustomerPaymentForCustomerPaymentJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'patchCustomerPaymentForCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomerPaymentForCustomerPaymentJournal(customerPaymentCompanyId, customerPaymentCustomerPaymentJournalId, customerPaymentCustomerPaymentId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'getCustomerPaymentForCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomerPayments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCustomerPayments(customerPaymentCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'listCustomerPayments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerPaymentPatchCustomerPaymentBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 1,
      customerId: 'string',
      customerNumber: 'string',
      contactId: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 2,
      appliesToInvoiceId: 'string',
      appliesToInvoiceNumber: 'string',
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 7,
                  totalSalesExcludingTax: 5,
                  overdueAmount: 4
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 3,
                  height: 2,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 5,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 8,
                calculateDiscountOnCreditMemos: false,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: true,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 2,
                totalSalesExcludingTax: 9,
                overdueAmount: 1
              }
            ],
            picture: [
              {
                id: 'string',
                width: 8,
                height: 8,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 4,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 4,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#patchCustomerPayment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCustomerPayment(customerPaymentCompanyId, customerPaymentCustomerPaymentId, customerPaymentPatchCustomerPaymentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'patchCustomerPayment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomerPayment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomerPayment(customerPaymentCompanyId, customerPaymentCustomerPaymentId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'getCustomerPayment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountCompanyId = 'fakedata';
    describe('#listAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAccounts(accountCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'listAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountAccountId = 'fakedata';
    describe('#getAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAccount(accountCompanyId, accountAccountId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'getAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taxGroupCompanyId = 'fakedata';
    const taxGroupPostTaxGroupBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      taxType: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postTaxGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTaxGroup(taxGroupCompanyId, taxGroupPostTaxGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxGroup', 'postTaxGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTaxGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listTaxGroups(taxGroupCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxGroup', 'listTaxGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taxGroupTaxGroupId = 'fakedata';
    const taxGroupPatchTaxGroupBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      taxType: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchTaxGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTaxGroup(taxGroupCompanyId, taxGroupTaxGroupId, taxGroupPatchTaxGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxGroup', 'patchTaxGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaxGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTaxGroup(taxGroupCompanyId, taxGroupTaxGroupId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxGroup', 'getTaxGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const journalCompanyId = 'fakedata';
    const journalPostJournalBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string',
      balancingAccountId: 'string',
      balancingAccountNumber: 'string'
    };
    describe('#postJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postJournal(journalCompanyId, journalPostJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Journal', 'postJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const journalJournalId = 'fakedata';
    describe('#postActionJournals - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postActionJournals(journalCompanyId, journalJournalId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Journal', 'postActionJournals', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listJournals - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listJournals(journalCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Journal', 'listJournals', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const journalPatchJournalBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string',
      balancingAccountId: 'string',
      balancingAccountNumber: 'string'
    };
    describe('#patchJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchJournal(journalCompanyId, journalJournalId, journalPatchJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Journal', 'patchJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJournal(journalCompanyId, journalJournalId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Journal', 'getJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const journalLineCompanyId = 'fakedata';
    const journalLinePostJournalLineBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 4,
      accountType: 'string',
      accountId: 'string',
      accountNumber: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 7,
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 2,
                  totalSalesExcludingTax: 4,
                  overdueAmount: 6
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 4,
                  height: 1,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 2,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 9,
                calculateDiscountOnCreditMemos: false,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: true,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 5,
                totalSalesExcludingTax: 6,
                overdueAmount: 9
              }
            ],
            picture: [
              {
                id: 'string',
                width: 3,
                height: 5,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 8,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 1,
              calculateDiscountOnCreditMemos: true,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#postJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postJournalLine(journalLineCompanyId, journalLinePostJournalLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'postJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const journalLineJournalId = 'fakedata';
    const journalLinePostJournalLineForJournalBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 5,
      accountType: 'string',
      accountId: 'string',
      accountNumber: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 7,
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 7,
                  totalSalesExcludingTax: 6,
                  overdueAmount: 2
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 8,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 6,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 9,
                calculateDiscountOnCreditMemos: false,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: false,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 2,
                totalSalesExcludingTax: 4,
                overdueAmount: 3
              }
            ],
            picture: [
              {
                id: 'string',
                width: 2,
                height: 3,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 6,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 1,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#postJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postJournalLineForJournal(journalLineCompanyId, journalLineJournalId, journalLinePostJournalLineForJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'postJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listJournalLines - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listJournalLines(journalLineCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'listJournalLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const journalLineJournalLineId = 'fakedata';
    const journalLinePatchJournalLineBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 6,
      accountType: 'string',
      accountId: 'string',
      accountNumber: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 7,
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 9,
                  totalSalesExcludingTax: 6,
                  overdueAmount: 1
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 7,
                  height: 10,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 2,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 7,
                calculateDiscountOnCreditMemos: true,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: false,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 5,
                totalSalesExcludingTax: 9,
                overdueAmount: 2
              }
            ],
            picture: [
              {
                id: 'string',
                width: 4,
                height: 5,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 1,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 10,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#patchJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchJournalLine(journalLineCompanyId, journalLineJournalLineId, journalLinePatchJournalLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'patchJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJournalLine(journalLineCompanyId, journalLineJournalLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'getJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listJournalLinesForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listJournalLinesForJournal(journalLineCompanyId, journalLineJournalId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'listJournalLinesForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const journalLinePatchJournalLineForJournalBodyParam = {
      id: 'string',
      journalDisplayName: 'string',
      lineNumber: 7,
      accountType: 'string',
      accountId: 'string',
      accountNumber: 'string',
      postingDate: 'string',
      documentNumber: 'string',
      externalDocumentNumber: 'string',
      amount: 2,
      description: 'string',
      comment: 'string',
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 5,
                  totalSalesExcludingTax: 1,
                  overdueAmount: 5
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 4,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 6,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 8,
                calculateDiscountOnCreditMemos: true,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: false,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 4,
                totalSalesExcludingTax: 3,
                overdueAmount: 6
              }
            ],
            picture: [
              {
                id: 'string',
                width: 6,
                height: 3,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 10,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 3,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModifiedDateTime: 'string'
    };
    describe('#patchJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchJournalLineForJournal(journalLineCompanyId, journalLineJournalId, journalLineJournalLineId, journalLinePatchJournalLineForJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'patchJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJournalLineForJournal(journalLineCompanyId, journalLineJournalId, journalLineJournalLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'getJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const attachmentsCompanyId = 'fakedata';
    const attachmentsPostAttachmentsBodyParam = {
      parentId: 'string',
      id: 'string',
      fileName: 'string',
      byteSize: 2,
      content: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAttachments(attachmentsCompanyId, attachmentsPostAttachmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'postAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const attachmentsJournalLineId = 'fakedata';
    const attachmentsPostAttachmentsForJournalLineBodyParam = {
      parentId: 'string',
      id: 'string',
      fileName: 'string',
      byteSize: 1,
      content: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postAttachmentsForJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAttachmentsForJournalLine(attachmentsCompanyId, attachmentsJournalLineId, attachmentsPostAttachmentsForJournalLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'postAttachmentsForJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const attachmentsJournalId = 'fakedata';
    const attachmentsPostAttachmentsForJournalLineForJournalBodyParam = {
      parentId: 'string',
      id: 'string',
      fileName: 'string',
      byteSize: 6,
      content: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postAttachmentsForJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAttachmentsForJournalLineForJournal(attachmentsCompanyId, attachmentsJournalId, attachmentsJournalLineId, attachmentsPostAttachmentsForJournalLineForJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'postAttachmentsForJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAttachments(attachmentsCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'listAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const attachmentsAttachmentsParentId = 'fakedata';
    const attachmentsAttachmentsId = 'fakedata';
    const attachmentsPatchAttachmentsBodyParam = {
      parentId: 'string',
      id: 'string',
      fileName: 'string',
      byteSize: 4,
      content: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchAttachments(attachmentsCompanyId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, attachmentsPatchAttachmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'patchAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAttachments(attachmentsCompanyId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'getAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachmentsForJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAttachmentsForJournalLine(attachmentsCompanyId, attachmentsJournalLineId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'listAttachmentsForJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const attachmentsPatchAttachmentsForJournalLineBodyParam = {
      parentId: 'string',
      id: 'string',
      fileName: 'string',
      byteSize: 8,
      content: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchAttachmentsForJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchAttachmentsForJournalLine(attachmentsCompanyId, attachmentsJournalLineId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, attachmentsPatchAttachmentsForJournalLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'patchAttachmentsForJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentsForJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAttachmentsForJournalLine(attachmentsCompanyId, attachmentsJournalLineId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'getAttachmentsForJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttachmentsForJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAttachmentsForJournalLineForJournal(attachmentsCompanyId, attachmentsJournalId, attachmentsJournalLineId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'listAttachmentsForJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const attachmentsPatchAttachmentsForJournalLineForJournalBodyParam = {
      parentId: 'string',
      id: 'string',
      fileName: 'string',
      byteSize: 1,
      content: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchAttachmentsForJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchAttachmentsForJournalLineForJournal(attachmentsCompanyId, attachmentsJournalId, attachmentsJournalLineId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, attachmentsPatchAttachmentsForJournalLineForJournalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'patchAttachmentsForJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentsForJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAttachmentsForJournalLineForJournal(attachmentsCompanyId, attachmentsJournalId, attachmentsJournalLineId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'getAttachmentsForJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const employeeCompanyId = 'fakedata';
    const employeePostEmployeeBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      givenName: 'string',
      middleName: 'string',
      surname: 'string',
      jobTitle: 'string',
      address: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 9,
            overdueAmount: 3
          }
        ],
        picture: [
          {
            id: 'string',
            width: 9,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 8,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 10,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      phoneNumber: 'string',
      mobilePhone: 'string',
      email: 'string',
      personalEmail: 'string',
      employmentDate: 'string',
      terminationDate: 'string',
      status: 'string',
      birthDate: 'string',
      statisticsGroupCode: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postEmployee(employeeCompanyId, employeePostEmployeeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Employee', 'postEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listEmployees - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listEmployees(employeeCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Employee', 'listEmployees', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const employeeEmployeeId = 'fakedata';
    const employeePatchEmployeeBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string',
      givenName: 'string',
      middleName: 'string',
      surname: 'string',
      jobTitle: 'string',
      address: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 3,
            totalSalesExcludingTax: 4,
            overdueAmount: 5
          }
        ],
        picture: [
          {
            id: 'string',
            width: 3,
            height: 10,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 10,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 1,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      phoneNumber: 'string',
      mobilePhone: 'string',
      email: 'string',
      personalEmail: 'string',
      employmentDate: 'string',
      terminationDate: 'string',
      status: 'string',
      birthDate: 'string',
      statisticsGroupCode: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchEmployee(employeeCompanyId, employeeEmployeeId, employeePatchEmployeeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Employee', 'patchEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEmployee(employeeCompanyId, employeeEmployeeId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Employee', 'getEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeRegistrationEntryCompanyId = 'fakedata';
    const timeRegistrationEntryEmployeeId = 'fakedata';
    const timeRegistrationEntryPostTimeRegistrationEntryForEmployeeBodyParam = {
      id: 'string',
      employeeId: 'string',
      employeeNumber: 'string',
      jobId: 'string',
      jobNumber: 'string',
      absence: 'string',
      lineNumber: 7,
      date: 'string',
      quantity: 7,
      status: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 6,
          picture: [
            {
              id: 'string',
              width: 4,
              height: 5,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 1,
            height: 9,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 6,
                  totalSalesExcludingTax: 4,
                  overdueAmount: 9
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 3,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 4,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 5,
                calculateDiscountOnCreditMemos: false,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: true,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 8,
                totalSalesExcludingTax: 3,
                overdueAmount: 7
              }
            ],
            picture: [
              {
                id: 'string',
                width: 5,
                height: 4,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 3,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 1,
              calculateDiscountOnCreditMemos: true,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModfiedDateTime: 'string'
    };
    describe('#postTimeRegistrationEntryForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTimeRegistrationEntryForEmployee(timeRegistrationEntryCompanyId, timeRegistrationEntryEmployeeId, timeRegistrationEntryPostTimeRegistrationEntryForEmployeeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'postTimeRegistrationEntryForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeRegistrationEntryPostTimeRegistrationEntryBodyParam = {
      id: 'string',
      employeeId: 'string',
      employeeNumber: 'string',
      jobId: 'string',
      jobNumber: 'string',
      absence: 'string',
      lineNumber: 10,
      date: 'string',
      quantity: 3,
      status: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 5,
          picture: [
            {
              id: 'string',
              width: 8,
              height: 9,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 2,
            height: 4,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 4,
                  totalSalesExcludingTax: 2,
                  overdueAmount: 2
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 7,
                  height: 2,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 1,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 7,
                calculateDiscountOnCreditMemos: true,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: true,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 2,
                totalSalesExcludingTax: 10,
                overdueAmount: 5
              }
            ],
            picture: [
              {
                id: 'string',
                width: 6,
                height: 2,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 9,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 9,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModfiedDateTime: 'string'
    };
    describe('#postTimeRegistrationEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTimeRegistrationEntry(timeRegistrationEntryCompanyId, timeRegistrationEntryPostTimeRegistrationEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'postTimeRegistrationEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTimeRegistrationEntriesForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listTimeRegistrationEntriesForEmployee(timeRegistrationEntryCompanyId, timeRegistrationEntryEmployeeId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'listTimeRegistrationEntriesForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeRegistrationEntryTimeRegistrationEntryId = 'fakedata';
    const timeRegistrationEntryPatchTimeRegistrationEntryForEmployeeBodyParam = {
      id: 'string',
      employeeId: 'string',
      employeeNumber: 'string',
      jobId: 'string',
      jobNumber: 'string',
      absence: 'string',
      lineNumber: 8,
      date: 'string',
      quantity: 8,
      status: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 1,
          picture: [
            {
              id: 'string',
              width: 2,
              height: 2,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 4,
            height: 1,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 2,
                  totalSalesExcludingTax: 10,
                  overdueAmount: 7
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 3,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 3,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 4,
                calculateDiscountOnCreditMemos: true,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: true,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 1,
                totalSalesExcludingTax: 8,
                overdueAmount: 3
              }
            ],
            picture: [
              {
                id: 'string',
                width: 5,
                height: 7,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 6,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 9,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModfiedDateTime: 'string'
    };
    describe('#patchTimeRegistrationEntryForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTimeRegistrationEntryForEmployee(timeRegistrationEntryCompanyId, timeRegistrationEntryEmployeeId, timeRegistrationEntryTimeRegistrationEntryId, timeRegistrationEntryPatchTimeRegistrationEntryForEmployeeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'patchTimeRegistrationEntryForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRegistrationEntryForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTimeRegistrationEntryForEmployee(timeRegistrationEntryCompanyId, timeRegistrationEntryEmployeeId, timeRegistrationEntryTimeRegistrationEntryId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'getTimeRegistrationEntryForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTimeRegistrationEntries - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listTimeRegistrationEntries(timeRegistrationEntryCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'listTimeRegistrationEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeRegistrationEntryPatchTimeRegistrationEntryBodyParam = {
      id: 'string',
      employeeId: 'string',
      employeeNumber: 'string',
      jobId: 'string',
      jobNumber: 'string',
      absence: 'string',
      lineNumber: 5,
      date: 'string',
      quantity: 2,
      status: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 2,
          picture: [
            {
              id: 'string',
              width: 1,
              height: 7,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 5,
            height: 6,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      dimensions: [
        {
          code: 'string',
          displayName: 'string',
          valueCode: 'string',
          valueDisplayName: 'string',
          customer: {
            id: 'string',
            number: 'string',
            displayName: 'string',
            type: 'string',
            address: {
              street: 'string',
              city: 'string',
              state: 'string',
              countryLetterCode: 'string',
              postalCode: 'string',
              customerFinancialDetails: [
                {
                  id: 'string',
                  number: 'string',
                  balance: 5,
                  totalSalesExcludingTax: 6,
                  overdueAmount: 5
                }
              ],
              picture: [
                {
                  id: 'string',
                  width: 2,
                  height: 1,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              currency: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                symbol: 'string',
                amountDecimalPlaces: 'string',
                amountRoundingPrecision: 2,
                lastModifiedDateTime: 'string'
              },
              paymentTerm: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                dueDateCalculation: 'string',
                discountDateCalculation: 'string',
                discountPercent: 10,
                calculateDiscountOnCreditMemos: true,
                lastModifiedDateTime: 'string'
              },
              shipmentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              },
              paymentMethod: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            phoneNumber: 'string',
            email: 'string',
            website: 'string',
            taxLiable: false,
            taxAreaId: 'string',
            taxAreaDisplayName: 'string',
            taxRegistrationNumber: 'string',
            currencyId: 'string',
            currencyCode: 'string',
            paymentTermsId: 'string',
            shipmentMethodId: 'string',
            paymentMethodId: 'string',
            blocked: 'string',
            lastModifiedDateTime: 'string',
            customerFinancialDetails: [
              {
                id: 'string',
                number: 'string',
                balance: 8,
                totalSalesExcludingTax: 4,
                overdueAmount: 3
              }
            ],
            picture: [
              {
                id: 'string',
                width: 3,
                height: 8,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            currency: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              symbol: 'string',
              amountDecimalPlaces: 'string',
              amountRoundingPrecision: 6,
              lastModifiedDateTime: 'string'
            },
            paymentTerm: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              dueDateCalculation: 'string',
              discountDateCalculation: 'string',
              discountPercent: 6,
              calculateDiscountOnCreditMemos: false,
              lastModifiedDateTime: 'string'
            },
            shipmentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            },
            paymentMethod: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        }
      ],
      lastModfiedDateTime: 'string'
    };
    describe('#patchTimeRegistrationEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTimeRegistrationEntry(timeRegistrationEntryCompanyId, timeRegistrationEntryTimeRegistrationEntryId, timeRegistrationEntryPatchTimeRegistrationEntryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'patchTimeRegistrationEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeRegistrationEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTimeRegistrationEntry(timeRegistrationEntryCompanyId, timeRegistrationEntryTimeRegistrationEntryId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'getTimeRegistrationEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalLedgerEntryCompanyId = 'fakedata';
    describe('#listGeneralLedgerEntries - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listGeneralLedgerEntries(generalLedgerEntryCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralLedgerEntry', 'listGeneralLedgerEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalLedgerEntryGeneralLedgerEntryId = 555;
    describe('#getGeneralLedgerEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGeneralLedgerEntry(generalLedgerEntryCompanyId, generalLedgerEntryGeneralLedgerEntryId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralLedgerEntry', 'getGeneralLedgerEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const currencyCompanyId = 'fakedata';
    const currencyPostCurrencyBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      symbol: 'string',
      amountDecimalPlaces: 'string',
      amountRoundingPrecision: 10,
      lastModifiedDateTime: 'string'
    };
    describe('#postCurrency - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postCurrency(currencyCompanyId, currencyPostCurrencyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Currency', 'postCurrency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCurrencies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCurrencies(currencyCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Currency', 'listCurrencies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const currencyCurrencyId = 'fakedata';
    const currencyPatchCurrencyBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      symbol: 'string',
      amountDecimalPlaces: 'string',
      amountRoundingPrecision: 1,
      lastModifiedDateTime: 'string'
    };
    describe('#patchCurrency - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCurrency(currencyCompanyId, currencyCurrencyId, currencyPatchCurrencyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Currency', 'patchCurrency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrency - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCurrency(currencyCompanyId, currencyCurrencyId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Currency', 'getCurrency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const paymentMethodCompanyId = 'fakedata';
    const paymentMethodPostPaymentMethodBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postPaymentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPaymentMethod(paymentMethodCompanyId, paymentMethodPostPaymentMethodBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentMethod', 'postPaymentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPaymentMethods - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPaymentMethods(paymentMethodCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentMethod', 'listPaymentMethods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const paymentMethodPaymentMethodId = 'fakedata';
    const paymentMethodPatchPaymentMethodBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchPaymentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPaymentMethod(paymentMethodCompanyId, paymentMethodPaymentMethodId, paymentMethodPatchPaymentMethodBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentMethod', 'patchPaymentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPaymentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPaymentMethod(paymentMethodCompanyId, paymentMethodPaymentMethodId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentMethod', 'getPaymentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dimensionCompanyId = 'fakedata';
    describe('#listDimensions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDimensions(dimensionCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dimension', 'listDimensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dimensionDimensionId = 'fakedata';
    describe('#getDimension - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDimension(dimensionCompanyId, dimensionDimensionId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dimension', 'getDimension', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dimensionValueCompanyId = 'fakedata';
    describe('#listDimensionValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDimensionValues(dimensionValueCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionValue', 'listDimensionValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dimensionValueDimensionValueId = 'fakedata';
    describe('#getDimensionValue - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDimensionValue(dimensionValueCompanyId, dimensionValueDimensionValueId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionValue', 'getDimensionValue', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dimensionValueDimensionId = 'fakedata';
    describe('#listDimensionValuesForDimension - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDimensionValuesForDimension(dimensionValueCompanyId, dimensionValueDimensionId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionValue', 'listDimensionValuesForDimension', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDimensionValueForDimension - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDimensionValueForDimension(dimensionValueCompanyId, dimensionValueDimensionId, dimensionValueDimensionValueId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionValue', 'getDimensionValueForDimension', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dimensionLineCompanyId = 'fakedata';
    const dimensionLinePostDimensionLineBodyParam = {
      parentId: 'string',
      id: 'string',
      code: 'string',
      displayName: 'string',
      valueId: 'string',
      valueCode: 'string',
      valueDisplayName: 'string'
    };
    describe('#postDimensionLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDimensionLine(dimensionLineCompanyId, dimensionLinePostDimensionLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionLine', 'postDimensionLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listDimensionLines - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listDimensionLines(dimensionLineCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionLine', 'listDimensionLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dimensionLineDimensionLineParentId = 'fakedata';
    const dimensionLineDimensionLineId = 'fakedata';
    const dimensionLinePatchDimensionLineBodyParam = {
      parentId: 'string',
      id: 'string',
      code: 'string',
      displayName: 'string',
      valueId: 'string',
      valueCode: 'string',
      valueDisplayName: 'string'
    };
    describe('#patchDimensionLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDimensionLine(dimensionLineCompanyId, dimensionLineDimensionLineParentId, dimensionLineDimensionLineId, dimensionLinePatchDimensionLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionLine', 'patchDimensionLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDimensionLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDimensionLine(dimensionLineCompanyId, dimensionLineDimensionLineParentId, dimensionLineDimensionLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionLine', 'getDimensionLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const paymentTermCompanyId = 'fakedata';
    const paymentTermPostPaymentTermBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      dueDateCalculation: 'string',
      discountDateCalculation: 'string',
      discountPercent: 10,
      calculateDiscountOnCreditMemos: true,
      lastModifiedDateTime: 'string'
    };
    describe('#postPaymentTerm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPaymentTerm(paymentTermCompanyId, paymentTermPostPaymentTermBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentTerm', 'postPaymentTerm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPaymentTerms - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPaymentTerms(paymentTermCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentTerm', 'listPaymentTerms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const paymentTermPaymentTermId = 'fakedata';
    const paymentTermPatchPaymentTermBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      dueDateCalculation: 'string',
      discountDateCalculation: 'string',
      discountPercent: 10,
      calculateDiscountOnCreditMemos: false,
      lastModifiedDateTime: 'string'
    };
    describe('#patchPaymentTerm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPaymentTerm(paymentTermCompanyId, paymentTermPaymentTermId, paymentTermPatchPaymentTermBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentTerm', 'patchPaymentTerm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPaymentTerm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPaymentTerm(paymentTermCompanyId, paymentTermPaymentTermId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentTerm', 'getPaymentTerm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentMethodCompanyId = 'fakedata';
    const shipmentMethodPostShipmentMethodBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postShipmentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postShipmentMethod(shipmentMethodCompanyId, shipmentMethodPostShipmentMethodBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ShipmentMethod', 'postShipmentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listShipmentMethods - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listShipmentMethods(shipmentMethodCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ShipmentMethod', 'listShipmentMethods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const shipmentMethodShipmentMethodId = 'fakedata';
    const shipmentMethodPatchShipmentMethodBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchShipmentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchShipmentMethod(shipmentMethodCompanyId, shipmentMethodShipmentMethodId, shipmentMethodPatchShipmentMethodBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ShipmentMethod', 'patchShipmentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getShipmentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getShipmentMethod(shipmentMethodCompanyId, shipmentMethodShipmentMethodId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ShipmentMethod', 'getShipmentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const itemCategoryCompanyId = 'fakedata';
    const itemCategoryPostItemCategoryBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postItemCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postItemCategory(itemCategoryCompanyId, itemCategoryPostItemCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ItemCategory', 'postItemCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listItemCategories - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listItemCategories(itemCategoryCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ItemCategory', 'listItemCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const itemCategoryItemCategoryId = 'fakedata';
    const itemCategoryPatchItemCategoryBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchItemCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchItemCategory(itemCategoryCompanyId, itemCategoryItemCategoryId, itemCategoryPatchItemCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ItemCategory', 'patchItemCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getItemCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getItemCategory(itemCategoryCompanyId, itemCategoryItemCategoryId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ItemCategory', 'getItemCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cashFlowStatementCompanyId = 'fakedata';
    describe('#listCashFlowStatement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCashFlowStatement(cashFlowStatementCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CashFlowStatement', 'listCashFlowStatement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cashFlowStatementCashFlowStatementLineNumber = 555;
    describe('#getCashFlowStatement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCashFlowStatement(cashFlowStatementCompanyId, cashFlowStatementCashFlowStatementLineNumber, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CashFlowStatement', 'getCashFlowStatement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const countryRegionCompanyId = 'fakedata';
    const countryRegionPostCountryRegionBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      addressFormat: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postCountryRegion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postCountryRegion(countryRegionCompanyId, countryRegionPostCountryRegionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CountryRegion', 'postCountryRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCountriesRegions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCountriesRegions(countryRegionCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CountryRegion', 'listCountriesRegions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const countryRegionCountryRegionId = 'fakedata';
    const countryRegionPatchCountryRegionBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      addressFormat: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchCountryRegion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchCountryRegion(countryRegionCompanyId, countryRegionCountryRegionId, countryRegionPatchCountryRegionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CountryRegion', 'patchCountryRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCountryRegion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCountryRegion(countryRegionCompanyId, countryRegionCountryRegionId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CountryRegion', 'getCountryRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderCompanyId = 'fakedata';
    const salesOrderPostSalesOrderBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      orderDate: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 5,
            totalSalesExcludingTax: 9,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 7,
            height: 3,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 4,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 3,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 3,
            totalSalesExcludingTax: 1,
            overdueAmount: 5
          }
        ],
        picture: [
          {
            id: 'string',
            width: 5,
            height: 5,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 7,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 8,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shippingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 8,
            totalSalesExcludingTax: 9,
            overdueAmount: 5
          }
        ],
        picture: [
          {
            id: 'string',
            width: 2,
            height: 3,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 6,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 5,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      pricesIncludeTax: false,
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      partialShipping: false,
      requestedDeliveryDate: 'string',
      discountAmount: 3,
      discountAppliedBeforeTax: true,
      totalAmountExcludingTax: 5,
      totalTaxAmount: 7,
      totalAmountIncludingTax: 7,
      fullyShipped: false,
      status: 'string',
      lastModifiedDateTime: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#postSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesOrder(salesOrderCompanyId, salesOrderPostSalesOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'postSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderSalesOrderId = 'fakedata';
    describe('#shipAndInvoiceActionSalesOrders - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.shipAndInvoiceActionSalesOrders(salesOrderCompanyId, salesOrderSalesOrderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'shipAndInvoiceActionSalesOrders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesOrders - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesOrders(salesOrderCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'listSalesOrders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderPatchSalesOrderBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      orderDate: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 9,
            overdueAmount: 8
          }
        ],
        picture: [
          {
            id: 'string',
            width: 1,
            height: 9,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 2,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 10,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 3,
            totalSalesExcludingTax: 1,
            overdueAmount: 2
          }
        ],
        picture: [
          {
            id: 'string',
            width: 3,
            height: 10,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 10,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 6,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shippingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 5,
            totalSalesExcludingTax: 5,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 10,
            height: 1,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 3,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 5,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      pricesIncludeTax: false,
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      partialShipping: false,
      requestedDeliveryDate: 'string',
      discountAmount: 2,
      discountAppliedBeforeTax: true,
      totalAmountExcludingTax: 2,
      totalTaxAmount: 4,
      totalAmountIncludingTax: 2,
      fullyShipped: false,
      status: 'string',
      lastModifiedDateTime: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#patchSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesOrder(salesOrderCompanyId, salesOrderSalesOrderId, salesOrderPatchSalesOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'patchSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesOrder(salesOrderCompanyId, salesOrderSalesOrderId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'getSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderLineCompanyId = 'fakedata';
    const salesOrderLinePostSalesOrderLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 7,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: false,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 6,
              picture: [
                {
                  id: 'string',
                  width: 3,
                  height: 1,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 9,
                height: 3,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 2,
          unitPrice: 6,
          priceIncludesTax: true,
          unitCost: 4,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 6,
              height: 8,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 10,
          picture: [
            {
              id: 'string',
              width: 5,
              height: 9,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 7,
            height: 6,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      quantity: 1,
      unitPrice: 3,
      discountAmount: 10,
      discountPercent: 8,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 9,
      taxCode: 'string',
      taxPercent: 9,
      totalTaxAmount: 10,
      amountIncludingTax: 7,
      invoiceDiscountAllocation: 7,
      netAmount: 8,
      netTaxAmount: 2,
      netAmountIncludingTax: 9,
      shipmentDate: 'string',
      shippedQuantity: 9,
      invoicedQuantity: 9,
      invoiceQuantity: 4,
      shipQuantity: 6
    };
    describe('#postSalesOrderLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesOrderLine(salesOrderLineCompanyId, salesOrderLinePostSalesOrderLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'postSalesOrderLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderLineSalesOrderId = 'fakedata';
    const salesOrderLinePostSalesOrderLineForSalesOrderBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 10,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: false,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 2,
              picture: [
                {
                  id: 'string',
                  width: 3,
                  height: 5,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 10,
                height: 5,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 9,
          unitPrice: 7,
          priceIncludesTax: false,
          unitCost: 3,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 6,
              height: 2,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 5,
          picture: [
            {
              id: 'string',
              width: 8,
              height: 1,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 6,
            height: 4,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      quantity: 10,
      unitPrice: 2,
      discountAmount: 1,
      discountPercent: 3,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 1,
      taxCode: 'string',
      taxPercent: 2,
      totalTaxAmount: 2,
      amountIncludingTax: 5,
      invoiceDiscountAllocation: 1,
      netAmount: 2,
      netTaxAmount: 8,
      netAmountIncludingTax: 4,
      shipmentDate: 'string',
      shippedQuantity: 4,
      invoicedQuantity: 2,
      invoiceQuantity: 6,
      shipQuantity: 10
    };
    describe('#postSalesOrderLineForSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesOrderLineForSalesOrder(salesOrderLineCompanyId, salesOrderLineSalesOrderId, salesOrderLinePostSalesOrderLineForSalesOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'postSalesOrderLineForSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesOrderLines - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesOrderLines(salesOrderLineCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'listSalesOrderLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderLineSalesOrderLineId = 'fakedata';
    const salesOrderLinePatchSalesOrderLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 6,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 5,
              picture: [
                {
                  id: 'string',
                  width: 7,
                  height: 7,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 6,
                height: 3,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 2,
          unitPrice: 3,
          priceIncludesTax: true,
          unitCost: 5,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 4,
              height: 8,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 1,
          picture: [
            {
              id: 'string',
              width: 8,
              height: 10,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 3,
            height: 1,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      quantity: 4,
      unitPrice: 1,
      discountAmount: 2,
      discountPercent: 6,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 1,
      taxCode: 'string',
      taxPercent: 10,
      totalTaxAmount: 9,
      amountIncludingTax: 7,
      invoiceDiscountAllocation: 8,
      netAmount: 6,
      netTaxAmount: 1,
      netAmountIncludingTax: 3,
      shipmentDate: 'string',
      shippedQuantity: 3,
      invoicedQuantity: 4,
      invoiceQuantity: 5,
      shipQuantity: 5
    };
    describe('#patchSalesOrderLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesOrderLine(salesOrderLineCompanyId, salesOrderLineSalesOrderLineId, salesOrderLinePatchSalesOrderLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'patchSalesOrderLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesOrderLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesOrderLine(salesOrderLineCompanyId, salesOrderLineSalesOrderLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'getSalesOrderLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesOrderLinesForSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesOrderLinesForSalesOrder(salesOrderLineCompanyId, salesOrderLineSalesOrderId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'listSalesOrderLinesForSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesOrderLinePatchSalesOrderLineForSalesOrderBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 9,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 1,
              picture: [
                {
                  id: 'string',
                  width: 9,
                  height: 8,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 3,
                height: 5,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 6,
          unitPrice: 5,
          priceIncludesTax: false,
          unitCost: 2,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 2,
              height: 5,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 8,
          picture: [
            {
              id: 'string',
              width: 3,
              height: 6,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 8,
            height: 9,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      quantity: 10,
      unitPrice: 7,
      discountAmount: 8,
      discountPercent: 10,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 10,
      taxCode: 'string',
      taxPercent: 4,
      totalTaxAmount: 3,
      amountIncludingTax: 9,
      invoiceDiscountAllocation: 3,
      netAmount: 6,
      netTaxAmount: 5,
      netAmountIncludingTax: 5,
      shipmentDate: 'string',
      shippedQuantity: 3,
      invoicedQuantity: 5,
      invoiceQuantity: 9,
      shipQuantity: 6
    };
    describe('#patchSalesOrderLineForSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesOrderLineForSalesOrder(salesOrderLineCompanyId, salesOrderLineSalesOrderId, salesOrderLineSalesOrderLineId, salesOrderLinePatchSalesOrderLineForSalesOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'patchSalesOrderLineForSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesOrderLineForSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesOrderLineForSalesOrder(salesOrderLineCompanyId, salesOrderLineSalesOrderId, salesOrderLineSalesOrderLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'getSalesOrderLineForSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const retainedEarningsStatementCompanyId = 'fakedata';
    describe('#listRetainedEarningsStatement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listRetainedEarningsStatement(retainedEarningsStatementCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RetainedEarningsStatement', 'listRetainedEarningsStatement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const retainedEarningsStatementRetainedEarningsStatementLineNumber = 555;
    describe('#getRetainedEarningsStatement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getRetainedEarningsStatement(retainedEarningsStatementCompanyId, retainedEarningsStatementRetainedEarningsStatementLineNumber, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RetainedEarningsStatement', 'getRetainedEarningsStatement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const unitOfMeasureCompanyId = 'fakedata';
    const unitOfMeasurePostUnitOfMeasureBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      internationalStandardCode: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postUnitOfMeasure - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postUnitOfMeasure(unitOfMeasureCompanyId, unitOfMeasurePostUnitOfMeasureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UnitOfMeasure', 'postUnitOfMeasure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUnitsOfMeasure - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listUnitsOfMeasure(unitOfMeasureCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UnitOfMeasure', 'listUnitsOfMeasure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const unitOfMeasureUnitOfMeasureId = 'fakedata';
    const unitOfMeasurePatchUnitOfMeasureBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      internationalStandardCode: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchUnitOfMeasure - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchUnitOfMeasure(unitOfMeasureCompanyId, unitOfMeasureUnitOfMeasureId, unitOfMeasurePatchUnitOfMeasureBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UnitOfMeasure', 'patchUnitOfMeasure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnitOfMeasure - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUnitOfMeasure(unitOfMeasureCompanyId, unitOfMeasureUnitOfMeasureId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UnitOfMeasure', 'getUnitOfMeasure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const agedAccountsReceivableCompanyId = 'fakedata';
    describe('#listAgedAccountsReceivable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAgedAccountsReceivable(agedAccountsReceivableCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AgedAccountsReceivable', 'listAgedAccountsReceivable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const agedAccountsReceivableAgedAccountsReceivableCustomerId = 'fakedata';
    describe('#getAgedAccountsReceivable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAgedAccountsReceivable(agedAccountsReceivableCompanyId, agedAccountsReceivableAgedAccountsReceivableCustomerId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AgedAccountsReceivable', 'getAgedAccountsReceivable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const agedAccountsPayableCompanyId = 'fakedata';
    describe('#listAgedAccountsPayable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAgedAccountsPayable(agedAccountsPayableCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AgedAccountsPayable', 'listAgedAccountsPayable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const agedAccountsPayableAgedAccountsPayableVendorId = 'fakedata';
    describe('#getAgedAccountsPayable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAgedAccountsPayable(agedAccountsPayableCompanyId, agedAccountsPayableAgedAccountsPayableVendorId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AgedAccountsPayable', 'getAgedAccountsPayable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const balanceSheetCompanyId = 'fakedata';
    describe('#listBalanceSheet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listBalanceSheet(balanceSheetCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BalanceSheet', 'listBalanceSheet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const balanceSheetBalanceSheetLineNumber = 555;
    describe('#getBalanceSheet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBalanceSheet(balanceSheetCompanyId, balanceSheetBalanceSheetLineNumber, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BalanceSheet', 'getBalanceSheet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trialBalanceCompanyId = 'fakedata';
    describe('#listTrialBalance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listTrialBalance(trialBalanceCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrialBalance', 'listTrialBalance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trialBalanceTrialBalanceNumber = 'fakedata';
    describe('#getTrialBalance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTrialBalance(trialBalanceCompanyId, trialBalanceTrialBalanceNumber, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrialBalance', 'getTrialBalance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incomeStatementCompanyId = 'fakedata';
    describe('#listIncomeStatement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listIncomeStatement(incomeStatementCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IncomeStatement', 'listIncomeStatement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incomeStatementIncomeStatementLineNumber = 555;
    describe('#getIncomeStatement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIncomeStatement(incomeStatementCompanyId, incomeStatementIncomeStatementLineNumber, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IncomeStatement', 'getIncomeStatement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taxAreaCompanyId = 'fakedata';
    const taxAreaPostTaxAreaBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      taxType: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postTaxArea - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTaxArea(taxAreaCompanyId, taxAreaPostTaxAreaBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxArea', 'postTaxArea', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTaxAreas - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listTaxAreas(taxAreaCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxArea', 'listTaxAreas', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taxAreaTaxAreaId = 'fakedata';
    const taxAreaPatchTaxAreaBodyParam = {
      id: 'string',
      code: 'string',
      displayName: 'string',
      taxType: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchTaxArea - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchTaxArea(taxAreaCompanyId, taxAreaTaxAreaId, taxAreaPatchTaxAreaBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxArea', 'patchTaxArea', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaxArea - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTaxArea(taxAreaCompanyId, taxAreaTaxAreaId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxArea', 'getTaxArea', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesQuoteCompanyId = 'fakedata';
    const salesQuotePostSalesQuoteBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      documentDate: 'string',
      dueDate: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 9,
            totalSalesExcludingTax: 9,
            overdueAmount: 1
          }
        ],
        picture: [
          {
            id: 'string',
            width: 10,
            height: 3,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 1,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 7,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 1,
            overdueAmount: 2
          }
        ],
        picture: [
          {
            id: 'string',
            width: 5,
            height: 8,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 8,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 8,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shippingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 2,
            totalSalesExcludingTax: 4,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 6,
            height: 8,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 8,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 10,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      discountAmount: 10,
      totalAmountExcludingTax: 9,
      totalTaxAmount: 2,
      totalAmountIncludingTax: 4,
      status: 'string',
      sentDate: 'string',
      validUntilDate: 'string',
      acceptedDate: 'string',
      lastModifiedDateTime: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#postSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesQuote(salesQuoteCompanyId, salesQuotePostSalesQuoteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'postSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesQuoteSalesQuoteId = 'fakedata';
    describe('#makeInvoiceActionSalesQuotes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.makeInvoiceActionSalesQuotes(salesQuoteCompanyId, salesQuoteSalesQuoteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'makeInvoiceActionSalesQuotes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#makeOrderActionSalesQuotes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.makeOrderActionSalesQuotes(salesQuoteCompanyId, salesQuoteSalesQuoteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'makeOrderActionSalesQuotes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendActionSalesQuotes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sendActionSalesQuotes(salesQuoteCompanyId, salesQuoteSalesQuoteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'sendActionSalesQuotes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesQuotes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesQuotes(salesQuoteCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'listSalesQuotes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesQuotePatchSalesQuoteBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      documentDate: 'string',
      dueDate: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 9,
            totalSalesExcludingTax: 7,
            overdueAmount: 10
          }
        ],
        picture: [
          {
            id: 'string',
            width: 4,
            height: 7,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 9,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 3,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 7,
            totalSalesExcludingTax: 7,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 3,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 4,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 5,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shippingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 7,
            totalSalesExcludingTax: 9,
            overdueAmount: 5
          }
        ],
        picture: [
          {
            id: 'string',
            width: 6,
            height: 3,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 10,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 3,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      discountAmount: 5,
      totalAmountExcludingTax: 7,
      totalTaxAmount: 3,
      totalAmountIncludingTax: 4,
      status: 'string',
      sentDate: 'string',
      validUntilDate: 'string',
      acceptedDate: 'string',
      lastModifiedDateTime: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#patchSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesQuote(salesQuoteCompanyId, salesQuoteSalesQuoteId, salesQuotePatchSalesQuoteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'patchSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesQuote(salesQuoteCompanyId, salesQuoteSalesQuoteId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'getSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesQuoteLineCompanyId = 'fakedata';
    const salesQuoteLinePostSalesQuoteLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 3,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 7,
              picture: [
                {
                  id: 'string',
                  width: 8,
                  height: 1,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 4,
                height: 4,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 3,
          unitPrice: 10,
          priceIncludesTax: false,
          unitCost: 9,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 5,
              height: 6,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 5,
          picture: [
            {
              id: 'string',
              width: 1,
              height: 4,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 7,
            height: 8,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 3,
      quantity: 8,
      discountAmount: 10,
      discountPercent: 5,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 6,
      taxCode: 'string',
      taxPercent: 9,
      totalTaxAmount: 10,
      amountIncludingTax: 2,
      netAmount: 7,
      netTaxAmount: 5,
      netAmountIncludingTax: 1
    };
    describe('#postSalesQuoteLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesQuoteLine(salesQuoteLineCompanyId, salesQuoteLinePostSalesQuoteLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'postSalesQuoteLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesQuoteLineSalesQuoteId = 'fakedata';
    const salesQuoteLinePostSalesQuoteLineForSalesQuoteBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 7,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 6,
              picture: [
                {
                  id: 'string',
                  width: 7,
                  height: 6,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 3,
                height: 10,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 7,
          unitPrice: 7,
          priceIncludesTax: true,
          unitCost: 5,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 2,
              height: 6,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 6,
          picture: [
            {
              id: 'string',
              width: 8,
              height: 3,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 8,
            height: 7,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 3,
      quantity: 2,
      discountAmount: 10,
      discountPercent: 4,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 8,
      taxCode: 'string',
      taxPercent: 5,
      totalTaxAmount: 1,
      amountIncludingTax: 8,
      netAmount: 10,
      netTaxAmount: 7,
      netAmountIncludingTax: 2
    };
    describe('#postSalesQuoteLineForSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesQuoteLineForSalesQuote(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteId, salesQuoteLinePostSalesQuoteLineForSalesQuoteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'postSalesQuoteLineForSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesQuoteLines - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesQuoteLines(salesQuoteLineCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'listSalesQuoteLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesQuoteLineSalesQuoteLineId = 'fakedata';
    const salesQuoteLinePatchSalesQuoteLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 7,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 5,
              picture: [
                {
                  id: 'string',
                  width: 7,
                  height: 1,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 7,
                height: 7,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 4,
          unitPrice: 3,
          priceIncludesTax: false,
          unitCost: 3,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 5,
              height: 5,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 8,
          picture: [
            {
              id: 'string',
              width: 9,
              height: 3,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 2,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 1,
      quantity: 9,
      discountAmount: 4,
      discountPercent: 1,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 10,
      taxCode: 'string',
      taxPercent: 2,
      totalTaxAmount: 10,
      amountIncludingTax: 9,
      netAmount: 8,
      netTaxAmount: 2,
      netAmountIncludingTax: 8
    };
    describe('#patchSalesQuoteLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesQuoteLine(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteLineId, salesQuoteLinePatchSalesQuoteLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'patchSalesQuoteLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesQuoteLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesQuoteLine(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'getSalesQuoteLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesQuoteLinesForSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesQuoteLinesForSalesQuote(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'listSalesQuoteLinesForSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesQuoteLinePatchSalesQuoteLineForSalesQuoteBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 10,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: false,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 5,
              picture: [
                {
                  id: 'string',
                  width: 9,
                  height: 5,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 2,
                height: 3,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 9,
          unitPrice: 4,
          priceIncludesTax: true,
          unitCost: 10,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 7,
              height: 4,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 2,
          picture: [
            {
              id: 'string',
              width: 4,
              height: 5,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 3,
            height: 3,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 4,
      quantity: 7,
      discountAmount: 3,
      discountPercent: 2,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 9,
      taxCode: 'string',
      taxPercent: 6,
      totalTaxAmount: 10,
      amountIncludingTax: 6,
      netAmount: 8,
      netTaxAmount: 2,
      netAmountIncludingTax: 3
    };
    describe('#patchSalesQuoteLineForSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesQuoteLineForSalesQuote(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteId, salesQuoteLineSalesQuoteLineId, salesQuoteLinePatchSalesQuoteLineForSalesQuoteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'patchSalesQuoteLineForSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesQuoteLineForSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesQuoteLineForSalesQuote(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteId, salesQuoteLineSalesQuoteLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'getSalesQuoteLineForSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesCreditMemoCompanyId = 'fakedata';
    const salesCreditMemoPostSalesCreditMemoBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      creditMemoDate: 'string',
      dueDate: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 1,
            totalSalesExcludingTax: 7,
            overdueAmount: 5
          }
        ],
        picture: [
          {
            id: 'string',
            width: 5,
            height: 7,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 5,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 6,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 1,
            totalSalesExcludingTax: 5,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 2,
            height: 9,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 9,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 9,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      pricesIncludeTax: true,
      discountAmount: 1,
      discountAppliedBeforeTax: true,
      totalAmountExcludingTax: 8,
      totalTaxAmount: 10,
      totalAmountIncludingTax: 8,
      status: 'string',
      lastModifiedDateTime: 'string',
      invoiceId: 'string',
      invoiceNumber: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#postSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesCreditMemo(salesCreditMemoCompanyId, salesCreditMemoPostSalesCreditMemoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'postSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesCreditMemoSalesCreditMemoId = 'fakedata';
    describe('#cancelActionSalesCreditMemos - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelActionSalesCreditMemos(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'cancelActionSalesCreditMemos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelAndSendActionSalesCreditMemos - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelAndSendActionSalesCreditMemos(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'cancelAndSendActionSalesCreditMemos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postActionSalesCreditMemos - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postActionSalesCreditMemos(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'postActionSalesCreditMemos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAndSendActionSalesCreditMemos - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAndSendActionSalesCreditMemos(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'postAndSendActionSalesCreditMemos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendActionSalesCreditMemos - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sendActionSalesCreditMemos(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'sendActionSalesCreditMemos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesCreditMemos - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesCreditMemos(salesCreditMemoCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'listSalesCreditMemos', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesCreditMemoPatchSalesCreditMemoBodyParam = {
      id: 'string',
      number: 'string',
      externalDocumentNumber: 'string',
      creditMemoDate: 'string',
      dueDate: 'string',
      customerId: 'string',
      contactId: 'string',
      customerNumber: 'string',
      customerName: 'string',
      billToName: 'string',
      billToCustomerId: 'string',
      billToCustomerNumber: 'string',
      sellingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 9,
            totalSalesExcludingTax: 6,
            overdueAmount: 4
          }
        ],
        picture: [
          {
            id: 'string',
            width: 5,
            height: 8,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 6,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 1,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      billingPostalAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 2,
            totalSalesExcludingTax: 4,
            overdueAmount: 9
          }
        ],
        picture: [
          {
            id: 'string',
            width: 10,
            height: 4,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 6,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 6,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      paymentTermsId: 'string',
      shipmentMethodId: 'string',
      salesperson: 'string',
      pricesIncludeTax: false,
      discountAmount: 4,
      discountAppliedBeforeTax: false,
      totalAmountExcludingTax: 8,
      totalTaxAmount: 7,
      totalAmountIncludingTax: 7,
      status: 'string',
      lastModifiedDateTime: 'string',
      invoiceId: 'string',
      invoiceNumber: 'string',
      phoneNumber: 'string',
      email: 'string'
    };
    describe('#patchSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesCreditMemo(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, salesCreditMemoPatchSalesCreditMemoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'patchSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesCreditMemo(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'getSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesCreditMemoLineCompanyId = 'fakedata';
    const salesCreditMemoLinePostSalesCreditMemoLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 10,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 8,
              picture: [
                {
                  id: 'string',
                  width: 5,
                  height: 9,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 10,
                height: 5,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 4,
          unitPrice: 8,
          priceIncludesTax: true,
          unitCost: 3,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 8,
              height: 9,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 3,
          picture: [
            {
              id: 'string',
              width: 10,
              height: 3,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 7,
            height: 7,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 3,
      quantity: 1,
      discountAmount: 1,
      discountPercent: 8,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 7,
      taxCode: 'string',
      taxPercent: 3,
      totalTaxAmount: 10,
      amountIncludingTax: 2,
      invoiceDiscountAllocation: 7,
      netAmount: 6,
      netTaxAmount: 3,
      netAmountIncludingTax: 3,
      shipmentDate: 'string'
    };
    describe('#postSalesCreditMemoLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesCreditMemoLine(salesCreditMemoLineCompanyId, salesCreditMemoLinePostSalesCreditMemoLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'postSalesCreditMemoLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesCreditMemoLineSalesCreditMemoId = 'fakedata';
    const salesCreditMemoLinePostSalesCreditMemoLineForSalesCreditMemoBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 4,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 4,
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 6,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 4,
                height: 9,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 7,
          unitPrice: 4,
          priceIncludesTax: true,
          unitCost: 4,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 4,
              height: 1,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 1,
          picture: [
            {
              id: 'string',
              width: 2,
              height: 7,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 7,
            height: 3,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 1,
      quantity: 2,
      discountAmount: 8,
      discountPercent: 10,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 5,
      taxCode: 'string',
      taxPercent: 9,
      totalTaxAmount: 10,
      amountIncludingTax: 4,
      invoiceDiscountAllocation: 9,
      netAmount: 4,
      netTaxAmount: 5,
      netAmountIncludingTax: 8,
      shipmentDate: 'string'
    };
    describe('#postSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSalesCreditMemoLineForSalesCreditMemo(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoId, salesCreditMemoLinePostSalesCreditMemoLineForSalesCreditMemoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'postSalesCreditMemoLineForSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesCreditMemoLines - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesCreditMemoLines(salesCreditMemoLineCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'listSalesCreditMemoLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesCreditMemoLineSalesCreditMemoLineId = 'fakedata';
    const salesCreditMemoLinePatchSalesCreditMemoLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 6,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 4,
              picture: [
                {
                  id: 'string',
                  width: 9,
                  height: 5,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 10,
                height: 8,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 3,
          unitPrice: 7,
          priceIncludesTax: true,
          unitCost: 3,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 7,
              height: 8,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 4,
          picture: [
            {
              id: 'string',
              width: 3,
              height: 8,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 9,
            height: 9,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 1,
      quantity: 5,
      discountAmount: 9,
      discountPercent: 8,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 9,
      taxCode: 'string',
      taxPercent: 3,
      totalTaxAmount: 1,
      amountIncludingTax: 1,
      invoiceDiscountAllocation: 6,
      netAmount: 7,
      netTaxAmount: 5,
      netAmountIncludingTax: 6,
      shipmentDate: 'string'
    };
    describe('#patchSalesCreditMemoLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesCreditMemoLine(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoLineId, salesCreditMemoLinePatchSalesCreditMemoLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'patchSalesCreditMemoLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesCreditMemoLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesCreditMemoLine(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'getSalesCreditMemoLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listSalesCreditMemoLinesForSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listSalesCreditMemoLinesForSalesCreditMemo(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'listSalesCreditMemoLinesForSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const salesCreditMemoLinePatchSalesCreditMemoLineForSalesCreditMemoBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 1,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 3,
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 9,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 8,
                height: 10,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 10,
          unitPrice: 3,
          priceIncludesTax: true,
          unitCost: 1,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 9,
              height: 4,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasureId: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 5,
          picture: [
            {
              id: 'string',
              width: 3,
              height: 8,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 2,
            height: 10,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitPrice: 7,
      quantity: 4,
      discountAmount: 1,
      discountPercent: 2,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 8,
      taxCode: 'string',
      taxPercent: 5,
      totalTaxAmount: 9,
      amountIncludingTax: 5,
      invoiceDiscountAllocation: 8,
      netAmount: 9,
      netTaxAmount: 10,
      netAmountIncludingTax: 2,
      shipmentDate: 'string'
    };
    describe('#patchSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchSalesCreditMemoLineForSalesCreditMemo(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoId, salesCreditMemoLineSalesCreditMemoLineId, salesCreditMemoLinePatchSalesCreditMemoLineForSalesCreditMemoBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'patchSalesCreditMemoLineForSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSalesCreditMemoLineForSalesCreditMemo(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoId, salesCreditMemoLineSalesCreditMemoLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'getSalesCreditMemoLineForSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalLedgerEntryAttachmentsCompanyId = 'fakedata';
    const generalLedgerEntryAttachmentsPostGeneralLedgerEntryAttachmentsBodyParam = {
      generalLedgerEntryNumber: 8,
      id: 'string',
      fileName: 'string',
      byteSize: 10,
      content: 'string',
      createdDateTime: 'string'
    };
    describe('#postGeneralLedgerEntryAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postGeneralLedgerEntryAttachments(generalLedgerEntryAttachmentsCompanyId, generalLedgerEntryAttachmentsPostGeneralLedgerEntryAttachmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralLedgerEntryAttachments', 'postGeneralLedgerEntryAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listGeneralLedgerEntryAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listGeneralLedgerEntryAttachments(generalLedgerEntryAttachmentsCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralLedgerEntryAttachments', 'listGeneralLedgerEntryAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsGeneralLedgerEntryNumber = 555;
    const generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsId = 'fakedata';
    const generalLedgerEntryAttachmentsPatchGeneralLedgerEntryAttachmentsBodyParam = {
      generalLedgerEntryNumber: 8,
      id: 'string',
      fileName: 'string',
      byteSize: 1,
      content: 'string',
      createdDateTime: 'string'
    };
    describe('#patchGeneralLedgerEntryAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchGeneralLedgerEntryAttachments(generalLedgerEntryAttachmentsCompanyId, generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsGeneralLedgerEntryNumber, generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsId, generalLedgerEntryAttachmentsPatchGeneralLedgerEntryAttachmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralLedgerEntryAttachments', 'patchGeneralLedgerEntryAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGeneralLedgerEntryAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGeneralLedgerEntryAttachments(generalLedgerEntryAttachmentsCompanyId, generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsGeneralLedgerEntryNumber, generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralLedgerEntryAttachments', 'getGeneralLedgerEntryAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseInvoiceCompanyId = 'fakedata';
    const purchaseInvoicePostPurchaseInvoiceBodyParam = {
      id: 'string',
      number: 'string',
      invoiceDate: 'string',
      dueDate: 'string',
      vendorInvoiceNumber: 'string',
      vendorId: 'string',
      vendorNumber: 'string',
      vendorName: 'string',
      payToName: 'string',
      payToContact: 'string',
      payToVendorId: 'string',
      payToVendorNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      buyFromAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 7,
            totalSalesExcludingTax: 4,
            overdueAmount: 3
          }
        ],
        picture: [
          {
            id: 'string',
            width: 2,
            height: 5,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 7,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 1,
          calculateDiscountOnCreditMemos: false,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      payToAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 7,
            totalSalesExcludingTax: 1,
            overdueAmount: 9
          }
        ],
        picture: [
          {
            id: 'string',
            width: 4,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 3,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 4,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shipToAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 1,
            totalSalesExcludingTax: 9,
            overdueAmount: 9
          }
        ],
        picture: [
          {
            id: 'string',
            width: 7,
            height: 1,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 8,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 5,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      pricesIncludeTax: false,
      discountAmount: 3,
      discountAppliedBeforeTax: true,
      totalAmountExcludingTax: 3,
      totalTaxAmount: 9,
      totalAmountIncludingTax: 9,
      status: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#postPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPurchaseInvoice(purchaseInvoiceCompanyId, purchaseInvoicePostPurchaseInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoice', 'postPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseInvoicePurchaseInvoiceId = 'fakedata';
    describe('#postActionPurchaseInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postActionPurchaseInvoices(purchaseInvoiceCompanyId, purchaseInvoicePurchaseInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoice', 'postActionPurchaseInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPurchaseInvoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPurchaseInvoices(purchaseInvoiceCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoice', 'listPurchaseInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseInvoicePatchPurchaseInvoiceBodyParam = {
      id: 'string',
      number: 'string',
      invoiceDate: 'string',
      dueDate: 'string',
      vendorInvoiceNumber: 'string',
      vendorId: 'string',
      vendorNumber: 'string',
      vendorName: 'string',
      payToName: 'string',
      payToContact: 'string',
      payToVendorId: 'string',
      payToVendorNumber: 'string',
      shipToName: 'string',
      shipToContact: 'string',
      buyFromAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 4,
            totalSalesExcludingTax: 7,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 10,
            height: 2,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 8,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 10,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      payToAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 3,
            totalSalesExcludingTax: 3,
            overdueAmount: 7
          }
        ],
        picture: [
          {
            id: 'string',
            width: 2,
            height: 4,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 4,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 10,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      shipToAddress: {
        street: 'string',
        city: 'string',
        state: 'string',
        countryLetterCode: 'string',
        postalCode: 'string',
        customerFinancialDetails: [
          {
            id: 'string',
            number: 'string',
            balance: 6,
            totalSalesExcludingTax: 4,
            overdueAmount: 4
          }
        ],
        picture: [
          {
            id: 'string',
            width: 9,
            height: 3,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        currency: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          symbol: 'string',
          amountDecimalPlaces: 'string',
          amountRoundingPrecision: 10,
          lastModifiedDateTime: 'string'
        },
        paymentTerm: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          dueDateCalculation: 'string',
          discountDateCalculation: 'string',
          discountPercent: 1,
          calculateDiscountOnCreditMemos: true,
          lastModifiedDateTime: 'string'
        },
        shipmentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        },
        paymentMethod: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      currencyId: 'string',
      currencyCode: 'string',
      pricesIncludeTax: false,
      discountAmount: 8,
      discountAppliedBeforeTax: false,
      totalAmountExcludingTax: 1,
      totalTaxAmount: 9,
      totalAmountIncludingTax: 10,
      status: 'string',
      lastModifiedDateTime: 'string'
    };
    describe('#patchPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPurchaseInvoice(purchaseInvoiceCompanyId, purchaseInvoicePurchaseInvoiceId, purchaseInvoicePatchPurchaseInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoice', 'patchPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPurchaseInvoice(purchaseInvoiceCompanyId, purchaseInvoicePurchaseInvoiceId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoice', 'getPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseInvoiceLineCompanyId = 'fakedata';
    const purchaseInvoiceLinePostPurchaseInvoiceLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 9,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 5,
              picture: [
                {
                  id: 'string',
                  width: 1,
                  height: 6,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 9,
                height: 10,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 8,
          unitPrice: 2,
          priceIncludesTax: false,
          unitCost: 2,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 1,
              height: 3,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 3,
          picture: [
            {
              id: 'string',
              width: 6,
              height: 7,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 7,
            height: 1,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitCost: 3,
      quantity: 3,
      discountAmount: 7,
      discountPercent: 8,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 2,
      taxCode: 'string',
      taxPercent: 10,
      totalTaxAmount: 3,
      amountIncludingTax: 7,
      invoiceDiscountAllocation: 4,
      netAmount: 5,
      netTaxAmount: 2,
      netAmountIncludingTax: 6,
      expectedReceiptDate: 'string'
    };
    describe('#postPurchaseInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPurchaseInvoiceLine(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePostPurchaseInvoiceLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'postPurchaseInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseInvoiceLinePurchaseInvoiceId = 'fakedata';
    const purchaseInvoiceLinePostPurchaseInvoiceLineForPurchaseInvoiceBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 7,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: false,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 7,
              picture: [
                {
                  id: 'string',
                  width: 6,
                  height: 3,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 8,
                height: 7,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 7,
          unitPrice: 8,
          priceIncludesTax: false,
          unitCost: 2,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 7,
              height: 4,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 4,
          picture: [
            {
              id: 'string',
              width: 1,
              height: 3,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 4,
            height: 1,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: true,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitCost: 10,
      quantity: 8,
      discountAmount: 10,
      discountPercent: 4,
      discountAppliedBeforeTax: true,
      amountExcludingTax: 10,
      taxCode: 'string',
      taxPercent: 3,
      totalTaxAmount: 8,
      amountIncludingTax: 10,
      invoiceDiscountAllocation: 9,
      netAmount: 6,
      netTaxAmount: 8,
      netAmountIncludingTax: 5,
      expectedReceiptDate: 'string'
    };
    describe('#postPurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPurchaseInvoiceLineForPurchaseInvoice(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceId, purchaseInvoiceLinePostPurchaseInvoiceLineForPurchaseInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'postPurchaseInvoiceLineForPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPurchaseInvoiceLines - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPurchaseInvoiceLines(purchaseInvoiceLineCompanyId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'listPurchaseInvoiceLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseInvoiceLinePurchaseInvoiceLineId = 'fakedata';
    const purchaseInvoiceLinePatchPurchaseInvoiceLineBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 7,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: false,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 5,
              picture: [
                {
                  id: 'string',
                  width: 1,
                  height: 2,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: true,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 9,
                height: 5,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: true,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 1,
          unitPrice: 5,
          priceIncludesTax: true,
          unitCost: 8,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 5,
              height: 4,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: true,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 1,
          picture: [
            {
              id: 'string',
              width: 3,
              height: 8,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: true,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 9,
            height: 10,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitCost: 1,
      quantity: 8,
      discountAmount: 2,
      discountPercent: 3,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 1,
      taxCode: 'string',
      taxPercent: 3,
      totalTaxAmount: 10,
      amountIncludingTax: 3,
      invoiceDiscountAllocation: 4,
      netAmount: 6,
      netTaxAmount: 4,
      netAmountIncludingTax: 6,
      expectedReceiptDate: 'string'
    };
    describe('#patchPurchaseInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPurchaseInvoiceLine(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceLineId, purchaseInvoiceLinePatchPurchaseInvoiceLineBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'patchPurchaseInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPurchaseInvoiceLine(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'getPurchaseInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listPurchaseInvoiceLinesForPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listPurchaseInvoiceLinesForPurchaseInvoice(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'listPurchaseInvoiceLinesForPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseInvoiceLinePatchPurchaseInvoiceLineForPurchaseInvoiceBodyParam = {
      id: 'string',
      documentId: 'string',
      sequence: 6,
      itemId: 'string',
      accountId: 'string',
      lineType: 'string',
      lineDetails: {
        number: 'string',
        displayName: 'string',
        item: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          type: 'string',
          itemCategoryId: 'string',
          itemCategoryCode: 'string',
          blocked: true,
          baseUnitOfMeasureId: 'string',
          baseUnitOfMeasure: {
            code: 'string',
            displayName: 'string',
            symbol: 'string',
            unitConversion: {
              toUnitOfMeasure: 'string',
              fromToConversionRate: 9,
              picture: [
                {
                  id: 'string',
                  width: 9,
                  height: 1,
                  contentType: 'string',
                  'content@odata.mediaEditLink': 'string',
                  'content@odata.mediaReadLink': 'string'
                }
              ],
              defaultDimensions: [
                {
                  parentId: 'string',
                  dimensionId: 'string',
                  dimensionCode: 'string',
                  dimensionValueId: 'string',
                  dimensionValueCode: 'string',
                  postingValidation: 'string',
                  account: {
                    id: 'string',
                    number: 'string',
                    displayName: 'string',
                    category: 'string',
                    subCategory: 'string',
                    blocked: false,
                    lastModifiedDateTime: 'string'
                  },
                  dimension: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string',
                    dimensionValues: [
                      {
                        id: 'string',
                        code: 'string',
                        displayName: 'string',
                        lastModifiedDateTime: 'string'
                      }
                    ]
                  },
                  dimensionValue: {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                }
              ],
              itemCategory: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            },
            picture: [
              {
                id: 'string',
                width: 1,
                height: 2,
                contentType: 'string',
                'content@odata.mediaEditLink': 'string',
                'content@odata.mediaReadLink': 'string'
              }
            ],
            defaultDimensions: [
              {
                parentId: 'string',
                dimensionId: 'string',
                dimensionCode: 'string',
                dimensionValueId: 'string',
                dimensionValueCode: 'string',
                postingValidation: 'string',
                account: {
                  id: 'string',
                  number: 'string',
                  displayName: 'string',
                  category: 'string',
                  subCategory: 'string',
                  blocked: false,
                  lastModifiedDateTime: 'string'
                },
                dimension: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string',
                  dimensionValues: [
                    {
                      id: 'string',
                      code: 'string',
                      displayName: 'string',
                      lastModifiedDateTime: 'string'
                    }
                  ]
                },
                dimensionValue: {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              }
            ],
            itemCategory: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          },
          gtin: 'string',
          inventory: 9,
          unitPrice: 7,
          priceIncludesTax: true,
          unitCost: 10,
          taxGroupId: 'string',
          taxGroupCode: 'string',
          lastModifiedDateTime: 'string',
          picture: [
            {
              id: 'string',
              width: 1,
              height: 10,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        account: {
          id: 'string',
          number: 'string',
          displayName: 'string',
          category: 'string',
          subCategory: 'string',
          blocked: false,
          lastModifiedDateTime: 'string'
        }
      },
      description: 'string',
      unitOfMeasure: {
        code: 'string',
        displayName: 'string',
        symbol: 'string',
        unitConversion: {
          toUnitOfMeasure: 'string',
          fromToConversionRate: 4,
          picture: [
            {
              id: 'string',
              width: 7,
              height: 3,
              contentType: 'string',
              'content@odata.mediaEditLink': 'string',
              'content@odata.mediaReadLink': 'string'
            }
          ],
          defaultDimensions: [
            {
              parentId: 'string',
              dimensionId: 'string',
              dimensionCode: 'string',
              dimensionValueId: 'string',
              dimensionValueCode: 'string',
              postingValidation: 'string',
              account: {
                id: 'string',
                number: 'string',
                displayName: 'string',
                category: 'string',
                subCategory: 'string',
                blocked: false,
                lastModifiedDateTime: 'string'
              },
              dimension: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string',
                dimensionValues: [
                  {
                    id: 'string',
                    code: 'string',
                    displayName: 'string',
                    lastModifiedDateTime: 'string'
                  }
                ]
              },
              dimensionValue: {
                id: 'string',
                code: 'string',
                displayName: 'string',
                lastModifiedDateTime: 'string'
              }
            }
          ],
          itemCategory: {
            id: 'string',
            code: 'string',
            displayName: 'string',
            lastModifiedDateTime: 'string'
          }
        },
        picture: [
          {
            id: 'string',
            width: 9,
            height: 8,
            contentType: 'string',
            'content@odata.mediaEditLink': 'string',
            'content@odata.mediaReadLink': 'string'
          }
        ],
        defaultDimensions: [
          {
            parentId: 'string',
            dimensionId: 'string',
            dimensionCode: 'string',
            dimensionValueId: 'string',
            dimensionValueCode: 'string',
            postingValidation: 'string',
            account: {
              id: 'string',
              number: 'string',
              displayName: 'string',
              category: 'string',
              subCategory: 'string',
              blocked: false,
              lastModifiedDateTime: 'string'
            },
            dimension: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string',
              dimensionValues: [
                {
                  id: 'string',
                  code: 'string',
                  displayName: 'string',
                  lastModifiedDateTime: 'string'
                }
              ]
            },
            dimensionValue: {
              id: 'string',
              code: 'string',
              displayName: 'string',
              lastModifiedDateTime: 'string'
            }
          }
        ],
        itemCategory: {
          id: 'string',
          code: 'string',
          displayName: 'string',
          lastModifiedDateTime: 'string'
        }
      },
      unitCost: 10,
      quantity: 1,
      discountAmount: 3,
      discountPercent: 2,
      discountAppliedBeforeTax: false,
      amountExcludingTax: 9,
      taxCode: 'string',
      taxPercent: 4,
      totalTaxAmount: 10,
      amountIncludingTax: 5,
      invoiceDiscountAllocation: 9,
      netAmount: 5,
      netTaxAmount: 1,
      netAmountIncludingTax: 9,
      expectedReceiptDate: 'string'
    };
    describe('#patchPurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchPurchaseInvoiceLineForPurchaseInvoice(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceId, purchaseInvoiceLinePurchaseInvoiceLineId, purchaseInvoiceLinePatchPurchaseInvoiceLineForPurchaseInvoiceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'patchPurchaseInvoiceLineForPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPurchaseInvoiceLineForPurchaseInvoice(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceId, purchaseInvoiceLinePurchaseInvoiceLineId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'getPurchaseInvoiceLineForPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectCompanyId = 'fakedata';
    const projectPostProjectBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string'
    };
    describe('#postProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postProject(projectCompanyId, projectPostProjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Project', 'postProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listProjects - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listProjects(projectCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Project', 'listProjects', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectProjectId = 'fakedata';
    const projectPatchProjectBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string'
    };
    describe('#patchProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchProject(projectCompanyId, projectProjectId, projectPatchProjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Project', 'patchProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getProject(projectCompanyId, projectProjectId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Project', 'getProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bankAccountCompanyId = 'fakedata';
    const bankAccountPostBankAccountBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string'
    };
    describe('#postBankAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBankAccount(bankAccountCompanyId, bankAccountPostBankAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BankAccount', 'postBankAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listBankAccounts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listBankAccounts(bankAccountCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BankAccount', 'listBankAccounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bankAccountBankAccountId = 'fakedata';
    const bankAccountPatchBankAccountBodyParam = {
      id: 'string',
      number: 'string',
      displayName: 'string'
    };
    describe('#patchBankAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchBankAccount(bankAccountCompanyId, bankAccountBankAccountId, bankAccountPatchBankAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BankAccount', 'patchBankAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBankAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getBankAccount(bankAccountCompanyId, bankAccountBankAccountId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BankAccount', 'getBankAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerSaleCompanyId = 'fakedata';
    describe('#listCustomerSales - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listCustomerSales(customerSaleCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerSale', 'listCustomerSales', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customerSaleCustomerSaleCustomerId = 'fakedata';
    const customerSaleCustomerSaleCustomerNumber = 'fakedata';
    const customerSaleCustomerSaleName = 'fakedata';
    describe('#getCustomerSale - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCustomerSale(customerSaleCompanyId, customerSaleCustomerSaleCustomerId, customerSaleCustomerSaleCustomerNumber, customerSaleCustomerSaleName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerSale', 'getCustomerSale', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorPurchaseCompanyId = 'fakedata';
    describe('#listVendorPurchases - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listVendorPurchases(vendorPurchaseCompanyId, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VendorPurchase', 'listVendorPurchases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorPurchaseVendorPurchaseVendorId = 'fakedata';
    const vendorPurchaseVendorPurchaseVendorNumber = 'fakedata';
    const vendorPurchaseVendorPurchaseName = 'fakedata';
    describe('#getVendorPurchase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVendorPurchase(vendorPurchaseCompanyId, vendorPurchaseVendorPurchaseVendorId, vendorPurchaseVendorPurchaseVendorNumber, vendorPurchaseVendorPurchaseName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VendorPurchase', 'getVendorPurchase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteItem(itemCompanyId, itemItemId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Item', 'deleteItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePictureForCustomer(pictureCompanyId, pictureCustomerId, picturePictureId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'deletePictureForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePictureForEmployee(pictureCompanyId, pictureEmployeeId, picturePictureId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'deletePictureForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePictureForItem(pictureCompanyId, pictureItemId, picturePictureId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'deletePictureForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePicture - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePicture(pictureCompanyId, picturePictureId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'deletePicture', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePictureForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePictureForVendor(pictureCompanyId, pictureVendorId, picturePictureId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Picture', 'deletePictureForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDefaultDimensionsForCustomer(defaultDimensionsCompanyId, defaultDimensionsCustomerId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'deleteDefaultDimensionsForCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDefaultDimensions(defaultDimensionsCompanyId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'deleteDefaultDimensions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDefaultDimensionsForEmployee(defaultDimensionsCompanyId, defaultDimensionsEmployeeId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'deleteDefaultDimensionsForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDefaultDimensionsForItem(defaultDimensionsCompanyId, defaultDimensionsItemId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'deleteDefaultDimensionsForItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDefaultDimensionsForVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDefaultDimensionsForVendor(defaultDimensionsCompanyId, defaultDimensionsVendorId, defaultDimensionsDefaultDimensionsParentId, defaultDimensionsDefaultDimensionsDimensionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DefaultDimensions', 'deleteDefaultDimensionsForVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomer(customerCompanyId, customerCustomerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Customer', 'deleteCustomer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVendor - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVendor(vendorCompanyId, vendorVendorId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'deleteVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesInvoice(salesInvoiceCompanyId, salesInvoiceSalesInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoice', 'deleteSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesInvoiceLine(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'deleteSalesInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesInvoiceLineForSalesInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesInvoiceLineForSalesInvoice(salesInvoiceLineCompanyId, salesInvoiceLineSalesInvoiceId, salesInvoiceLineSalesInvoiceLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesInvoiceLine', 'deleteSalesInvoiceLineForSalesInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomerPaymentJournal(customerPaymentJournalCompanyId, customerPaymentJournalCustomerPaymentJournalId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPaymentJournal', 'deleteCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerPaymentForCustomerPaymentJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomerPaymentForCustomerPaymentJournal(customerPaymentCompanyId, customerPaymentCustomerPaymentJournalId, customerPaymentCustomerPaymentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'deleteCustomerPaymentForCustomerPaymentJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerPayment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomerPayment(customerPaymentCompanyId, customerPaymentCustomerPaymentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomerPayment', 'deleteCustomerPayment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTaxGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTaxGroup(taxGroupCompanyId, taxGroupTaxGroupId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxGroup', 'deleteTaxGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJournal(journalCompanyId, journalJournalId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Journal', 'deleteJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJournalLine(journalLineCompanyId, journalLineJournalLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'deleteJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJournalLineForJournal(journalLineCompanyId, journalLineJournalId, journalLineJournalLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JournalLine', 'deleteJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAttachments(attachmentsCompanyId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'deleteAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachmentsForJournalLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAttachmentsForJournalLine(attachmentsCompanyId, attachmentsJournalLineId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'deleteAttachmentsForJournalLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachmentsForJournalLineForJournal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAttachmentsForJournalLineForJournal(attachmentsCompanyId, attachmentsJournalId, attachmentsJournalLineId, attachmentsAttachmentsParentId, attachmentsAttachmentsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachments', 'deleteAttachmentsForJournalLineForJournal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEmployee(employeeCompanyId, employeeEmployeeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Employee', 'deleteEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeRegistrationEntryForEmployee - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTimeRegistrationEntryForEmployee(timeRegistrationEntryCompanyId, timeRegistrationEntryEmployeeId, timeRegistrationEntryTimeRegistrationEntryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'deleteTimeRegistrationEntryForEmployee', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeRegistrationEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTimeRegistrationEntry(timeRegistrationEntryCompanyId, timeRegistrationEntryTimeRegistrationEntryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeRegistrationEntry', 'deleteTimeRegistrationEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCurrency - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCurrency(currencyCompanyId, currencyCurrencyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Currency', 'deleteCurrency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePaymentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePaymentMethod(paymentMethodCompanyId, paymentMethodPaymentMethodId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentMethod', 'deletePaymentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDimensionLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDimensionLine(dimensionLineCompanyId, dimensionLineDimensionLineParentId, dimensionLineDimensionLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DimensionLine', 'deleteDimensionLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePaymentTerm - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePaymentTerm(paymentTermCompanyId, paymentTermPaymentTermId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PaymentTerm', 'deletePaymentTerm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteShipmentMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteShipmentMethod(shipmentMethodCompanyId, shipmentMethodShipmentMethodId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ShipmentMethod', 'deleteShipmentMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteItemCategory - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteItemCategory(itemCategoryCompanyId, itemCategoryItemCategoryId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ItemCategory', 'deleteItemCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCountryRegion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCountryRegion(countryRegionCompanyId, countryRegionCountryRegionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CountryRegion', 'deleteCountryRegion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesOrder(salesOrderCompanyId, salesOrderSalesOrderId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrder', 'deleteSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesOrderLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesOrderLine(salesOrderLineCompanyId, salesOrderLineSalesOrderLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'deleteSalesOrderLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesOrderLineForSalesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesOrderLineForSalesOrder(salesOrderLineCompanyId, salesOrderLineSalesOrderId, salesOrderLineSalesOrderLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesOrderLine', 'deleteSalesOrderLineForSalesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUnitOfMeasure - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUnitOfMeasure(unitOfMeasureCompanyId, unitOfMeasureUnitOfMeasureId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UnitOfMeasure', 'deleteUnitOfMeasure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTaxArea - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTaxArea(taxAreaCompanyId, taxAreaTaxAreaId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TaxArea', 'deleteTaxArea', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesQuote(salesQuoteCompanyId, salesQuoteSalesQuoteId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuote', 'deleteSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesQuoteLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesQuoteLine(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'deleteSalesQuoteLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesQuoteLineForSalesQuote - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesQuoteLineForSalesQuote(salesQuoteLineCompanyId, salesQuoteLineSalesQuoteId, salesQuoteLineSalesQuoteLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesQuoteLine', 'deleteSalesQuoteLineForSalesQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesCreditMemo(salesCreditMemoCompanyId, salesCreditMemoSalesCreditMemoId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemo', 'deleteSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesCreditMemoLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesCreditMemoLine(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'deleteSalesCreditMemoLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSalesCreditMemoLineForSalesCreditMemo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSalesCreditMemoLineForSalesCreditMemo(salesCreditMemoLineCompanyId, salesCreditMemoLineSalesCreditMemoId, salesCreditMemoLineSalesCreditMemoLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SalesCreditMemoLine', 'deleteSalesCreditMemoLineForSalesCreditMemo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGeneralLedgerEntryAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGeneralLedgerEntryAttachments(generalLedgerEntryAttachmentsCompanyId, generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsGeneralLedgerEntryNumber, generalLedgerEntryAttachmentsGeneralLedgerEntryAttachmentsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GeneralLedgerEntryAttachments', 'deleteGeneralLedgerEntryAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePurchaseInvoice(purchaseInvoiceCompanyId, purchaseInvoicePurchaseInvoiceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoice', 'deletePurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseInvoiceLine - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePurchaseInvoiceLine(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'deletePurchaseInvoiceLine', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseInvoiceLineForPurchaseInvoice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePurchaseInvoiceLineForPurchaseInvoice(purchaseInvoiceLineCompanyId, purchaseInvoiceLinePurchaseInvoiceId, purchaseInvoiceLinePurchaseInvoiceLineId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseInvoiceLine', 'deletePurchaseInvoiceLineForPurchaseInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProject - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteProject(projectCompanyId, projectProjectId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Project', 'deleteProject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBankAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteBankAccount(bankAccountCompanyId, bankAccountBankAccountId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-microsoft_dynamics-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BankAccount', 'deleteBankAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
