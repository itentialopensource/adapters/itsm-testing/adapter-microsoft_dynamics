## Authenticating Microsoft Dynamics Adapter 

This document will go through the steps for authenticating the Microsoft Dynamics adapter with OAuth Authentication. Properly configuring the properties for an adapter in IAP is critical for getting the adapter online. You can read more about adapter authentication <a href="https://docs.itential.com/opensource/docs/authentication" target="_blank">HERE</a>.

### OAuth Authentication
The Microsoft Dynamics adapter requires OAuth Authentication. If you change authentication methods, you should change this section accordingly and merge it back into the adapter repository.

STEPS  
1. Ensure you have access to a Microsoft Dynamics server and that it is running
2. Follow the steps in the README.md to import the adapter into IAP if you have not already done so
3. The auth requires two extra fields, ```tenant_id under``` under ```properties.authentication``` and ```resource``` under ```properties.request``` field.
```json
"authentication": {
  "auth_method": "request_token",
  "tenant_id": "tenantId",
  "username": "username",
  "password": "password",
  "token_timeout": 3600000,
  "token_cache": "local",
  "invalid_token_error": 401,
  "auth_field": "header.headers.Authorization",
  "auth_field_format": "Bearer {token}",
  "auth_logging": false,
  "client_id": "clientId",
  "client_secret": "clientSecret",
  "grant_type": "client_credentials"
}

"request": {
  "number_redirects": 0,
  "number_retries": 3,
  "limit_retry_error": [
    0
  ],
  "failover_codes": [],
  "attempt_timeout": 5000,
  "global_request": {
    "payload": {},
    "uriOptions": {},
    "addlHeaders": {},
    "authData": {
      "resource": "https://domain.dynamics.com"
    }
  },
  "healthcheck_on_timeout": true,
  "return_raw": false,
  "archiving": false,
  "return_request": false
}
```
4. Restart the adapter. If your properties were set correctly, the adapter should go online. 

### Troubleshooting
- Make sure you copied over the correct client_id, client_secret, grant_type, tenant_id and resource.
- Turn on debug level logs for the adapter in IAP Admin Essentials.
- Turn on auth_logging for the adapter in IAP Admin Essentials (adapter properties).
- Investigate the logs - in particular:
  - The FULL REQUEST log to make sure the proper headers are being sent with the request.
  - The FULL BODY log to make sure the payload is accurate.
  - The CALL RETURN log to see what the other system is telling us.
- Remember when you are done to turn auth_logging off as you do not want to log credentials.
