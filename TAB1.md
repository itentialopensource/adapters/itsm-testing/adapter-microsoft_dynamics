# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Microsoft_dynamics System. The API that was used to build the adapter for Microsoft_dynamics is usually available in the report directory of this adapter. The adapter utilizes the Microsoft_dynamics API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Microsoft Dynamics adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Microsoft's Dynamics 365.

With this adapter you have the ability to perform operations with Microsoft Dynamics on items such as:

- Account
- Project
- Purchase Invoice
- Sales Quote

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
