## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Microsoft Dynamics. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Microsoft Dynamics.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>


### Specific Adapter Calls

Specific adapter calls are built based on the API of the Microsoft_dynamics. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">listCompanies(top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCompanies</td>
    <td style="padding:15px">{base_path}/{version}/companies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCompany(companyId, select, callback)</td>
    <td style="padding:15px">getCompany</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listItems(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listItems</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postItem(companyId, body, callback)</td>
    <td style="padding:15px">postItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/items?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItem(companyId, itemId, expand, select, callback)</td>
    <td style="padding:15px">getItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteItem(companyId, itemId, callback)</td>
    <td style="padding:15px">deleteItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchItem(companyId, itemId, body, callback)</td>
    <td style="padding:15px">patchItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPictureForItem(companyId, itemId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPictureForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/picture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPictureForItem(companyId, itemId, pictureId, select, callback)</td>
    <td style="padding:15px">getPictureForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePictureForItem(companyId, itemId, pictureId, callback)</td>
    <td style="padding:15px">deletePictureForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPictureForItem(companyId, itemId, pictureId, body, callback)</td>
    <td style="padding:15px">patchPictureForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPicture(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPicture</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/picture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPicture(companyId, pictureId, select, callback)</td>
    <td style="padding:15px">getPicture</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePicture(companyId, pictureId, callback)</td>
    <td style="padding:15px">deletePicture</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPicture(companyId, pictureId, body, callback)</td>
    <td style="padding:15px">patchPicture</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPictureForCustomer(companyId, customerId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPictureForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/picture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPictureForCustomer(companyId, customerId, pictureId, select, callback)</td>
    <td style="padding:15px">getPictureForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePictureForCustomer(companyId, customerId, pictureId, callback)</td>
    <td style="padding:15px">deletePictureForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPictureForCustomer(companyId, customerId, pictureId, body, callback)</td>
    <td style="padding:15px">patchPictureForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPictureForVendor(companyId, vendorId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPictureForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/picture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPictureForVendor(companyId, vendorId, pictureId, select, callback)</td>
    <td style="padding:15px">getPictureForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePictureForVendor(companyId, vendorId, pictureId, callback)</td>
    <td style="padding:15px">deletePictureForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPictureForVendor(companyId, vendorId, pictureId, body, callback)</td>
    <td style="padding:15px">patchPictureForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPictureForEmployee(companyId, employeeId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPictureForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/picture?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPictureForEmployee(companyId, employeeId, pictureId, select, callback)</td>
    <td style="padding:15px">getPictureForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePictureForEmployee(companyId, employeeId, pictureId, callback)</td>
    <td style="padding:15px">deletePictureForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPictureForEmployee(companyId, employeeId, pictureId, body, callback)</td>
    <td style="padding:15px">patchPictureForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultDimensionsForItem(companyId, itemId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listDefaultDimensionsForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDefaultDimensionsForItem(companyId, itemId, body, callback)</td>
    <td style="padding:15px">postDefaultDimensionsForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultDimensionsForItem(companyId, itemId, defaultDimensionsParentId, defaultDimensionsDimensionId, expand, select, callback)</td>
    <td style="padding:15px">getDefaultDimensionsForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDefaultDimensionsForItem(companyId, itemId, defaultDimensionsParentId, defaultDimensionsDimensionId, callback)</td>
    <td style="padding:15px">deleteDefaultDimensionsForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDefaultDimensionsForItem(companyId, itemId, defaultDimensionsParentId, defaultDimensionsDimensionId, body, callback)</td>
    <td style="padding:15px">patchDefaultDimensionsForItem</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultDimensions(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listDefaultDimensions</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDefaultDimensions(companyId, body, callback)</td>
    <td style="padding:15px">postDefaultDimensions</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultDimensions(companyId, defaultDimensionsParentId, defaultDimensionsDimensionId, expand, select, callback)</td>
    <td style="padding:15px">getDefaultDimensions</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDefaultDimensions(companyId, defaultDimensionsParentId, defaultDimensionsDimensionId, callback)</td>
    <td style="padding:15px">deleteDefaultDimensions</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDefaultDimensions(companyId, defaultDimensionsParentId, defaultDimensionsDimensionId, body, callback)</td>
    <td style="padding:15px">patchDefaultDimensions</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultDimensionsForCustomer(companyId, customerId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listDefaultDimensionsForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDefaultDimensionsForCustomer(companyId, customerId, body, callback)</td>
    <td style="padding:15px">postDefaultDimensionsForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultDimensionsForCustomer(companyId, customerId, defaultDimensionsParentId, defaultDimensionsDimensionId, expand, select, callback)</td>
    <td style="padding:15px">getDefaultDimensionsForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDefaultDimensionsForCustomer(companyId, customerId, defaultDimensionsParentId, defaultDimensionsDimensionId, callback)</td>
    <td style="padding:15px">deleteDefaultDimensionsForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDefaultDimensionsForCustomer(companyId, customerId, defaultDimensionsParentId, defaultDimensionsDimensionId, body, callback)</td>
    <td style="padding:15px">patchDefaultDimensionsForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultDimensionsForVendor(companyId, vendorId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listDefaultDimensionsForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDefaultDimensionsForVendor(companyId, vendorId, body, callback)</td>
    <td style="padding:15px">postDefaultDimensionsForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultDimensionsForVendor(companyId, vendorId, defaultDimensionsParentId, defaultDimensionsDimensionId, expand, select, callback)</td>
    <td style="padding:15px">getDefaultDimensionsForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDefaultDimensionsForVendor(companyId, vendorId, defaultDimensionsParentId, defaultDimensionsDimensionId, callback)</td>
    <td style="padding:15px">deleteDefaultDimensionsForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDefaultDimensionsForVendor(companyId, vendorId, defaultDimensionsParentId, defaultDimensionsDimensionId, body, callback)</td>
    <td style="padding:15px">patchDefaultDimensionsForVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDefaultDimensionsForEmployee(companyId, employeeId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listDefaultDimensionsForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDefaultDimensionsForEmployee(companyId, employeeId, body, callback)</td>
    <td style="padding:15px">postDefaultDimensionsForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/defaultDimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultDimensionsForEmployee(companyId, employeeId, defaultDimensionsParentId, defaultDimensionsDimensionId, expand, select, callback)</td>
    <td style="padding:15px">getDefaultDimensionsForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDefaultDimensionsForEmployee(companyId, employeeId, defaultDimensionsParentId, defaultDimensionsDimensionId, callback)</td>
    <td style="padding:15px">deleteDefaultDimensionsForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDefaultDimensionsForEmployee(companyId, employeeId, defaultDimensionsParentId, defaultDimensionsDimensionId, body, callback)</td>
    <td style="padding:15px">patchDefaultDimensionsForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomers(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listCustomers</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomer(companyId, body, callback)</td>
    <td style="padding:15px">postCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomer(companyId, customerId, expand, select, callback)</td>
    <td style="padding:15px">getCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomer(companyId, customerId, callback)</td>
    <td style="padding:15px">deleteCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCustomer(companyId, customerId, body, callback)</td>
    <td style="padding:15px">patchCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerFinancialDetailsForCustomer(companyId, customerId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCustomerFinancialDetailsForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/customerFinancialDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerFinancialDetailForCustomer(companyId, customerId, customerFinancialDetailId, select, callback)</td>
    <td style="padding:15px">getCustomerFinancialDetailForCustomer</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerFinancialDetails(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCustomerFinancialDetails</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customerFinancialDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerFinancialDetail(companyId, customerFinancialDetailId, select, callback)</td>
    <td style="padding:15px">getCustomerFinancialDetail</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVendors(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listVendors</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVendor(companyId, body, callback)</td>
    <td style="padding:15px">postVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendor(companyId, vendorId, expand, select, callback)</td>
    <td style="padding:15px">getVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVendor(companyId, vendorId, callback)</td>
    <td style="padding:15px">deleteVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVendor(companyId, vendorId, body, callback)</td>
    <td style="padding:15px">patchVendor</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCompanyInformation(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCompanyInformation</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/companyInformation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCompanyInformation(companyId, companyInformationId, select, callback)</td>
    <td style="padding:15px">getCompanyInformation</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCompanyInformation(companyId, companyInformationId, body, callback)</td>
    <td style="padding:15px">patchCompanyInformation</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesInvoices(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesInvoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesInvoice(companyId, body, callback)</td>
    <td style="padding:15px">postSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesInvoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesInvoice(companyId, salesInvoiceId, expand, select, callback)</td>
    <td style="padding:15px">getSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesInvoice(companyId, salesInvoiceId, callback)</td>
    <td style="padding:15px">deleteSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesInvoice(companyId, salesInvoiceId, body, callback)</td>
    <td style="padding:15px">patchSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelAndSendActionSalesInvoices(companyId, salesInvoiceId, callback)</td>
    <td style="padding:15px">cancelAndSendActionSalesInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.cancelAndSend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelActionSalesInvoices(companyId, salesInvoiceId, callback)</td>
    <td style="padding:15px">cancelActionSalesInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">makeCorrectiveCreditMemoActionSalesInvoices(companyId, salesInvoiceId, callback)</td>
    <td style="padding:15px">makeCorrectiveCreditMemoActionSalesInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.makeCorrectiveCreditMemo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAndSendActionSalesInvoices(companyId, salesInvoiceId, callback)</td>
    <td style="padding:15px">postAndSendActionSalesInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.postAndSend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postActionSalesInvoices(companyId, salesInvoiceId, callback)</td>
    <td style="padding:15px">postActionSalesInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.post?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendActionSalesInvoices(companyId, salesInvoiceId, callback)</td>
    <td style="padding:15px">sendActionSalesInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.send?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesInvoiceLinesForSalesInvoice(companyId, salesInvoiceId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesInvoiceLinesForSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesInvoiceLineForSalesInvoice(companyId, salesInvoiceId, body, callback)</td>
    <td style="padding:15px">postSalesInvoiceLineForSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesInvoiceLineForSalesInvoice(companyId, salesInvoiceId, salesInvoiceLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesInvoiceLineForSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesInvoiceLineForSalesInvoice(companyId, salesInvoiceId, salesInvoiceLineId, callback)</td>
    <td style="padding:15px">deleteSalesInvoiceLineForSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesInvoiceLineForSalesInvoice(companyId, salesInvoiceId, salesInvoiceLineId, body, callback)</td>
    <td style="padding:15px">patchSalesInvoiceLineForSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesInvoiceLines(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesInvoiceLines</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesInvoiceLine(companyId, body, callback)</td>
    <td style="padding:15px">postSalesInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesInvoiceLine(companyId, salesInvoiceLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesInvoiceLine(companyId, salesInvoiceLineId, callback)</td>
    <td style="padding:15px">deleteSalesInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesInvoiceLine(companyId, salesInvoiceLineId, body, callback)</td>
    <td style="padding:15px">patchSalesInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPdfDocumentForSalesInvoice(companyId, salesInvoiceId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPdfDocumentForSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/pdfDocument?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPdfDocumentForSalesInvoice(companyId, salesInvoiceId, pdfDocumentId, select, callback)</td>
    <td style="padding:15px">getPdfDocumentForSalesInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPdfDocument(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPdfDocument</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/pdfDocument?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPdfDocument(companyId, pdfDocumentId, select, callback)</td>
    <td style="padding:15px">getPdfDocument</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPdfDocumentForSalesQuote(companyId, salesQuoteId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPdfDocumentForSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/pdfDocument?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPdfDocumentForSalesQuote(companyId, salesQuoteId, pdfDocumentId, select, callback)</td>
    <td style="padding:15px">getPdfDocumentForSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPdfDocumentForSalesCreditMemo(companyId, salesCreditMemoId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPdfDocumentForSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/pdfDocument?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPdfDocumentForSalesCreditMemo(companyId, salesCreditMemoId, pdfDocumentId, select, callback)</td>
    <td style="padding:15px">getPdfDocumentForSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPdfDocumentForPurchaseInvoice(companyId, purchaseInvoiceId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPdfDocumentForPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/pdfDocument?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPdfDocumentForPurchaseInvoice(companyId, purchaseInvoiceId, pdfDocumentId, select, callback)</td>
    <td style="padding:15px">getPdfDocumentForPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerPaymentJournals(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listCustomerPaymentJournals</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customerPaymentJournals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomerPaymentJournal(companyId, body, callback)</td>
    <td style="padding:15px">postCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customerPaymentJournals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerPaymentJournal(companyId, customerPaymentJournalId, expand, select, callback)</td>
    <td style="padding:15px">getCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomerPaymentJournal(companyId, customerPaymentJournalId, callback)</td>
    <td style="padding:15px">deleteCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCustomerPaymentJournal(companyId, customerPaymentJournalId, body, callback)</td>
    <td style="padding:15px">patchCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerPaymentsForCustomerPaymentJournal(companyId, customerPaymentJournalId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listCustomerPaymentsForCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/customerPayments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomerPaymentForCustomerPaymentJournal(companyId, customerPaymentJournalId, body, callback)</td>
    <td style="padding:15px">postCustomerPaymentForCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/customerPayments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerPaymentForCustomerPaymentJournal(companyId, customerPaymentJournalId, customerPaymentId, expand, select, callback)</td>
    <td style="padding:15px">getCustomerPaymentForCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomerPaymentForCustomerPaymentJournal(companyId, customerPaymentJournalId, customerPaymentId, callback)</td>
    <td style="padding:15px">deleteCustomerPaymentForCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCustomerPaymentForCustomerPaymentJournal(companyId, customerPaymentJournalId, customerPaymentId, body, callback)</td>
    <td style="padding:15px">patchCustomerPaymentForCustomerPaymentJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerPayments(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listCustomerPayments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customerPayments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomerPayment(companyId, body, callback)</td>
    <td style="padding:15px">postCustomerPayment</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customerPayments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerPayment(companyId, customerPaymentId, expand, select, callback)</td>
    <td style="padding:15px">getCustomerPayment</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomerPayment(companyId, customerPaymentId, callback)</td>
    <td style="padding:15px">deleteCustomerPayment</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCustomerPayment(companyId, customerPaymentId, body, callback)</td>
    <td style="padding:15px">patchCustomerPayment</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAccounts(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listAccounts</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/accounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccount(companyId, accountId, select, callback)</td>
    <td style="padding:15px">getAccount</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTaxGroups(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listTaxGroups</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/taxGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTaxGroup(companyId, body, callback)</td>
    <td style="padding:15px">postTaxGroup</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/taxGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaxGroup(companyId, taxGroupId, select, callback)</td>
    <td style="padding:15px">getTaxGroup</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTaxGroup(companyId, taxGroupId, callback)</td>
    <td style="padding:15px">deleteTaxGroup</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTaxGroup(companyId, taxGroupId, body, callback)</td>
    <td style="padding:15px">patchTaxGroup</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listJournals(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listJournals</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/journals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJournal(companyId, body, callback)</td>
    <td style="padding:15px">postJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/journals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJournal(companyId, journalId, expand, select, callback)</td>
    <td style="padding:15px">getJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJournal(companyId, journalId, callback)</td>
    <td style="padding:15px">deleteJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchJournal(companyId, journalId, body, callback)</td>
    <td style="padding:15px">patchJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postActionJournals(companyId, journalId, callback)</td>
    <td style="padding:15px">postActionJournals</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.post?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listJournalLinesForJournal(companyId, journalId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listJournalLinesForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/journalLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJournalLineForJournal(companyId, journalId, body, callback)</td>
    <td style="padding:15px">postJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/journalLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJournalLineForJournal(companyId, journalId, journalLineId, expand, select, callback)</td>
    <td style="padding:15px">getJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJournalLineForJournal(companyId, journalId, journalLineId, callback)</td>
    <td style="padding:15px">deleteJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchJournalLineForJournal(companyId, journalId, journalLineId, body, callback)</td>
    <td style="padding:15px">patchJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listJournalLines(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listJournalLines</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/journalLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJournalLine(companyId, body, callback)</td>
    <td style="padding:15px">postJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/journalLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJournalLine(companyId, journalLineId, expand, select, callback)</td>
    <td style="padding:15px">getJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJournalLine(companyId, journalLineId, callback)</td>
    <td style="padding:15px">deleteJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchJournalLine(companyId, journalLineId, body, callback)</td>
    <td style="padding:15px">patchJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachmentsForJournalLineForJournal(companyId, journalId, journalLineId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listAttachmentsForJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAttachmentsForJournalLineForJournal(companyId, journalId, journalLineId, body, callback)</td>
    <td style="padding:15px">postAttachmentsForJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachmentsForJournalLineForJournal(companyId, journalId, journalLineId, attachmentsParentId, attachmentsId, select, callback)</td>
    <td style="padding:15px">getAttachmentsForJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttachmentsForJournalLineForJournal(companyId, journalId, journalLineId, attachmentsParentId, attachmentsId, callback)</td>
    <td style="padding:15px">deleteAttachmentsForJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAttachmentsForJournalLineForJournal(companyId, journalId, journalLineId, attachmentsParentId, attachmentsId, body, callback)</td>
    <td style="padding:15px">patchAttachmentsForJournalLineForJournal</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachmentsForJournalLine(companyId, journalLineId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listAttachmentsForJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAttachmentsForJournalLine(companyId, journalLineId, body, callback)</td>
    <td style="padding:15px">postAttachmentsForJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachmentsForJournalLine(companyId, journalLineId, attachmentsParentId, attachmentsId, select, callback)</td>
    <td style="padding:15px">getAttachmentsForJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttachmentsForJournalLine(companyId, journalLineId, attachmentsParentId, attachmentsId, callback)</td>
    <td style="padding:15px">deleteAttachmentsForJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAttachmentsForJournalLine(companyId, journalLineId, attachmentsParentId, attachmentsId, body, callback)</td>
    <td style="padding:15px">patchAttachmentsForJournalLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttachments(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAttachments(companyId, body, callback)</td>
    <td style="padding:15px">postAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachments(companyId, attachmentsParentId, attachmentsId, select, callback)</td>
    <td style="padding:15px">getAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttachments(companyId, attachmentsParentId, attachmentsId, callback)</td>
    <td style="padding:15px">deleteAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchAttachments(companyId, attachmentsParentId, attachmentsId, body, callback)</td>
    <td style="padding:15px">patchAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listEmployees(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listEmployees</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/employees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEmployee(companyId, body, callback)</td>
    <td style="padding:15px">postEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/employees?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmployee(companyId, employeeId, expand, select, callback)</td>
    <td style="padding:15px">getEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEmployee(companyId, employeeId, callback)</td>
    <td style="padding:15px">deleteEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEmployee(companyId, employeeId, body, callback)</td>
    <td style="padding:15px">patchEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTimeRegistrationEntriesForEmployee(companyId, employeeId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listTimeRegistrationEntriesForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/timeRegistrationEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTimeRegistrationEntryForEmployee(companyId, employeeId, body, callback)</td>
    <td style="padding:15px">postTimeRegistrationEntryForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/timeRegistrationEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeRegistrationEntryForEmployee(companyId, employeeId, timeRegistrationEntryId, expand, select, callback)</td>
    <td style="padding:15px">getTimeRegistrationEntryForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTimeRegistrationEntryForEmployee(companyId, employeeId, timeRegistrationEntryId, callback)</td>
    <td style="padding:15px">deleteTimeRegistrationEntryForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTimeRegistrationEntryForEmployee(companyId, employeeId, timeRegistrationEntryId, body, callback)</td>
    <td style="padding:15px">patchTimeRegistrationEntryForEmployee</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTimeRegistrationEntries(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listTimeRegistrationEntries</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/timeRegistrationEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTimeRegistrationEntry(companyId, body, callback)</td>
    <td style="padding:15px">postTimeRegistrationEntry</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/timeRegistrationEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeRegistrationEntry(companyId, timeRegistrationEntryId, expand, select, callback)</td>
    <td style="padding:15px">getTimeRegistrationEntry</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTimeRegistrationEntry(companyId, timeRegistrationEntryId, callback)</td>
    <td style="padding:15px">deleteTimeRegistrationEntry</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTimeRegistrationEntry(companyId, timeRegistrationEntryId, body, callback)</td>
    <td style="padding:15px">patchTimeRegistrationEntry</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGeneralLedgerEntries(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listGeneralLedgerEntries</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/generalLedgerEntries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeneralLedgerEntry(companyId, generalLedgerEntryId, expand, select, callback)</td>
    <td style="padding:15px">getGeneralLedgerEntry</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCurrencies(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCurrencies</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/currencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCurrency(companyId, body, callback)</td>
    <td style="padding:15px">postCurrency</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/currencies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrency(companyId, currencyId, select, callback)</td>
    <td style="padding:15px">getCurrency</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCurrency(companyId, currencyId, callback)</td>
    <td style="padding:15px">deleteCurrency</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCurrency(companyId, currencyId, body, callback)</td>
    <td style="padding:15px">patchCurrency</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPaymentMethods(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPaymentMethods</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/paymentMethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPaymentMethod(companyId, body, callback)</td>
    <td style="padding:15px">postPaymentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/paymentMethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPaymentMethod(companyId, paymentMethodId, select, callback)</td>
    <td style="padding:15px">getPaymentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePaymentMethod(companyId, paymentMethodId, callback)</td>
    <td style="padding:15px">deletePaymentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPaymentMethod(companyId, paymentMethodId, body, callback)</td>
    <td style="padding:15px">patchPaymentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDimensions(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listDimensions</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/dimensions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDimension(companyId, dimensionId, expand, select, callback)</td>
    <td style="padding:15px">getDimension</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDimensionValuesForDimension(companyId, dimensionId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listDimensionValuesForDimension</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/dimensionValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDimensionValueForDimension(companyId, dimensionId, dimensionValueId, select, callback)</td>
    <td style="padding:15px">getDimensionValueForDimension</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDimensionValues(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listDimensionValues</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/dimensionValues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDimensionValue(companyId, dimensionValueId, select, callback)</td>
    <td style="padding:15px">getDimensionValue</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listDimensionLines(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listDimensionLines</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/dimensionLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDimensionLine(companyId, body, callback)</td>
    <td style="padding:15px">postDimensionLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/dimensionLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDimensionLine(companyId, dimensionLineParentId, dimensionLineId, expand, select, callback)</td>
    <td style="padding:15px">getDimensionLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDimensionLine(companyId, dimensionLineParentId, dimensionLineId, callback)</td>
    <td style="padding:15px">deleteDimensionLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDimensionLine(companyId, dimensionLineParentId, dimensionLineId, body, callback)</td>
    <td style="padding:15px">patchDimensionLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPaymentTerms(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listPaymentTerms</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/paymentTerms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPaymentTerm(companyId, body, callback)</td>
    <td style="padding:15px">postPaymentTerm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/paymentTerms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPaymentTerm(companyId, paymentTermId, select, callback)</td>
    <td style="padding:15px">getPaymentTerm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePaymentTerm(companyId, paymentTermId, callback)</td>
    <td style="padding:15px">deletePaymentTerm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPaymentTerm(companyId, paymentTermId, body, callback)</td>
    <td style="padding:15px">patchPaymentTerm</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listShipmentMethods(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listShipmentMethods</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/shipmentMethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postShipmentMethod(companyId, body, callback)</td>
    <td style="padding:15px">postShipmentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/shipmentMethods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getShipmentMethod(companyId, shipmentMethodId, select, callback)</td>
    <td style="padding:15px">getShipmentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteShipmentMethod(companyId, shipmentMethodId, callback)</td>
    <td style="padding:15px">deleteShipmentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchShipmentMethod(companyId, shipmentMethodId, body, callback)</td>
    <td style="padding:15px">patchShipmentMethod</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listItemCategories(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listItemCategories</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/itemCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postItemCategory(companyId, body, callback)</td>
    <td style="padding:15px">postItemCategory</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/itemCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getItemCategory(companyId, itemCategoryId, select, callback)</td>
    <td style="padding:15px">getItemCategory</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteItemCategory(companyId, itemCategoryId, callback)</td>
    <td style="padding:15px">deleteItemCategory</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchItemCategory(companyId, itemCategoryId, body, callback)</td>
    <td style="padding:15px">patchItemCategory</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCashFlowStatement(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCashFlowStatement</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/cashFlowStatement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCashFlowStatement(companyId, cashFlowStatementLineNumber, select, callback)</td>
    <td style="padding:15px">getCashFlowStatement</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCountriesRegions(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCountriesRegions</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/countriesRegions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCountryRegion(companyId, body, callback)</td>
    <td style="padding:15px">postCountryRegion</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/countriesRegions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCountryRegion(companyId, countryRegionId, select, callback)</td>
    <td style="padding:15px">getCountryRegion</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCountryRegion(companyId, countryRegionId, callback)</td>
    <td style="padding:15px">deleteCountryRegion</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCountryRegion(companyId, countryRegionId, body, callback)</td>
    <td style="padding:15px">patchCountryRegion</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesOrders(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesOrders</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesOrders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesOrder(companyId, body, callback)</td>
    <td style="padding:15px">postSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesOrders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesOrder(companyId, salesOrderId, expand, select, callback)</td>
    <td style="padding:15px">getSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesOrder(companyId, salesOrderId, callback)</td>
    <td style="padding:15px">deleteSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesOrder(companyId, salesOrderId, body, callback)</td>
    <td style="padding:15px">patchSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">shipAndInvoiceActionSalesOrders(companyId, salesOrderId, callback)</td>
    <td style="padding:15px">shipAndInvoiceActionSalesOrders</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.shipAndInvoice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesOrderLinesForSalesOrder(companyId, salesOrderId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesOrderLinesForSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesOrderLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesOrderLineForSalesOrder(companyId, salesOrderId, body, callback)</td>
    <td style="padding:15px">postSalesOrderLineForSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesOrderLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesOrderLineForSalesOrder(companyId, salesOrderId, salesOrderLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesOrderLineForSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesOrderLineForSalesOrder(companyId, salesOrderId, salesOrderLineId, callback)</td>
    <td style="padding:15px">deleteSalesOrderLineForSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesOrderLineForSalesOrder(companyId, salesOrderId, salesOrderLineId, body, callback)</td>
    <td style="padding:15px">patchSalesOrderLineForSalesOrder</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesOrderLines(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesOrderLines</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesOrderLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesOrderLine(companyId, body, callback)</td>
    <td style="padding:15px">postSalesOrderLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesOrderLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesOrderLine(companyId, salesOrderLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesOrderLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesOrderLine(companyId, salesOrderLineId, callback)</td>
    <td style="padding:15px">deleteSalesOrderLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesOrderLine(companyId, salesOrderLineId, body, callback)</td>
    <td style="padding:15px">patchSalesOrderLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listRetainedEarningsStatement(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listRetainedEarningsStatement</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/retainedEarningsStatement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRetainedEarningsStatement(companyId, retainedEarningsStatementLineNumber, select, callback)</td>
    <td style="padding:15px">getRetainedEarningsStatement</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUnitsOfMeasure(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listUnitsOfMeasure</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/unitsOfMeasure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUnitOfMeasure(companyId, body, callback)</td>
    <td style="padding:15px">postUnitOfMeasure</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/unitsOfMeasure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnitOfMeasure(companyId, unitOfMeasureId, select, callback)</td>
    <td style="padding:15px">getUnitOfMeasure</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUnitOfMeasure(companyId, unitOfMeasureId, callback)</td>
    <td style="padding:15px">deleteUnitOfMeasure</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUnitOfMeasure(companyId, unitOfMeasureId, body, callback)</td>
    <td style="padding:15px">patchUnitOfMeasure</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAgedAccountsReceivable(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listAgedAccountsReceivable</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/agedAccountsReceivable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAgedAccountsReceivable(companyId, agedAccountsReceivableCustomerId, select, callback)</td>
    <td style="padding:15px">getAgedAccountsReceivable</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAgedAccountsPayable(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listAgedAccountsPayable</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/agedAccountsPayable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAgedAccountsPayable(companyId, agedAccountsPayableVendorId, select, callback)</td>
    <td style="padding:15px">getAgedAccountsPayable</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBalanceSheet(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listBalanceSheet</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/balanceSheet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBalanceSheet(companyId, balanceSheetLineNumber, select, callback)</td>
    <td style="padding:15px">getBalanceSheet</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTrialBalance(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listTrialBalance</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/trialBalance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTrialBalance(companyId, trialBalanceNumber, expand, select, callback)</td>
    <td style="padding:15px">getTrialBalance</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listIncomeStatement(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listIncomeStatement</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/incomeStatement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncomeStatement(companyId, incomeStatementLineNumber, select, callback)</td>
    <td style="padding:15px">getIncomeStatement</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTaxAreas(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listTaxAreas</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/taxAreas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTaxArea(companyId, body, callback)</td>
    <td style="padding:15px">postTaxArea</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/taxAreas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaxArea(companyId, taxAreaId, select, callback)</td>
    <td style="padding:15px">getTaxArea</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTaxArea(companyId, taxAreaId, callback)</td>
    <td style="padding:15px">deleteTaxArea</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTaxArea(companyId, taxAreaId, body, callback)</td>
    <td style="padding:15px">patchTaxArea</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesQuotes(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesQuotes</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesQuotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesQuote(companyId, body, callback)</td>
    <td style="padding:15px">postSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesQuotes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesQuote(companyId, salesQuoteId, expand, select, callback)</td>
    <td style="padding:15px">getSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesQuote(companyId, salesQuoteId, callback)</td>
    <td style="padding:15px">deleteSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesQuote(companyId, salesQuoteId, body, callback)</td>
    <td style="padding:15px">patchSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">makeInvoiceActionSalesQuotes(companyId, salesQuoteId, callback)</td>
    <td style="padding:15px">makeInvoiceActionSalesQuotes</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.makeInvoice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">makeOrderActionSalesQuotes(companyId, salesQuoteId, callback)</td>
    <td style="padding:15px">makeOrderActionSalesQuotes</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.makeOrder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendActionSalesQuotes(companyId, salesQuoteId, callback)</td>
    <td style="padding:15px">sendActionSalesQuotes</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.send?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesQuoteLinesForSalesQuote(companyId, salesQuoteId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesQuoteLinesForSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesQuoteLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesQuoteLineForSalesQuote(companyId, salesQuoteId, body, callback)</td>
    <td style="padding:15px">postSalesQuoteLineForSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesQuoteLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesQuoteLineForSalesQuote(companyId, salesQuoteId, salesQuoteLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesQuoteLineForSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesQuoteLineForSalesQuote(companyId, salesQuoteId, salesQuoteLineId, callback)</td>
    <td style="padding:15px">deleteSalesQuoteLineForSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesQuoteLineForSalesQuote(companyId, salesQuoteId, salesQuoteLineId, body, callback)</td>
    <td style="padding:15px">patchSalesQuoteLineForSalesQuote</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesQuoteLines(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesQuoteLines</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesQuoteLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesQuoteLine(companyId, body, callback)</td>
    <td style="padding:15px">postSalesQuoteLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesQuoteLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesQuoteLine(companyId, salesQuoteLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesQuoteLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesQuoteLine(companyId, salesQuoteLineId, callback)</td>
    <td style="padding:15px">deleteSalesQuoteLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesQuoteLine(companyId, salesQuoteLineId, body, callback)</td>
    <td style="padding:15px">patchSalesQuoteLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesCreditMemos(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesCreditMemos</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesCreditMemos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesCreditMemo(companyId, body, callback)</td>
    <td style="padding:15px">postSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesCreditMemos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesCreditMemo(companyId, salesCreditMemoId, expand, select, callback)</td>
    <td style="padding:15px">getSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesCreditMemo(companyId, salesCreditMemoId, callback)</td>
    <td style="padding:15px">deleteSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesCreditMemo(companyId, salesCreditMemoId, body, callback)</td>
    <td style="padding:15px">patchSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelAndSendActionSalesCreditMemos(companyId, salesCreditMemoId, callback)</td>
    <td style="padding:15px">cancelAndSendActionSalesCreditMemos</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.cancelAndSend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelActionSalesCreditMemos(companyId, salesCreditMemoId, callback)</td>
    <td style="padding:15px">cancelActionSalesCreditMemos</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAndSendActionSalesCreditMemos(companyId, salesCreditMemoId, callback)</td>
    <td style="padding:15px">postAndSendActionSalesCreditMemos</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.postAndSend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postActionSalesCreditMemos(companyId, salesCreditMemoId, callback)</td>
    <td style="padding:15px">postActionSalesCreditMemos</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.post?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendActionSalesCreditMemos(companyId, salesCreditMemoId, callback)</td>
    <td style="padding:15px">sendActionSalesCreditMemos</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.send?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesCreditMemoLinesForSalesCreditMemo(companyId, salesCreditMemoId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesCreditMemoLinesForSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesCreditMemoLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesCreditMemoLineForSalesCreditMemo(companyId, salesCreditMemoId, body, callback)</td>
    <td style="padding:15px">postSalesCreditMemoLineForSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/salesCreditMemoLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesCreditMemoLineForSalesCreditMemo(companyId, salesCreditMemoId, salesCreditMemoLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesCreditMemoLineForSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesCreditMemoLineForSalesCreditMemo(companyId, salesCreditMemoId, salesCreditMemoLineId, callback)</td>
    <td style="padding:15px">deleteSalesCreditMemoLineForSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesCreditMemoLineForSalesCreditMemo(companyId, salesCreditMemoId, salesCreditMemoLineId, body, callback)</td>
    <td style="padding:15px">patchSalesCreditMemoLineForSalesCreditMemo</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSalesCreditMemoLines(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listSalesCreditMemoLines</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesCreditMemoLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSalesCreditMemoLine(companyId, body, callback)</td>
    <td style="padding:15px">postSalesCreditMemoLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/salesCreditMemoLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSalesCreditMemoLine(companyId, salesCreditMemoLineId, expand, select, callback)</td>
    <td style="padding:15px">getSalesCreditMemoLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSalesCreditMemoLine(companyId, salesCreditMemoLineId, callback)</td>
    <td style="padding:15px">deleteSalesCreditMemoLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSalesCreditMemoLine(companyId, salesCreditMemoLineId, body, callback)</td>
    <td style="padding:15px">patchSalesCreditMemoLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listGeneralLedgerEntryAttachments(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listGeneralLedgerEntryAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/generalLedgerEntryAttachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeneralLedgerEntryAttachments(companyId, body, callback)</td>
    <td style="padding:15px">postGeneralLedgerEntryAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/generalLedgerEntryAttachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGeneralLedgerEntryAttachments(companyId, generalLedgerEntryAttachmentsGeneralLedgerEntryNumber, generalLedgerEntryAttachmentsId, expand, select, callback)</td>
    <td style="padding:15px">getGeneralLedgerEntryAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGeneralLedgerEntryAttachments(companyId, generalLedgerEntryAttachmentsGeneralLedgerEntryNumber, generalLedgerEntryAttachmentsId, callback)</td>
    <td style="padding:15px">deleteGeneralLedgerEntryAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchGeneralLedgerEntryAttachments(companyId, generalLedgerEntryAttachmentsGeneralLedgerEntryNumber, generalLedgerEntryAttachmentsId, body, callback)</td>
    <td style="padding:15px">patchGeneralLedgerEntryAttachments</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPurchaseInvoices(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listPurchaseInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/purchaseInvoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPurchaseInvoice(companyId, body, callback)</td>
    <td style="padding:15px">postPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/purchaseInvoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseInvoice(companyId, purchaseInvoiceId, expand, select, callback)</td>
    <td style="padding:15px">getPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePurchaseInvoice(companyId, purchaseInvoiceId, callback)</td>
    <td style="padding:15px">deletePurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPurchaseInvoice(companyId, purchaseInvoiceId, body, callback)</td>
    <td style="padding:15px">patchPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postActionPurchaseInvoices(companyId, purchaseInvoiceId, callback)</td>
    <td style="padding:15px">postActionPurchaseInvoices</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/Microsoft.NAV.post?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPurchaseInvoiceLinesForPurchaseInvoice(companyId, purchaseInvoiceId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listPurchaseInvoiceLinesForPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/purchaseInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPurchaseInvoiceLineForPurchaseInvoice(companyId, purchaseInvoiceId, body, callback)</td>
    <td style="padding:15px">postPurchaseInvoiceLineForPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/purchaseInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseInvoiceLineForPurchaseInvoice(companyId, purchaseInvoiceId, purchaseInvoiceLineId, expand, select, callback)</td>
    <td style="padding:15px">getPurchaseInvoiceLineForPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePurchaseInvoiceLineForPurchaseInvoice(companyId, purchaseInvoiceId, purchaseInvoiceLineId, callback)</td>
    <td style="padding:15px">deletePurchaseInvoiceLineForPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPurchaseInvoiceLineForPurchaseInvoice(companyId, purchaseInvoiceId, purchaseInvoiceLineId, body, callback)</td>
    <td style="padding:15px">patchPurchaseInvoiceLineForPurchaseInvoice</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listPurchaseInvoiceLines(companyId, top, skip, limit, filter, expand, select, callback)</td>
    <td style="padding:15px">listPurchaseInvoiceLines</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/purchaseInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPurchaseInvoiceLine(companyId, body, callback)</td>
    <td style="padding:15px">postPurchaseInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/purchaseInvoiceLines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseInvoiceLine(companyId, purchaseInvoiceLineId, expand, select, callback)</td>
    <td style="padding:15px">getPurchaseInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePurchaseInvoiceLine(companyId, purchaseInvoiceLineId, callback)</td>
    <td style="padding:15px">deletePurchaseInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPurchaseInvoiceLine(companyId, purchaseInvoiceLineId, body, callback)</td>
    <td style="padding:15px">patchPurchaseInvoiceLine</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listProjects(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listProjects</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProject(companyId, body, callback)</td>
    <td style="padding:15px">postProject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProject(companyId, projectId, select, callback)</td>
    <td style="padding:15px">getProject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProject(companyId, projectId, callback)</td>
    <td style="padding:15px">deleteProject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchProject(companyId, projectId, body, callback)</td>
    <td style="padding:15px">patchProject</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listBankAccounts(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listBankAccounts</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/bankAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBankAccount(companyId, body, callback)</td>
    <td style="padding:15px">postBankAccount</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/bankAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBankAccount(companyId, bankAccountId, select, callback)</td>
    <td style="padding:15px">getBankAccount</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBankAccount(companyId, bankAccountId, callback)</td>
    <td style="padding:15px">deleteBankAccount</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchBankAccount(companyId, bankAccountId, body, callback)</td>
    <td style="padding:15px">patchBankAccount</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomerSales(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listCustomerSales</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/customerSales?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomerSale(companyId, customerSaleCustomerId, customerSaleCustomerNumber, customerSaleName, select, callback)</td>
    <td style="padding:15px">getCustomerSale</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listVendorPurchases(companyId, top, skip, limit, filter, select, callback)</td>
    <td style="padding:15px">listVendorPurchases</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/vendorPurchases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorPurchase(companyId, vendorPurchaseVendorId, vendorPurchaseVendorNumber, vendorPurchaseName, select, callback)</td>
    <td style="padding:15px">getVendorPurchase</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
